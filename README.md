<div align="center">
<h1>compress4cj</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v1.0.0-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.58.3-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-90.1%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## 介绍

compress4cj 库定义了一个用于多种文件的压缩(打包)和解压缩(解包)的工具库, 主要包括存档格式 RAR、TAR、ZIP格式, 流式包含bzip2、gzip、zlib、deflate格式的压缩/解压功能，只支持RAR4.0文件解压功能。


### 特性

- 💡 支持rar4.0格式文件的解压功能

- 💡 支持bzip2，提供公共的压缩和解压缩接口

- 💡 支持gzip，提供公共的压缩和解压缩接口

- 💡 支持deflate，提供公共的压缩和解压缩接口

- 💡 整合标准库的zlib包 提供公共的压缩和解压缩接口

- 💡 整合zip4cj，提供公共的压缩和解压缩接口

- 💡 支持tar，提供公共的打包和解包接口

## 软件架构

### 架构图

<p align="center">
<img src="./doc/assets/frame.PNG" width="60%" >
</p>

架构图文字说明，包括模块说明、架构层次等详细说明。

### 源码目录

```shell
/compress4cj            # 三方库源代码
├── doc                 # 文档
│   ├── assets
│   ├── design.md
│   └── feature_api.md  # api接口文档
├── src
│   ├── archivers
│   │   ├── rar         # rar源码组件目录
│   │   ├── tar         # tar源码组件目录
│   │   └── zip         # zip源码组件目录
│   ├── compressors
│   │   ├── bzip2       # bzip2源码组件目录
│   │   ├── deflate     # deflate源码组件目录
│   │   ├── gzip        # gzip源码组件目录
│   │   └── zlib        # zlib源码组件目录
│   └── utils           # 工具源码组件目录
├── test
│   ├── doc             # README文档示例目录
│   ├── FUZZ            # fuzz测试用例目录
│   ├── HLT             # HLT测试用例目录
│   ├── LLT             # LLT测试用例目录
│   └── resources       # HLT和LLT测试文件目录
├── CHANGELOG.md
├── LICENSE.txt
├── module.json
├── README.md
└── README.OpenSource

```

- `doc`  存放库的设计文档、使用文档
- `src`  是库源码目录
- `test` 存放 DOC 演示样例、fuzz 测试用例、HLT 测试用例、LLT 自测用例和 resources 测试用例使用文件

### 接口说明

主要类和函数接口说明详见 [API](./doc/feature_api.md)


## 使用说明

### 编译构建

#### windows 编译过程：
```bat
cjpm update
cjpm build
```
#### windows 测试过程：
```bat
set Path=%构建build目录%;%Path%
cjc -O2  --import-path %构建build目录% -L %构建build目录%/compress4cj -L %构建build目录%/charset -l compress4cj_archivers -l compress4cj_archivers.rar.crc -l compress4cj_archivers.rar -l compress4cj_archivers.rar.exception -l compress4cj_archivers.rar.io -l compress4cj_archivers.rar.rarfile -l compress4cj_archivers.rar.unpack.decode -l compress4cj_archivers.rar.unpack -l compress4cj_archivers.rar.unpack.ppm -l compress4cj_archivers.rar.unpack.vm -l compress4cj_archivers.rar.unsigned -l compress4cj_archivers.rar.volume -l compress4cj_archivers.support -l compress4cj_archivers.tar -l compress4cj_archivers.zip -l compress4cj_compressors.bzip2 -l compress4cj_compressors.deflate -l compress4cj_compressors -l compress4cj_compressors.gzip -l compress4cj_compressors.support -l compress4cj_compressors.zlib -l compress4cj_utils -l compress4cj_utils.externals -l compress4cj_utils.internals -l compress4cj_utils.nio -l charset_charset -l charset_charset.encoding -l charset_charset.japanese -l charset_charset.korean -l charset_charset.simplechinese -l charset_charset.singlebyte -l charset_charset.traditionchinese -l charset_charset.unicode  xxxx.cj -o xxxx.cj.exe
:: 需要将依赖库的.dll文件和.cjo文件复制到.exe当前目录下
:: 执行exe
xxxx.cj.exe
```
#### linux 编译过程：
```shell
cjpm update 
cjpm build
```
#### linux 测试过程：
```shell
export LD_LIBRARY_PATH=${构建build目录}:${LD_LIBRARY_PATH}
cjc -O2  --import-path ${构建build目录} -L ${构建build目录}/compress4cj -L ${构建build目录}/charset -l compress4cj_archivers -l compress4cj_archivers.rar.crc -l compress4cj_archivers.rar -l compress4cj_archivers.rar.exception -l compress4cj_archivers.rar.io -l compress4cj_archivers.rar.rarfile -l compress4cj_archivers.rar.unpack.decode -l compress4cj_archivers.rar.unpack -l compress4cj_archivers.rar.unpack.ppm -l compress4cj_archivers.rar.unpack.vm -l compress4cj_archivers.rar.unsigned -l compress4cj_archivers.rar.volume -l compress4cj_archivers.support -l compress4cj_archivers.tar -l compress4cj_archivers.zip -l compress4cj_compressors.bzip2 -l compress4cj_compressors.deflate -l compress4cj_compressors -l compress4cj_compressors.gzip -l compress4cj_compressors.support -l compress4cj_compressors.zlib -l compress4cj_utils -l compress4cj_utils.externals -l compress4cj_utils.internals -l compress4cj_utils.nio -l charset_charset -l charset_charset.encoding -l charset_charset.japanese -l charset_charset.korean -l charset_charset.simplechinese -l charset_charset.singlebyte -l charset_charset.traditionchinese -l charset_charset.unicode  xxxx.cj -o xxxx.cj.out
./xxxx.cj.out
```


### 功能示例
#### rar 模块解压rar文件功能实例, 方式一
功能示例描述:
```cangjie
import std.fs.*
import std.os.posix.*
import compress4cj.utils.externals.*
import compress4cj.archivers.*
import compress4cj.*
main () {
    let input: File = File("./test/resources/MobaXterm_Portable_v22.1_1.rar", Open(true, false))
    let cis: ArchiveInputStream = ArchiveStreamFactory.DEFAULT.createArchiveInputStream("rar", input)
    IOUtils.copy(cis, getcwd())
}
```

执行结果如下： 
```cangjie
2023/08/22 23:25:40.917096 INFO Logger extracting: MobaXterm_Portable_v22.1\CygUtils.plugin
2023/08/22 23:25:43.893536 INFO Logger extracting: MobaXterm_Portable_v22.1\MobaXterm_Personal_22.1.exe
2023/08/22 23:25:45.550802 INFO Logger extracting: MobaXterm_Portable_v22.1
```

#### rar 模块解压rar文件功能实例, 方式二
```cangjie
import std.fs.*
import std.os.posix.*
import compress4cj.utils.externals.*
import compress4cj.archivers.*
import compress4cj.*
main () {
    Junrar.extract("./test/resources/MobaXterm_Portable_v22.1_1.rar", getcwd())
    return 0
}
```

执行结果如下： 
```cangjie
2023/08/22 23:25:40.917096 INFO Logger extracting: MobaXterm_Portable_v22.1\CygUtils.plugin
2023/08/22 23:25:43.893536 INFO Logger extracting: MobaXterm_Portable_v22.1\MobaXterm_Personal_22.1.exe
2023/08/22 23:25:45.550802 INFO Logger extracting: MobaXterm_Portable_v22.1
```

#### zip 整合接口 功能示例

功能示例描述:

示例代码如下：

```cangjie

import compress4cj.archivers.*
import compress4cj.*
import compress4cj.archivers.zip.*
import compress4cj.utils.externals.*
import compress.zlib.*
import std.fs.*
import std.os.posix.*

main() {   
    zipTest()
    unZipTest()
    return 0
}

func zipTest() {
    let input1: File = File("test1.xml", Open(true, true))
    let input2: File = File("test2.xml", Open(true, true))
    let ouput: File = File("bla.zip", CreateOrTruncate(true))
    let cos: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStream("zip", ouput)
    IOUtils.copy(input1, cos)
    IOUtils.copy(input2, cos)
    cos.flush()
}

func unZipTest() {
    var path2: String = getcwd()
    let input: File = File("bla.zip", Open(true, true))
    let ais: ArchiveInputStream = ArchiveStreamFactory.DEFAULT.createArchiveInputStream("zip", input)
    let path: String = "${path2}/testZipFile02/"
    IOUtils.copy(ais, path) 
}
```

执行结果如下：

```shell
2023/09/25 01:07:14.932315 INFO Logger test1.xml解压完成
2023/09/25 01:07:14.932521 INFO Logger test2.xml解压完成
```

#### zlib 整合接口 功能示例

功能示例描述:

示例代码如下：

```cangjie
import compress4cj.compressors.*
import compress4cj.compressors.zlib.*
import compress4cj.*
import compress4cj.utils.externals.*
import compress.zlib.*
import std.fs.*
import std.io.*

main() {   
    testZlibCreation()
    testZlibUnarchiveno()
    return 0
}


func testZlibCreation() {
    let input: File = File("test1.xml", Open(true, true))
    let ouput: File = File("test111.xml.zlib", CreateOrTruncate(true))
    let cos: CompressorOutputStream = CompressorStreamFactory().createCompressorOutputStream("zlib", ouput)
    IOUtils.copy(input, cos)
}

func testZlibUnarchiveno() {
    let input: File = File("oktest.xml.zlib", Open(true, true))
    let output: File = File("ok.xml", CreateOrTruncate(true))
    let inp: CompressorInputStream = CompressorStreamFactory().createCompressorInputStream("zlib", input)
    IOUtils.copy(inp, output)
}
```

执行结果如下：

```shell
0
```

#### tar模块打包功能实例，实现方式一

功能示例描述:

示例代码如下：

```cangjie
import std.io.*
import std.os.*
import std.fs.*
import std.os.posix.*
import compress4cj.archivers.tar.*

main () {
    let tos = TarArchiveOutputStream(File("test.tar", CreateOrTruncate(true)))
    let en1 = TarArchiveEntry("file1")
    en1.setSize(25)
    tos.putArchiveEntry(en1)
    tos.write("this is content of file1\n".toUtf8Array())
    tos.closeArchiveEntry()
    let en2 = TarArchiveEntry("file2")
    en2.setSize(25)
    tos.putArchiveEntry(en2)
    tos.write("this is content of file2\n".toUtf8Array())
    tos.closeArchiveEntry()
    tos.close()
    println("Successfully packaged the file.")
}
```

执行结果如下：

```cangjie
Successfully packaged the file.
```

#### tar模块打包功能实例，实现方式二
示例代码如下：

```cangjie
import std.io.*
import std.os.*
import std.fs.*
import std.os.posix.*
import compress4cj.archivers.tar.*
import compress4cj.utils.externals.*
import compress4cj.archivers.*
import compress4cj.*

main() {
    let name1 = Path("/home/liyanqing/workspace/compress/test/resources/HLT_resource/test1.xml")
    let name2 = Path("/home/liyanqing/workspace/compress/test/resources/HLT_resource/test2.xml")
    var output = File("./bla.tar", CreateOrTruncate(true))
    let tar: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStream(ArchiveStreamFactory.TAR, output)
    tar.putArchiveEntry(TarArchiveEntry(name1, name1.fileName.getOrThrow()))
    IOUtils.copy(File(name1, Open(true, false)), tar)
    tar.closeArchiveEntry()

    tar.putArchiveEntry(TarArchiveEntry(name2, name2.fileName.getOrThrow()))
    IOUtils.copy(File(name2, Open(true, false)), tar)
    tar.closeArchiveEntry()
    tar.close()
    println("Successfully packaged the file.")
}
```

执行结果如下：

```cangjie
Successfully packaged the file.
```

#### tar模块解档功能实例，实现方式一

功能示例描述:

示例代码如下：

```cangjie
import std.io.*
import std.os.*
import std.fs.*
import std.os.posix.*
import compress4cj.archivers.tar.*
import compress4cj.utils.externals.*
import compress4cj.archivers.*
import compress4cj.*

main() {   
    let input: File = File("./bla.tar", Open(true, true))
    let ais: ArchiveInputStream = ArchiveStreamFactory.DEFAULT.createArchiveInputStream("tar", input)
    IOUtils.copy(ais, getcwd())
    println("Successfully unpacked the file")
}
```

执行结果如下：
```cangjie
Successfully unpacked the file
```

#### tar模块解档功能实例，实现方式二

功能示例描述:

示例代码如下：

```cangjie
import std.io.*
import std.os.*
import std.fs.*
import std.os.posix.*
import compress4cj.archivers.tar.*
import compress4cj.utils.externals.*
import compress4cj.archivers.*
import compress4cj.*

main() {   
    let tar = TarArchiveInputStream(File("./bla.tar", Open(true, false)))
    let buffer = Array<Byte>(1024, item: 0)
    var read = 0
    while (true) {
        match(tar.getNextTarEntry()) {
            case None => break
            case Some(v) =>
                if (v.isDirectory()) {
                    if (!Directory.exists(v.getName())){
                        Directory.create(v.getName())
                    }
                    continue;
                }
                let outputFile = File(v.getName(), CreateOrTruncate(true))
                do {
                    read = tar.read(buffer)
                    if (read <= 0) {
                        break
                    }
                    outputFile.write(buffer[0..read])
                }
                while (true)
        }
    }
    println("Successfully unpacked the file")
}
```

执行结果如下：
```cangjie
Successfully unpacked the file
```

#### 

#### gzip 整合接口 功能示例

功能示例描述:

示例代码如下：

```cangjie

import compress4cj.*
import compress4cj.compressors.*
import compress4cj.compressors.gzip.*
import compress4cj.utils.externals.*
import compress.zlib.*
import std.fs.*
import std.io.*

main() {   

    testGzipCreation()
    testGzipUnarchive()
    return 0
}

func testGzipCreation() {
    let input: File = File("test1.xml", Open(true, true))
    let ouput: File = File("test111.xml.gz", CreateOrTruncate(true))
    let cos: CompressorOutputStream = CompressorStreamFactory().createCompressorOutputStream("gz", ouput)
    IOUtils.copy(input, cos)
    
}

func testGzipUnarchive() {
    let input: File = File("bla.tgz", Open(true, true))
    let output: File = File("bla.tar", CreateOrTruncate(true))
    let inp: CompressorInputStream = CompressorStreamFactory().createCompressorInputStream("gz", input)
    IOUtils.copy(inp, output)
}
```

执行结果如下：

```shell
0
```

#### deflate 整合接口 功能示例

功能示例描述:

示例代码如下：

```cangjie

import compress4cj.compressors.*
import compress4cj.compressors.deflate.*
import compress4cj.utils.externals.*
import compress.zlib.*
import compress4cj.*
import std.fs.*
import std.io.*

main() {   

    testDeflateCreation()
    testDeflateUnarchiveno()
    return 0
}


func testDeflateCreation() {
    let input: File = File("test1.xml", Open(true, true))
    let ouput: File = File("test111.xml.deflatez", CreateOrTruncate(true))
    let cos: CompressorOutputStream = CompressorStreamFactory().createCompressorOutputStream("deflate", ouput)
    IOUtils.copy(input, cos)
    
}

func testDeflateUnarchiveno() {
    let input: File = File("bla.tar.deflate", Open(true, true))
    let output: File = File("bla.tar", CreateOrTruncate(true))
    let inp: CompressorInputStream = CompressorStreamFactory().createCompressorInputStream("deflate", input)
    IOUtils.copy(inp, output)
}
```

执行结果如下：

```shell
0
```

#### bzip2 整合接口 功能示例

功能示例描述:

示例代码如下：

```cangjie
import compress4cj.compressors.*
import compress4cj.compressors.bzip2.*
import compress4cj.utils.externals.*
import compress4cj.*
import std.fs.*

main() {
    let dataFile: File = File("123.txt", CreateOrTruncate(true))
    dataFile.write("123".toUtf8Array())
    dataFile.close()
    compress1() // 压缩
    compress2() // 压缩
    decompress1() // 解压
    decompress2() // 解压
    0
}
func compress1(){
    let dataFile: File = File("123.txt", Open(true, true))
    let bzFile: File = File("123.bz2", CreateOrTruncate(true))
    let bzOut: BZip2CompressorOutputStream = BZip2CompressorOutputStream(bzFile)
    IOUtils.copy(dataFile, bzOut)
    bzOut.close()
}
func compress2(){
    let dataFile: File = File("123.txt", Open(true, true))
    let bzFile: File = File("123.bz2", CreateOrTruncate(true))
    let bzOut: CompressorOutputStream = CompressorStreamFactory().createCompressorOutputStream("bzip2", bzFile)
    IOUtils.copy(dataFile, bzOut)
    bzOut.close()
}
func decompress1(){
    let dataFile: File = File("123.txt", CreateOrTruncate(true))
    let bzFile: File = File("123.bz2", Open(true, true))
    let bzIn: BZip2CompressorInputStream = BZip2CompressorInputStream(bzFile)
    IOUtils.copy(bzIn, dataFile)
}
func decompress2(){
    let dataFile: File = File("123.txt", CreateOrTruncate(true))
    let bzFile: File = File("123.bz2", Open(true, true))
    let bzIn: CompressorInputStream = CompressorStreamFactory().createCompressorInputStream("bzip2", bzFile)
    IOUtils.copy(bzIn, dataFile)
}
```

执行结果如下：

```shell
0
```


## 开源协议

本项目基于 [Apache License 2.0](/LICENSE) ，请自由的享受和参与开源。

## <img alt="" src="./doc/assets/readme-icon-contribute.png" style="display: inline-block;" width=3%/> 参与贡献

欢迎给我们提交 PR，欢迎给我们提交 issue，欢迎参与任何形式的贡献。