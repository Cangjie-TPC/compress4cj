## v1.0.0
 - 升级compress4cj到0.58.3
 - 修改bug

## v0.0.1
 - 整合zip文件压缩和解压缩
 - 整合zlib、deflate、gzip压缩和解压缩
 - 提供rar4.0解压功能
 - 提供tar文件打包和解包完整功能
 - 提供bzip2压缩和解压缩完整功能
 - 提供zip解压功能
 - 适配windows环境运行、ohos环境、Linux环境运行
 