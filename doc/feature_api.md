# Compress4cj

# 介绍

compress4cj 库定义了一个用于多种文件的压缩(打包)和解压缩(解包)的工具库, 主要包括存档格式 RAR、TAR、ZIP格式, 流式包含bzip2、gzip、zib、deflate格式的压缩/解压功能，只支持RAR4.0文件解压功能。

# 接口说明
## 1 全局接口模块
### 1.1 archivers.support 包
#### 1.1.1 介绍
主要提供创建多种压缩格式文件的IO流
#### 1.1.2 主要接口
##### 1.1.2.1 ArchiveStreamFactory
```cangjie
public class ArchiveStreamFactory<: ArchiveStreamProvider {
    
    public static let DEFAULT: ArchiveStreamFactory = ArchiveStreamFactory()
    public static let TAR: String = "tar"
    public static let ZIP: String = "zip"
    public static let RAR: String = "rar"
    
    /*
     * 构造方法
     * 
     */
    public init ()
    
    /*
     * 构造方法
     * 
     * 参数 String- 要使用的编码
     */
    public init (encode: ?String)
    
    /*
     * 返回用于zip、rar和tar文件的编码，或者为归档器默认值为null
     * 
     * 返回值 String- 条目编码，归档器默认值为空, 如果encode设置None, 调用此方法会抛NoneValueException.
     */
    public func getEntryEncoding (): String
    
    /*
     * 设置用于zip、rar和tar文件的编码，或者为归档器默认值为null
     * 
     * 参数 String- 条目编码，条目编码null使用归档器默认值
     */
    public func setEntryEncoding (entryEncod: String)
    
    /*
     * 从存档器名称和输入流创建存档输入流
     * 
     * 参数 String - 存档名称, 目前只支持 zip/rar/tar 三种, 若传参错误会报非法参数异常.
     * 参数 InputStream - 输入流
     * 返回值 ArchiveInputStream - 包装输入流的存档器输入流
     */
    public func createArchiveInputStream (archiverName: String, inp: InputStream): ArchiveInputStream
    
    /*
     * 从存档器名称和输入流创建存档输入流
     * 
     * 参数 String - 存档名称, 目前只支持 zip/rar/tar 三种, 若传参错误会报非法参数异常.
     * 参数 InputStream - 输入流
     * 参数 String - 编码类型
     * 返回值 ArchiveInputStream - 包装输入流的存档器输入流
     */
    public func createArchiveInputStream (arcName: String, inp: InputStream, actualEncoding: ?String): ArchiveInputStream
    
    /*
     * 从存档器名称和输出流创建存档输出流
     * 
     * 参数 String - 存档名称, 目前只支持 zip/tar 两种, 若传参错误会报非法参数异常.
     * 参数 OutputStream - 输出流
     * 返回值 ArchiveOutputStream - 包装输入流的存档器输出流
     */
    public func createArchiveOutputStream (arcName: String, out: OutputStream): ArchiveOutputStream
    
    /*
     * 从存档器名称和输出流创建存档输出流
     * 
     * 参数 String - 存档名称, 目前只支持 zip/tar 两种, 若传参错误会报非法参数异常.
     * 参数 OutputStream - 输出流
     * 参数 String - 编码类型
     * 返回值 CompressorOutputStream - 包装输入流的存档器输出流
     */
    public func createArchiveOutputStream (arcName: String, out: OutputStream, actualEncoding: ?String): ArchiveOutputStream
    
    /*
     * 获取此提供程序的所有输入流存档名称
     * 
     * 返回值 Set<String> - 存档名称
     */
    public func getInputStreamArchiveNames (): Set<String>
    
    /*
     * 获取此提供程序的所有输出流存档名称
     * 
     * 返回值 Set<String> - 存档名称
     */
    public func getOutputStreamArchiveNames (): Set<String>

    /*
     * 获取支持的输入流map对象, 此对象可以自定义输入流设置
     * 
     * 返回值  Map<String, ArchiveStreamProvider> - 输入流名称和输入流对象
     */
    public func getArchiveInputStreamProviders(): Map<String, ArchiveStreamProvider>

    /*
     * 获取支持的输出流map对象, 此对象可以自定义输出流设置
     * 
     * 返回值  Map<String, ArchiveStreamProvider> - 输出流名称和输出流对象
     */
    public func getArchiveOutputStreamProviders(): Map<String, ArchiveStreamProvider>
}
```
#### 1.1.3 示例
下面是解压rar文件的用例
代码如下:
```cangjie
from compress4cj import archivers.*
from compress4cj import archivers.tar.*
from compress4cj import archivers.rar.*
from compress4cj import archivers.support.*
from compress4cj import archivers.zip.*
from compress4cj import utils.externals.*
from compress import zlib.*
from std import fs.*
from std import io.*
from std import os.posix.*
main() {
    var path2: String = getcwd()
    let input: File = File("test.rar", Open(true, true))
    let ais: ArchiveInputStream = ArchiveStreamFactory.DEFAULT.createArchiveInputStream("rar", input)
    let output: String = path2
    IOUtils.copy(ais, output)
    println("rar success")
}
```
运行结果如下：
```cangjie
rar success
```
### 1.2 compressors.support 包
#### 1.2.1 介绍
主要提供创建多种流式压缩格式的IO流
#### 1.2.2 主要接口
##### 1.2.2.1 CompressorStreamFactory
```cangjie
public class CompressorStreamFactory<: CompressorStreamProvider {
    
    /*
     * 构造方法
     * 
     */
    public init()
    
    /*
     * 构造方法
     * 
     * 参数 Bool 如果为true，则解压缩，直到输入结束
     * 参数 Int32 一些流需要分配潜在的重要字节数组，并且它们可以提供检查以防止损坏文件上的oom。以kb为单位设置允许的最大内存分配
     */
    public init(decompressUntilEOF: Bool, memoryLimitInKb: Int32)
    
    /*
     * 构造方法
     * 
     * 参数 Bool 如果为true，则解压缩，直到输入结束
     */
    public init(dec: Bool)
    
    /*
     * 尝试检测压缩机流的类型
     * 
     * 参数 InputStream -input
     * 返回值 String - 字符串
     * 异常 - 如果输入流的流压缩信息不支持, 会抛CompressorException异常.
     */
    public static func detect(input: InputStream): String
    
    /*
     * 从输入流创建压缩器输入流，从流的前几个字节自动检测压缩器类型。InputStream必须支持标记，如BufferedInputStream
     * 
     * 参数 InputStream -input
     * 返回值 CompressorInputStream - 流式输入流
     */
    public func createCompressorInputStream(input: InputStream): CompressorInputStream
    
    /*
     * 从输入流创建压缩器输入流，从流的前几个字节自动检测压缩器类型。InputStream必须支持标记，如BufferedInputStream
     * 
     * 参数 String - 压缩类型名称, 目前只支持 deflate/gzip/zlib/bzip2 四种, 若传参错误会报非法参数异常.
     * 参数 InputStream - 输入流
     * 返回值 CompressorInputStream - 流式输入流
     * 异常 - 创建指定输入流name为空或者'\0'时, 会抛 IllegalArgumentException 异常.
     * 异常 - 创建指定输入流name不支持时, 会抛 CompressorException 异常.
     */
    public func createCompressorInputStream(name: String, input: InputStream): CompressorInputStream
    
    /*
     * 从输入流创建压缩器输入流，从流的前几个字节自动检测压缩器类型。InputStream必须支持标记，如BufferedInputStream
     * 
     * 参数 String - 压缩类型名称, 目前只支持 deflate/gzip/zlib/bzip2 四种, 若传参错误会报非法参数异常.
     * 参数 InputStream - 输入流
     * 参数 Bool - 实际解压连接
     * 返回值 CompressorInputStream - 流式输入流
     * 异常 - 创建指定输入流name为空或者'\0'时, 会抛 IllegalArgumentException 异常.
     * 异常 - 创建指定输入流name不支持时, 会抛 CompressorException 异常.
     */
    public func createCompressorInputStream(name: String, input: InputStream, actualDecompressConcatenated: Bool): CompressorInputStream
    
    /*
     * 从一个压缩名称和一个输出流创建一个压缩机输出流
     * 
     * 参数 String - 压缩类型名称, 目前只支持 deflate/gzip/zlib/bzip2 四种, 若传参错误会报非法参数异常.
     * 参数 OutputStream - 输出流
     * 返回值 CompressorOutputStream - 流式输出流
     * 异常 - 创建指定输出流name为空或者'\0'时, 会抛 IllegalArgumentException 异常.
     * 异常 - 创建指定输出流name不支持时, 会抛 CompressorException 异常.
     */
    public func createCompressorOutputStream(name: String, out: OutputStream): CompressorOutputStream
    
    /*
     * 获取解压连接
     * 
     * 返回值 Bool - 布尔值
     */
    public func getDecompressConcatenated(): Bool
    
    /*
     * 获取输入流名称
     * 
     * 返回值 Set<String> - 输入流名称
     */
    public func getInputStreamCompressorNames(): Set<String>
    
    /*
     * 获取输出流名称
     * 
     * 返回值 Set<String> - 输入流名称
     */
    public func getOutputStreamCompressorNames(): Set<String>
    
    /*
     * 设置解压连接
     * 返回值 Unit - 类型Unit
     */
    public func setDecompressConcatenated(decompressConcatenated: Bool): Unit

    
    /*
     * 获取支持的输入流map对象, 此对象可以自定义输入流设置
     * 
     * 返回值  Map<String, ArchiveStreamProvider> - 输入流名称和输入流对象
     */
    public func getCompressorInputStreamProviders(): Map<String, ArchiveStreamProvider>

    /*
     * 获取支持的输出流map对象, 此对象可以自定义输出流设置
     * 
     * 返回值  Map<String, ArchiveStreamProvider> - 输出流名称和输出流对象
     */
    public func getCompressorOutputStreamProviders(): Map<String, ArchiveStreamProvider>
}
```

#### 1.2.3 示例
下面是解压deflate的用例
代码如下:
```cangjie
from compress4cj import compressors.*
from compress4cj import compressors.deflate.*
from compress4cj import compressors.support.*
from compress4cj import utils.externals.*
from compress import zlib.*
from std import fs.*
from std import io.*
main() {
    let input: File = File("test1.xml", Open(true, true))
    let ouput: File = File("test111.xml.deflatez", CreateOrTruncate(true))
    let cos: CompressorOutputStream = CompressorStreamFactory().createCompressorOutputStream("deflate", ouput)
    IOUtils.copy(input, cos)
    println("deflate success")
}
```
运行结果如下：
```cangjie
deflate success
```
### 1.3 archivers 包

#### 1.3.1 介绍
存档格式的IO流父接口
#### 1.3.2 主要接口
##### 1.3.2.1 ArchiveInputStream
```cangjie
public abstract class ArchiveInputStream<: InputStream {
    
    /*
     * 获取 BytesRead
     * 
     * 返回值 Int64 - 数字类型
     */
    public func getBytesRead (): Int64
    
    /*
     * 获取 NextEntry 下一个存档条目
     * 
     * 返回值 ?ArchiveEntry - ?存档条目
     */
    public open func getNextEntry (): ?ArchiveEntry
}
```
##### 1.3.2.2 ArchiveOutputStream
```cangjie
public abstract class ArchiveOutputStream<: OutputStream {
    
    /*
     * 将输出流刷入到目标
     * 
     * 返回值 Unit - 类型Unit
     */
    public open func flush (): Unit
    
    /*
     * 关闭OutputStream。
     * 
     * 返回值 Unit - 类型Unit 
     */
    public open func finish (): Unit
    
    /*
     * 在输出流上放入一个条目。这将写入条目的头记录，并定位输出流以写入条目的内容。
     * 一旦调用了这个方法，流就可以调用write（）来写入条目的内容了。写入内容后，必须调用closeArchiveEntry（）以确保所有缓冲的数据都完全写入输出流。
     * 
     * 参数 archiveEntry - 要写入档案的TarEntry。
     * 返回值 Unit - 类型Unit 
     */
    public open func putArchiveEntry (entry: ArchiveEntry): Unit
    
    /*
     * 关闭一个条目。必须为所有包含数据的文件项调用此方法。原因是我们必须缓冲写入流的数据，以满足缓冲区基于记录的写入。
     * 因此，在关闭该条目和写入下一个条目之前，可能仍有数据片段在组装中，必须将其写入输出流。
     * 
     * 返回值 Unit - 类型Unit 
     */
    public open func closeArchiveEntry (): Unit
    
    /*
     * 实现接口的方法，调用outputstrea的flush方法
     * 
     * 返回值 Unit - 类型Unit 
     */
    public open func close (): Unit
}
```
##### 1.3.2.3 ArchiveException
```cangjie
public class ArchiveException <: Exception {
    
    /*
     * 构造 ArchiveException
     * 
     */
    public init ()
    
    /*
     * 构造 ArchiveException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
    
    /*
     * 异常打印信息
     * 
     * 返回值 message - 字符串
     */
    public override func toString(): String
}
```
### 1.4 compressors 包
#### 1.4.1 介绍
流式压缩/解压缩的IO流父接口
#### 1.4.2 主要接口
##### 1.4.2.1 CompressorOutputStream
```cangjie
public abstract class CompressorOutputStream<: OutputStream&Resource {
    
    /*
     * 刷数据到输出端
     * 
     * 返回值 Unit - 类型Unit
     */
    public open func flush (): Unit
    
    /*
     * 判断 是否关闭, 如果outputStream流是Resource的子类, 可以正常使用关闭流的操作, 如果不是Resource的子类, 可能无法关闭流.
     * 
     * 返回值 Bool - 布尔值
     */
    public open func isClosed (): Bool
    
    /*
     * 关闭输出流, 如果outputStream流是Resource的子类, 可以正常使用关闭流的操作, 如果不是Resource的子类, 可能无法关闭流.
     * 
     * 返回值 Unit - 类型Unit
     */
    public open func close (): Unit
}
```
##### 1.4.2.2 CompressorInputStream
```cangjie
public abstract class CompressorInputStream<: InputStream {
    
    /*
     * 获取 BytesRead
     * 
     * 返回值 Int64 - 数字类型
     */
    public func getBytesRead (): Int64
    
    /*
     * 获取 UncompressedCount
     * 
     * 返回值 Int64 - 数字类型
     */
    public func getUncompressedCount (): Int64
}
```
##### 1.4.2.3 CompressorException
```cangjie
public class CompressorException<: Exception {
    
    /*
     * 构造 CompressorException
     * 
     * 参数 message - 异常提示信息
     */
    public init (message: String)
    
    /*
     * 异常打印信息
     * 
     * 返回值 message - 字符串
     */
    public override func toString(): String
}
```
### 1.5 utils 包
#### 1.5.1 介绍
提供集合和输入流工具类
#### 1.5.2 主要接口
##### 1.5.2.1 BitSet
```cangjie
public class BitSet {
    
    /*
     * 构造 
     * 
     * 参数 size - 初始长度
     * 异常 如果size为负数时，抛NegativeArraySizeException
     */
    public init (size: Int64)
    
    /*
     * 获取指定位置的bit状态
     * 
     * 参数 index - 下标
     * 返回值 Bool - true已设置 false未设置
     */
    public func get (index: Int64): Bool
    
    /*
     * 设置指定位置的bit状态为true
     * 
     * 参数 index - 下标
     * 返回值 Unit - 类型Unit
     */
    public func set (index: Int64): Unit
    
    /*
     * 设置指定位置的bit状态为false
     * 
     * 参数 index - 下标
     * 返回值 Unit - 类型Unit
     */
    public func clear (index: Int64): Unit
    
    /*
     * 从指定下标开始(包含), 查找下一个状态为false的bit位置
     * 
     * 参数 index - 开始下标(包含)
     * 返回值 Int64 - 下一个状态为false的bit位置, 若无-1
     */
    public func nextClearBit (index: Int64): Int64
    
    /*
     * 从指定下标开始(包含), 查找下一个状态为true的bit位置
     * 
     * 参数 index - 开始下标(包含)
     * 返回值 Int64 - 下一个状态为false的bit位置, 若无-1
     */
    public func nextSetBit (index: Int64): Int64
}
```
##### 1.5.2.2 BitInputStream
```cangjie
public class BitInputStream {
    
    /*
     * 构造 
     * 
     * 参数 input - 源stream
     * 参数 byteOrder - 字节序
     */
    public init (input: InputStream, byteOrder: Endian)
    
    /*
     * 清空已读取bit, 保留未通过readBits读取的bit
     * 
     */
    public func clearBitCache ()
    
    /*
     * 从源stream读取最多63bit
     * 
     * 参数 count - 读取bit数量, 不大于63
     * 返回值 Int64 - 将位按流的字节顺序连接为Int64。如果在读取之前已达到基础流的结尾，则返回-1。
     * throws IllegalArgumentException 当count<0或count>63
     */
    public func readBits (count: Int64): Int64
    
    /*
     * 返回从该输入流中读取而不需要从底层输入流读取的比特数。
     * 
     * 返回值 Int64 - 将比特按流的字节顺序连接为Int64
     */
    public func bitsCached (): Int64
    
    /*
     * 可以读取的bit估计值。已缓存 + 未读取
     * 
     * 返回值 Int64 - 可读取bit数
     */
    public func bitsAvailable (): Int64
    
    /*
     * 抛弃bit，直到下一个字节边界
     */
    public func alignWithByteBoundary (): Unit
    
    /*
     * 返回已从底层流中读取的字节数。包括已缓存未读取的bit
     * 
     * 返回值 Int64 - 已从底层流中读取的字节数
     */
    public func getBytesRead (): Int64
}
```
##### 1.5.2.3 MarkableInputStream
```cangjie
public class MarkableInputStream<: InputStream {
    
    /*
     * 构造 MarkableInputStream
     * 
     * 参数 input - 输入流
     */
    public init (input: InputStream)
    
    /*
     * 读取数据到buff中
     * 
     * 参数 buff - 缓存
     * 返回值 Int64 - 读取的字节数
     */
    public func read (buff: Array<Byte>): Int64
    
    /*
     * 标记读取的位置
     * 
     * 返回值 Unit - 类型Unit
     */
    public func mark (): Unit
    
    /*
     * 重置标记的位置
     * 
     * 返回值 Unit - 类型Unit
     */
    public func reset (): Unit
}
```
### 1.6 utils.internals 包
#### 1.6.1 介绍
提供数组操作, 输出流工具类和无符号右移, bigint等工具函数
#### 1.6.2 主要接口
##### 1.6.2.1 FixedLengthBlockOutputStream
```cangjie
// 此类支持在固定长度的块中写入OutputStream
public class FixedLengthBlockOutputStream<: OutputStream {
    
    /*
     * 构造  FixedLengthBlockOutputStream
     * 
     * 参数 out - 输出
     * 参数 blockSize - 块大小
     * 异常 - blockSize 小于等于0时, IllegalArgumentException
     */
    public init (out: OutputStream, blockSize: Int64)
    
    /*
     * 实现接口方法，写入数据。写入数据超过块大小，未超过的数据刷出去之后，超过的数据在缓冲区未被刷出到output
     * 
     * 参数 buffer - 数据源
     * 返回值 Unit - 类型Unit
     */
    public func write (buffer: Array<Byte>): Unit
    
    /*
     * 缓冲区的数据刷入到output
     * 
     * 返回值 Unit - 类型Unit
     */
    public func flush (): Unit
    
    /*
     * 缓冲区的数据按照块大小刷入到output
     * 
     * 返回值 Unit - 类型Unit
     */
    public func flushBlock (): Unit
}
```
##### 1.6.2.2 ArchiveUtils
```cangjie
public class ArchiveUtils {
    
    /*
     * 如果数组的前N个字节均为零，则返回true 
     * 
     * 参数 a - 要检查的数组
     * 参数 size - 要检查的字符数（而不是数组的大小）
     * 返回值 Bool - 如果前N个字节为零，则为真, size参数为负数时, 返回值结果为true.
     */
    public static func isArrayZero (a: Array<Byte>, size: Int64): Bool
    
    /*
     * 检查缓冲区内容是否匹配ASCII字符串。
     * 
     * 参数 expected - 期望的字符串
     * 参数 buffer - 数组
     * 参数 offset - 数组的偏移量
     * 参数 length - 数组的长度
     * 返回值 Bool - 如果缓冲区与预期的字符串相同
     */
    public static func matchAsciiBuffer (expected: String, buffer: Array<Byte>, offset: Int64, length: Int64): Bool
    
    /*
     * 比较字节缓冲区，可以选择忽略后面的空值 
     * 
     * 参数 buffer1 - 数组1
     * 参数 offset1 - 数组1的偏移量
     * 参数 length1 - 数组1的长度
     * 参数 buffer2 - 数组2
     * 参数 offset2 - 数组2的偏移量
     * 参数 length2 - 数组2的长度
     * 参数 ignoreTrailingNulls - 忽略后面的空值
     * 返回值 Bool - 布尔值值
     */
    public static func isEqual (buffer1: Array<Byte>, offset1: Int64, length1: Int64, buffer2: Array<Byte>, offset2: Int64, length2: Int64, ignoreTrailingNulls: Bool): Bool
    
    /*
     * 将字符串转换为ASCII字节。
     * 
     * 参数 inputString - 转换字符串
     * 返回值 Array<Byte> - 字节
     * 异常 - 转换失败时, Exception
     */
    public static func toAsciiBytes (inputString: String): Array<Byte>
    
    /*
     * 使用ASCII字符集将输入字节数组转换为字符串。
     * 
     * 参数 inputBytes - 输入字节数组
     * 返回值 String - 字符串
     */
    public static func toAsciiString (inputBytes: Array<Byte>): String
    
    /*
     * 使用ASCII字符集将输入指定范围的字节数组转换为字符串。
     * 
     * 参数 inputBytes - 输入字节数组
     * 参数 offset - 偏移量
     * 参数 length - 长度
     * 返回值 String - 字符串
     * 异常 - 出现数字溢出或者参数不合法时, 会抛 IllegalArgumentException
     */
    public static func toAsciiStringView (inputBytes: Array<Byte>, offset: Int64, length: Int64): String
}
```
##### 1.6.2.3 CharsetNames
```cangjie
public class CharsetNames {
    public static let ISO_8859_1 = "ISO-8859-1"
    public static let US_ASCII = "US-ASCII"
    public static let UTF_16 = "UTF-16"
    public static let UTF_16BE = "UTF-16BE"
    public static let UTF_16LE = "UTF-16LE"
    public static let UTF_8 = "UTF-8"
}
```
##### 1.6.2.4 EncodingUtils
```cangjie
public class EncodingUtils {
    
    /*
     * 将bigint转换为bytes
     * 
     * 参数 value - 类型bigint
     * 返回值 Array<Byte> - 字节
     */
    public static func encoded (value: BigInt): Array<Byte>
}
```
##### 1.6.2.5 BigInt
```cangjie
public class BigInt<: Equatable<BigInt>&Hashable&ToString {
    /*
    * 构造方法
    *
    * 参数 n Int64数值
    */
    public init (n: Int64)

    /*
    * 构造方法
    *
    * 参数 x BigInt值
    */
    public init (x: BigInt)

    /*
    * 获取sign，true为正值，false为负值
    *
    * 返回值 sign值，bigint为0时，返回true。
    */
    public func getSign (): Bool

    /*
    * 获取digits
    *
    * 返回值 digits值
    */
    public func getDigits (): (Array<UInt32>,Int64)

    /*
    * 获取int值，超过Int64.Max截断
    *
    * @return int值
    * 异常 BigIntException - 转换int64失败，可能超出int64范围。
    */
    public func toInt (): Int64

    /*
    * 获取int值，可能为空
    *
    * 返回值 int值
    */
    public func checkedToInt (): Option<Int64>

    /*
    * 获取十进制字符串
    *
    * 返回值 十进制字符串
    */
    public func toStringDec (): String

    /*
    * 获取十六进制字符串
    *
    * 返回值 十六进制字符串
    */
    public func toStringHex (): String

    /*
    * 获取八进制字符串
    *
    * 返回值 八进制字符串
    */
    public func toStringOct (): String

    /*
    * 获取二进制字符串
    *
    * 返回值 二进制字符串
    */
    public func toStringBin (): String

    /*
    * 获取String,十进制内容
    *
    * 返回值 String
    */
    public func toString (): String

    /*
    * 获取toString()的hashCode值
    *
    * 返回值 hashCode值
    */
    public func hashCode (): Int64

    /*
    * 获取BigInt的绝对值
    *
    * 返回值 BigInt的绝对值
    */
    public func abs (): BigInt

    /*
    * 获取BigInt的负值
    *
    * 返回值 BigInt的负值
    */
    public operator func - (): BigInt

    /*
    * BigInt相加
    *
    * 参数 x BigInt值
    * 返回值 相加的结果
    */
    public operator func + (x: BigInt): BigInt

    /*
    * BigInt相加
    *
    * 参数 x Int64数值
    * 返回值 相加的结果
    */
    public operator func + (n: Int64): BigInt

    /*
    * BigInt相减
    *
    * 参数 x BigInt值
    * 返回值 相减的结果
    */
    public operator func - (x: BigInt): BigInt

    /*
    * BigInt相减
    *
    * 参数 x Int64数值
    * 返回值 相减的结果
    */
    public operator func - (n: Int64): BigInt

    /*
    * BigInt相乘
    *
    * 参数 x BigInt值
    * 返回值 相乘的结果
    */
    public operator func * (x: BigInt): BigInt

    /*
    * BigInt相乘
    *
    * 参数 x Int64数值
    * 返回值 相乘的结果
    */
    public operator func * (n: Int64): BigInt

    /*
    * BigInt相除
    *
    * 参数 x BigInt值
    * 返回值 相除的结果
    * 异常 ArithmeticException - 传入参数为0时发生异常。
    */
    public operator func / (x: BigInt): BigInt

    /*
    * BigInt左移
    *
    * 参数 n 左移数值
    * 返回值 左移的结果
    * 异常 ArithmeticException - 传入参数为负数时发生异常。
    */
    public func shiftLeft (n: Int64): BigInt

    /*
    * BigInt相除
    *
    * 参数 n Int64数值
    * 返回值 相除的结果
    * 异常 ArithmeticException - 传入参数为0时发生异常。
    */
    public operator func / (n: Int64): BigInt

    /*
    * ==方法
    *
    * 参数 x BigInt值
    * 返回值 是否相等
    */
    public operator func == (x: BigInt): Bool

    /*
    * !=方法
    *
    * 参数 x BigInt值
    * 返回值 是否不相等
    */
    public operator func !=(x: BigInt): Bool

    /*
    * >=方法
    *
    * 参数 x BigInt值
    * 返回值 是否大于等于
    */
    public operator func >= (x: BigInt): Bool

    /*
    * <=方法
    *
    * 参数 x BigInt值
    * 返回值 是否小于等于
    */
    public operator func <= (x: BigInt): Bool

    /*
    * >=方法
    *
    * 参数 x Int64数值
    * 返回值 是否大于等于
    */
    public operator func >= (n: Int64): Bool

    /*
    * <=方法
    *
    * 参数 x Int64数值
    * 返回值 是否小于等于
    */
    public operator func <= (n: Int64): Bool

    /*
    * BigInt右移
    *
    * 参数 n 右移数值
    * 返回值 右移的结果
    * 异常 ArithmeticException - 传入参数为负数时发生异常。
    */
    public func shiftRight (n: Int64): BigInt

    /*
    * <方法
    *
    * 参数 x BigInt值
    * 返回值 是否小于
    */
    public operator func < (x: BigInt): Bool

    /*
    * >方法
    *
    * 参数 x BigInt值
    * 返回值 是否大于
    */
    public operator func > (x: BigInt): Bool

    /*
    * >方法
    *
    * 参数 x Int64数值
    * 返回值 是否大于
    */
    public operator func > (n: Int64): Bool

    /*
    * <方法
    *
    * 参数 x Int64数值
    * 返回值 是否小于
    */
    public operator func < (n: Int64): Bool

    /*
    * sign取反
    *
    * 返回值 sign取反后的值
    */
    public func isNegative (): Bool
}
```
##### 1.6.2.6 ZipEncodingHelper
```cangjie
// 静态帮助器函数用于对ZIP文件中的文件名进行稳健编码。
public class ZipEncodingHelper {
    
    /*
     * 获取 默认编码格式的ZipEncoding
     * 
     * 返回值 ZipEncoding - 压缩编码
     */
    public static func getZipEncoding (): ZipEncoding
    
    /*
     * 获取 带有编码格式的 ZipEncoding
     * 
     * 参数 name - 字符集名称
     * 返回值 ZipEncoding - 压缩编码
     */
    public static func getZipEncoding (name: String): ZipEncoding 
}
```
##### 1.6.2.7 BoundedInputStream
```cangjie
// 一种流，它将从包装流中读取的数据限制在给定的字节数内。
public class BoundedInputStream<: InputStream {
    
    /*
     * 构造 最多从给定流中读取给定字节数的流。
     * 
     * 参数 inputStream - 输入流
     * 参数 size - 字节数, size为负数时,不保证程序的正确性.
     */
    public init (inputStream: InputStream, size: Int64)
    
    /*
     * 实现接口方法，读取数据到array中
     * 
     * 参数 array - 目标数组
     * 返回值 Int64 - 读取的大小
     */
    public func read (array: Array<Byte>): Int64
    
    /*
     * 读取一个字节的方法
     * 
     * 返回值 Int64 - 读取的字节
     */
    public func read (): Int64
    
    /*
     * 读取字节带有偏移量和长度
     * 
     * 参数 b - 读到目标数组
     * 参数 off - 偏移
     * 参数 len - 长度
     * 返回值 Int64 - 读取的大小
     * 异常 如果读取的偏移量和长度参数不匹配b的范围， 会抛越界异常。
     */
    public func readBytesOffset (b: Array<Byte>, off: Int64, len: Int64): Int64
    
    /*
     * 跳过字节数
     * 
     * 参数 n - 预期跳过的字节数
     * 返回值 Int64 - 实际跳过的字节数
     */
    public func skip (n: Int64): Int64
    
    /*
     * 获取 BytesRemaining
     * 
     * 返回值 Int64 - 字节sRemaining的值
     */
    public func getBytesRemaining (): Int64
}
```
##### 1.6.2.8 DecoderUtils
```cangjie
public class DecoderUtils {
    /*
     * 将Array<Byte>转换成BigInt
     *
     * @参数 value Byte数组
     * @返回值 BigInt
     * 异常 IndexOutOfBoundsException - 传入参数为空数组时发生异常.
     */
    public static func decode (value: Array<Byte>): BigInt
}
```
##### 1.6.2.9 全局函数
```cangjie
/*
 * 无符号右移的函数
 * 
 * 参数 src - 源数据
 * 参数 right - 右移位数
 * 返回值 Int64 - value值
 */
public func URS(src: Int64, right: Int64): Int64 

/*
 * 无符号右移的函数
 * 
 * 参数 src - 源数据
 * 参数 right - 右移位数
 * 返回值 Int64 - value值
 */
public func URS(src: Int64, right: Int32): Int64 

/*
 * 无符号右移的函数
 * 
 * 参数 src - 源数据
 * 参数 right - 右移位数
 * 返回值 Int64 - value值
 */
public func URS(src: Int64, right: Int16): Int64

/*
 * 无符号右移的函数
 * 
 * 参数 src - 源数据
 * 参数 right - 右移位数
 * 返回值 Int32 - value值
 */
public func URS(src: Int32, right: Int64): Int32 

/*
 * 无符号右移的函数
 * 
 * 参数 src - 源数据
 * 参数 right - 右移位数
 * 返回值 Int32 - value值
 */
public func URS(src: Int32, right: Int32): Int32

/*
 * 无符号右移的函数
 * 
 * 参数 src - 源数据
 * 参数 right - 右移位数
 * 返回值 Int32 - value值
 */
public func URS(src: Int32, right: Int16): Int32 

/*
 * 将指定的字节值分配给指定字节数组的每个元素
 * 
 * 参数 a - 源数据
 * 参数 fromIndex - 开始位置
 * 参数 toIndex - 结束位置
 * 参数 val - 字节值
 * 异常 - toIndex > a的size时, 会抛 IndexOutOfBoundsException
 */
public func fill<T>(a: Array<T>, fromIndex: Int32, toIndex: Int32, val: T): Unit

/*
 * 将指定的字节值分配给指定字节数组的每个元素
 * 
 * 参数 a - 源数据
 * 参数 val - 字节值
 */
public func fill<T>(a: Array<T>, val: T) : Unit

/*
 * 设置此矢量的大小。如果新的大小大于当前大小，则新的zeroValue值项将添加到向量的末尾。如果新大小小于当前大小，则索引newSize及以上的所有组件都将被丢弃。
 * 
 * 参数 src - 源数据
 * 参数 size - 范围大小大小值
 */
public func SetSize<T>(src: ArrayList<T>, size: Int64) : Unit

/*
 * 获取Logger对象
 * 
 * 返回值 Logger - 日志
 */
public func getLogger(): Logger 
```

##### 1.6.2.10 BigIntException
```cangjie
public class BigIntException <: Exception {
    /*
     * 构造 BigIntException
     * 参数 message - 异常信息
     */
    public init(message: String)

    /*
     * 打印异常信息
     * 返回值 String - 异常的toString方法
     */
    public override func toString(): String
}
```

### 1.7 utils.externals 包
#### 1.7.1 介绍
对外使用的IO工具类
#### 1.7.2 主要接口
##### 1.7.2.1 IOUtils
```cangjie
open class IOUtils {
    
    /*
     * 将InputStream的内容复制到OutputStream。使用8024字节的默认缓冲区大小
     * 
     * 参数 input - 输入流
     * 参数 output - 输出流
     * 返回值 Int64 - 流拷贝的大小
     */
    public static func copy (input: InputStream, output: OutputStream): Int64
    
    /*
     * 将InputStream的内容复制到OutputStream。使用8024字节的默认缓冲区大小
     * 
     * 参数 input - 输入流
     * 参数 path - 输出路径
     */
    public static func copy (input: InputStream, path: String)
    
    /*
     * 从文件中读取尽可能多的内容来填充给定的数组
     * 
     * 参数 input - 输入流
     * 参数 output - 输出流
     * 参数 buffersize - 缓冲大小
     * 返回值 Int64 - 流拷贝的大小
     * 异常 - 缓存fersize小于0时, IllegalArgumentException
     */
    public static func copy (input: InputStream, output: OutputStream, buffersize: Int32): Int64
    
    /*
     * 从输入流中读取尽可能多的内容来填充给定的数组
     * 
     * 参数 file - 输入流
     * 参数 arr - 缓存
     * 返回值 Int32 - 读取的内容长度
     */
    public static func read (file: File, arr: Array<Byte>): Int32
    
    /*
     * 从输入流中读取尽可能多的内容来填充给定的数组
     * 
     * 参数 input - 输入流
     * 参数 arr - 缓存
     * 返回值 Int32 - 读取的内容长度
     */
    public static func readFully (input: InputStream, arr: Array<Byte>): Int32
    
    /*
     *  从输入中尽可能多地读取，用给定的字节量填充给定的数组
     * 
     * 参数 input - 输入流
     * 参数 arr - 缓存
     * 参数 off - 偏移量
     * 参数 len - 长度
     * 返回值 Int32 - 读取的内容长度
     * 异常 - 参数越界时, IndexOutOfBoundsException
     */
    public static func readFully (input: InputStream, arr: Array<Byte>, off: Int32, len: Int32): Int32
    
    /*
     * 将源文件复制到给定的输出流
     * 
     * 参数 sourceFile - 源文件
     * 参数 output - 输出流
     */
    public static func copy (sourceFile: File, output: OutputStream): Unit
    
    /*
     * 将InputStream的部分内容复制到OutputStream中。使用8024字节的默认缓冲区大小
     * 
     * 参数 input - 输入流
     * 参数 len - 长度
     * 参数 output - 输出流
     * 返回值 Int64 - 复制的长度
     */
    public static func copyRange (input: InputStream, len: Int64, output: OutputStream): Int64
    
    /*
     * 将InputStream的部分内容复制到OutputStream中
     * 
     * 参数 input - 输入流
     * 参数 len - 长度
     * 参数 output - 输出流
     * 参数 buffersize - 缓存大小
     * 返回值 Int64 - 复制的长度
     * 异常 - 缓存fersize 小于0时, IllegalArgumentException
     */
    public static func copyRange (input: InputStream, len: Int64, output: OutputStream, buffersize: Int32): Int64
    
    /*
     * 将input返回出byte数组
     * 
     * 参数 input - 输入流
     * 返回值 Array<Byte> - 数组
     */
    public static func toByteArray (input: InputStream): Array<Byte>
}
```
### 1.8 utils.nio 包
#### 1.8.1 介绍
字节buffer的工具类
#### 1.8.2 主要接口
##### 1.8.2.1 Buffer
```cangjie
public abstract class Buffer<: Object&ToString {
    
    /*
     * 返回此缓冲区的容量。
     * 返回值 Int64 - 容量大小
     */
    public func capacity(): Int64

    /*
     * 返回此缓冲区的位置。
     * 返回值 Int64 - 位置
     */
    public func position(): Int64
    
    /*
     * 设置此缓冲区的位置。如果标记(mark)已定义且大于新位置(newPosition)，则丢弃该标记(mark)
     * 参数 newPosition - 新位置值；必须为非负且不大于当前限制(limit)
     * 返回值 Buffer - 缓存fer
     */
    public func position(newPosition: Int64): Buffer
    
    /*
     * 返回此缓冲区的限制。
     * 返回值 Int64 - 限制大小
     */
    public func limit(): Int64 
    
    /*
     * 设置此缓冲区的限制（limit）。
     * 如果位置（position）大于新限制（newlimit），则将其设置为新限制（limit）。
     * 如果标记（mark）已定义且大于新限制，则将丢弃该标记。
     * 参数 newPosition - 新的极限值；必须为非负，且不大于此缓冲区的容量（capacity）
     * 返回值 Buffer - 缓存fer
     */
    public func limit(newLimit: Int64): Buffer 
    
    /*
     * 返回此缓冲区的标记（mark）。
     * 返回值 Buffer - 缓存fer
     */
    public func mark(): Buffer
    
    /*
     * 将此缓冲区的位置重置为先前标记的位置，调用此方法既不会更改也不会丢弃标记的值。
     * 内部逻辑是把position设置成mark的值，相当于之前做过一个标记，现在要退回到之前标记的位置。
     * 返回值 Buffer - 缓存fer
     */
    public func reset(): Buffer
    
    /*
     * 重置初始化状态。它通常用于清除操作。
     * 此方法的功能是所有状态都将返回到初始状态。position=0， mark=-1，limit=capacity；
     * 注意，此方法与flip的区别在于limit的值是不一样的。
     * 返回值 Buffer - 缓存fer
     */
    public func clear(): Buffer
    
    /*
     * 翻转状态，将一个处于存数据状态的缓冲区变为一个处于准备取数据的状态，
     * 也就是让flip之后的position到limit这块区域变成之前的0到position这块
     * limit = position;position = 0;mark = -1;
     * 返回值 Buffer - 缓存fer
     */
    public func flip(): Buffer
    
    /*
     * 清零，一版用于写完之后的数据，想从起始位置开始读数据的情形
     * 把position设为0，mark设为-1，不改变limit的值。
     * 返回值 Buffer - 缓存fer
     */
    public func rewind(): Buffer
    
    /*
     * 返回limit和position之间相对位置差
     * 返回值 Int64 - 差
     */
    public func remaining(): Int64 
    
    /*
     * 返回是否还有未读内容
     * 返回值 Bool - 布尔值
     */
    public func hasRemaining(): Bool 
    
    /*
     * 返回此缓冲区中的第一个元素在缓冲区的底层实现数组中的偏移量 （可选操作）。
     * 如果存在实现此缓冲区的数组，则缓冲区位置 p 对应于数组索引 p + arrayOffset()。
     * 调用此方法之前要调用 hasArray 方法，以确保此缓冲区具有可访问的底层实现数组。
     * 返回值 Int64 - 偏移量
     */
    public func arrayOffset(): Int64
    
    /*
     * 实现此缓冲区的数组返回新的字节缓冲区
     * 新缓冲区的内容将从此缓冲区的当前位置开始。
     * 此缓冲区内容的更改在新缓冲区中是可见的，反之亦然；这两个缓冲区的位置、界限和标记值是相互独立的。
     * 新缓冲区的位置将为零，其容量和界限将为此缓冲区中所剩余的字节数量，其标记是不确定的。
     * 当且仅当此缓冲区为直接时，新缓冲区才是直接的，当且仅当此缓冲区为只读时，新缓冲区才是只读的。
     * 返回值 Int64 - 缓存fer
     */
    public func slice(): Buffer 
    
    /*
     * 创建共享此缓冲区内容的新的字节缓冲区。
     * 新缓冲区的内容将为此缓冲区的内容。
     * 此缓冲区内容的更改在新缓冲区中是可见的，反之亦然；这两个缓冲区的位置、界限和标记值是相互独立的。
     * 新缓冲区的容量、界限、位置和标记值将与此缓冲区相同。
     * 当且仅当此缓冲区为直接时，新缓冲区才是直接的，当且仅当此缓冲区为只读时，新缓冲区才是只读的。
     * 返回值 Buffer - 缓存fer
     */
    public func duplicate(): Buffer 
    
    /*
     * 返回支持此缓冲区的数组。
     * 此方法旨在允许更有效地将数组支持的缓冲区传递给本机代码。
     * 具体的实现子类会为该方法提供了更强类型的返回值。
     * 对该缓冲区内容的修改将导致返回数组的内容被修改，反之亦然。
     * 在调用此方法之前调用hasArray方法，以确保此缓冲区具有可访问的后备数组。
     * 返回值 Array<UInt8> - 数组
     */
    public func array(): Array<UInt8>
}
```
##### 1.8.2.2 ByteBuffer
```cangjie
public class ByteBuffer<: Buffer&ToString&Hashable&Equatable<Buffer> {
    
    /*
     * ByteBuffer类的主要入口函数.
     * 分配一个新的字节缓冲区。新缓冲区的位置将为零，其界限将为其容量，其标记是不确定的。它将具有一个底层实现数组，且其数组偏移量将为零。
     * 参数 capacity - 新缓冲区的容量，以字节为单位 
     * 返回值 ByteBuffer - 字节Buffer
     */
    public static func allocate(capacity: Int64): ByteBuffer
    
    /*
     * ByteBuffer类的主要入口函数.
     * 将 UInt8 数组包装到缓冲区中。新的缓冲区将由给定的 UInt8 数组支持。
     * 也就是说，缓冲区修改将导致数组修改，反之亦然。
     * 新缓冲区的容量将为array.length，其位置将为offset，其界限将为 offset + length，其标记是不确定的。
     * 其底层实现数组将为给定数组，并且其数组偏移量将为零。
     * 参数 array - 数组
     * 参数 offset - 要使用的子数组的偏移量；必须为非负且不大于array.length。将新缓冲区的位置设置为此值。
     * 参数 length - 要使用的子数组的长度；必须为非负且不大于array.length - 偏移量。将新缓冲区的界限设置为offset + length。 
     * 返回值 ByteBuffer - 字节Buffer
     */
    public static func wrap(array: Array<UInt8>, offset: Int64, length: Int64): ByteBuffer 
    
    /*
     * ByteBuffer类的主要入口函数.
     * 相当于wrap(array, 0, array.size)
     * 参数 array - 数组
     * 返回值 ByteBuffer - 字节Buffer
     */
    public static func wrap(array: Array<UInt8>): ByteBuffer 
    
    /*
     * 相当于 get(dst, 0, dst.size)
     * 参数 dst - 向其中写入字节的数组
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func get(dst: Array<UInt8>): ByteBuffer
    
    /*
     * 将给定src缓冲区中的剩余字节传输到此缓冲区中。
     * 参数 src - 要从中读取字节的源缓冲区；不能为当前的缓冲区
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func put(src: ByteBuffer) : ByteBuffer 
    
    /*
     * 判断是否可通过一个可访问的 byte 数组实现此缓冲区。
     * 返回值 Bool - 如果可访问返回 true
     */
    public func hasArray(): Bool 
    
    /*
     * 返回支持此缓冲区的数组。
     * 此方法旨在允许更有效地将数组支持的缓冲区传递给本机代码。
     * 具体的实现子类会为该方法提供了更强类型的返回值。
     * 对该缓冲区内容的修改将导致返回数组的内容被修改，反之亦然。
     * 在调用此方法之前调用hasArray方法，以确保此缓冲区具有可访问的后备数组。
     * 返回值 Array<UInt8> - 数组
     */
    public func array(): Array<UInt8> 
    
    /*
     * 返回此缓冲区中的第一个元素在缓冲区的底层实现数组中的偏移量 （可选操作）。
     * 如果存在实现此缓冲区的数组，则缓冲区位置 p 对应于数组索引 p + arrayOffset()。
     * 调用此方法之前要调用 hasArray 方法，以确保此缓冲区具有可访问的底层实现数组。
     * 返回值 Int64 - 偏移量
     */
    public func arrayOffset(): Int64 
    
    /*
     * 设置此缓冲区的位置。如果标记(mark)已定义且大于新位置(newPosition)，则丢弃该标记(mark)
     * 参数 newPosition - 新位置值；必须为非负且不大于当前限制(limit)
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func position(newPosition: Int64): ByteBuffer
    
    /*
     * 设置此缓冲区的限制（limit）。
     * 如果位置（position）大于新限制（newlimit），则将其设置为新限制（limit）。
     * 如果标记（mark）已定义且大于新限制，则将丢弃该标记。
     * 参数 newPosition - 新的极限值；必须为非负，且不大于此缓冲区的容量（capacity）
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func limit(newLimit: Int64): ByteBuffer 
    
    /*
     * 将此缓冲区的位置重置为先前标记的位置，调用此方法既不会更改也不会丢弃标记的值。
     * 内部逻辑是把position设置成mark的值，相当于之前做过一个标记，现在要退回到之前标记的位置。
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func reset(): ByteBuffer 
    
    /*
     * 重置初始化状态。它通常用于清除操作。
     * 此方法的功能是所有状态都将返回到初始状态。position=0， mark=-1，limit=capacity；
     * 注意，此方法与flip的区别在于limit的值是不一样的。
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func clear(): Unit
    
    /*
     * 翻转状态，将一个处于存数据状态的缓冲区变为一个处于准备取数据的状态，
     * 也就是让flip之后的position到limit这块区域变成之前的0到position这块
     * limit = position;position = 0;mark = -1;
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func flip(): ByteBuffer
    
    /*
     * 清零，一版用于写完之后的数据，想从起始位置开始读数据的情形
     * 把position设为0，mark设为-1，不改变limit的值。
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func rewind(): ByteBuffer
    
    /*
     * 返回此类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
    
    /*
     * 返回子类的哈希值. 
     * 
     * 返回值 Int64 - 数字类型
     */
    public func hashCode (): Int64
    
    /*
     * 判断两个ByteBuffer是否是一个对象
     * 
     * 参数 thats - 另一个对象
     * 返回值 Bool - 相等返回true
     */
    public operator func == (thats: Buffer): Bool
    
    /*
     * 判断两个ByteBuffer是否是一个对象
     * 
     * 参数 thats - 另一个对象
     * 返回值 Bool - 相等返回false
     */
    public operator func != (thats: Buffer): Bool
    
    /*
     * 实现此缓冲区的数组返回新的字节缓冲区
     * 新缓冲区的内容将从此缓冲区的当前位置开始。
     * 此缓冲区内容的更改在新缓冲区中是可见的，反之亦然；这两个缓冲区的位置、界限和标记值是相互独立的。
     * 新缓冲区的位置将为零，其容量和界限将为此缓冲区中所剩余的字节数量，其标记是不确定的。
     * 当且仅当此缓冲区为直接时，新缓冲区才是直接的，当且仅当此缓冲区为只读时，新缓冲区才是只读的。
     * 返回值 Int64 - 缓存fer
     */
    public func slice(): Buffer 
    
    /*
     * 实现此缓冲区的数组返回新的字节缓冲区, 其内容是此缓冲区内容的共享子序列。
     * 新缓冲区的内容将从该缓冲区中的位置索引开始，并包含长度元素。
     * 对该缓冲区内容的更改将在新缓冲区中可见，反之亦然；两个缓冲区的位置、限制和标记值将是独立的。
     * 参数 index - 此缓冲区的开始索引
     * 参数 length - 长度
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func slice (pos: Int64, limit: Int64): ByteBuffer
    
    /*
     * 创建共享此缓冲区内容的新的字节缓冲区。
     * 新缓冲区的内容将为此缓冲区的内容。
     * 此缓冲区内容的更改在新缓冲区中是可见的，反之亦然；这两个缓冲区的位置、界限和标记值是相互独立的。
     * 新缓冲区的容量、界限、位置和标记值将与此缓冲区相同。
     * 当且仅当此缓冲区为直接时，新缓冲区才是直接的，当且仅当此缓冲区为只读时，新缓冲区才是只读的。
     * 返回值 Buffer - 缓存fer
     */
    public func duplicate (): ByteBuffer
    
    /*
     * 创建共享此缓冲区内容的新的只读字节缓冲区。
     * 新缓冲区的内容将为此缓冲区的内容。
     * 此缓冲区内容的更改在新缓冲区中是可见的，但新缓冲区将是只读的并且不允许修改共享内容。
     * 两个缓冲区的位置、界限和标记值是相互独立的。
     * 新缓冲区的容量、界限、位置和标记值将与此缓冲区相同。
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func asReadOnlyBuffer (): ByteBuffer
    
    /*
     * 获取一个字节
     * 返回值 UInt8 - 类型UInt8
     */
    public func get(): UInt8
    
    /*
     * 读取指定索引处的字节。 
     * 
     * 参数 i - 索引
     * 返回值 Byte - 字节
     */
    public func get (i: Int64): Byte
    
    /*
     * 将此缓冲区的字节传输到给定的目标数组中。
     * 如果此缓冲中剩余的字节少于满足请求所需的字节（即如果 length > remaining()），则不传输字节且抛出Exception。
     * 参数 dst - 向其中写入字节的数组
     * 参数 offset - 要写入的第一个字节在数组中的偏移量；必须为非负且不大于  dst.size
     * 参数 length - 要写入到给定数组中的字节的最大数量；必须为非负且不大于  dst.size - 偏移量
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func get (dsts: Array<Byte>, offset: Int64, length: Int64): ByteBuffer
    
    /*
     * 存储一个字节
     * 参数 x - 类型UInt8
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func put (x: Byte): ByteBuffer
    
    /*
     * 将给定字节写入此缓冲区的给定索引处。如果 index 为负或不小于缓冲区界限, 将抛出 IndexOutOfBoundsException 。
     * 参数 i - 将在该位置写入字节的索引
     * 参数 x - 要写入的字节值
     * 返回值 Buffer - 缓存fer
     */
    public func put (i: Int64, x: Byte): ByteBuffer
    
    /*
     * 将给定src数组中的剩余字节传输到此缓冲区中。
     * 参数 src - 要从中读取字节的数组
     * 参数 offset - 要读取的第一个字节在数组中的偏移量；必须为非负且不大于  array.size
     * 参数 length - 要从给定数组读取的字节的数量；必须为非负且不大于  array.size - 偏移量
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func put (src: Array<Byte>, offsets: Int64, length: Int64): ByteBuffer
    
    /*
     * 将给定src缓冲区中的剩余字节传输到此缓冲区中。
     * 参数 src - 要从中读取字节的源缓冲区；不能为当前的缓冲区
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func put (srcs: ByteBuffer): ByteBuffer
    
    /*
     * 压缩此缓冲区 （可选操作）。
     * 把从 position 到 limit 中的内容移到 0 到 limit - position 的区域内，position 和 limit 的取值也分别变成 limit - position、capacity。
     * 如果先将 positon 设置到 limit，再 compact，那么相当于 clear()
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func compact (): ByteBuffer
    
    /*
     * 存储一个 Int64（八个字节），只支持 Big-Endian（大端）写入操作
     * 参数 value - 数字类型
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func putLong (val: Int64): ByteBuffer
    
    /*
     * 存储一个 Int32（四个字节），只支持 Big-Endian（大端）写入操作
     * 参数 value - 类型int32
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func putInt (val: Int32): ByteBuffer
    
    /*
     * 存储一个 Int16（二个字节），只支持 Big-Endian（大端）写入操作
     * 参数 value - 类型int16
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func putShort (val: Int16): ByteBuffer
    
    /*
     * 获取一个 Int64 值（八个字节）
     * 返回值 Int64 - 数字类型
     */
    public func getLong (): Int64
    
    /*
     * 获取一个 Int32 值（四个字节）
     * 返回值 Int32 - 类型int32
     */
    public func getInt (): Int32
    
    /*
     * 获取一个 Int16 值（二个字节）
     * 返回值 Int16 - 类型int16
     */
    public func getShort (): Int16
    
    /*
     * 存储一个 UInt64（八个字节），只支持 Big-Endian（大端）写入操作
     * 参数 value - 类型UInt64
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func putUInt64 (val: UInt64): ByteBuffer
    
    /*
     * 存储一个 UInt32（四个字节），只支持 Big-Endian（大端）写入操作
     * 参数 value - 类型UInt32
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func putUInt32 (val: UInt32): ByteBuffer
    
    /*
     * 存储一个 UInt16（二个字节），只支持 Big-Endian（大端）写入操作
     * 参数 value - 类型UInt16
     * 返回值 ByteBuffer - 字节Buffer
     */
    public func putUInt16 (val: UInt16): ByteBuffer
    
    /*
     * 获取一个 UInt64 值（八个字节）
     * 返回值 UInt64 - 类型UInt64
     */
    public func getUInt64 (): UInt64
    
    /*
     * 获取一个 UInt32 值（四个字节）
     * 返回值 UInt32 - 类型UInt32
     */
    public func getUInt32 (): UInt32
    
    /*
     * 获取一个 UInt16 值（二个字节）
     * 返回值 UInt16 - 类型UInt16
     */
    public func getUInt16 (): UInt16
}
```
##### 1.8.2.3 BufferOverflowException
```cangjie
public class BufferOverflowException<: Exception {
    
    /*
     * 构造 BufferOverflowException
     * 
     */
    public init ()
    
    /*
     * 异常打印信息
     * 
     * 返回值 message - 字符串
     */
    public override func toString(): String
}
```
## 2 文件解压缩(解包)和压缩(打包)模块功能
### 2.1 介绍

    前置条件：NA
    场景：
    1、支持rar、tar、zlib4cj、zip4cj、bzip2 多种格式的解压缩功能统一接口
    约束：不支持rar5.0格式的解压
    性能： 支持版本几何性能持平
    可靠性： NA
### 2.2 主要接口
#### 2.2.1 archivers.rar 包
##### 2.2.1.1 RarArchiveInputStream
```cangjie
public class RarArchiveInputStream<: ArchiveInputStream {
    
    /*
     * 构造方法
     * 
     * 参数 String - 文件路径
     */
    public init (input: InputStream)
    
    /*
     * 解压操作, 调用此函数之前如果没有设置outPath，会解压到当前目录，设置outpath会解压到outpath目录中。
     *
     * 参数 Array<Byte> - junrar的接口和compress4cj进行整合, 这里的参数是实现InputStream的方法.
     * 返回值 Int64 解压文件字节数
     * 异常 - 如果构造的参数输入流不是一个File, 会抛 NoneValueException
     */
    public func read (arr: Array<Byte>): Int64
    
    /**
     * 设置输出路径，如果路径不存在会自动创建该目录，支持相对和绝对路径。
     *
     * 参数 outPath - 输出路径 压缩操作前设置为压缩结果文件的路径和名称, 解压缩操作前设置为解压结果存放路径
     */
    public func setOutPath (path: String)： Unit
}
```
##### 2.2.1.2 ContentDescription
```cangjie
public class ContentDescription<: Hashable&ToString {
    
    /*
     * 初始化 ContentDescription 类
     * 
     * 参数 path - 路径
     * 参数 size - 描述大小
     */
    public init (path: ?String, size: Int64)
    
    /*
     * 返回子类的哈希值. 
     * 
     * 返回值 Int64 - 哈希值.
     */
    public override func hashCode (): Int64
    
    /*
     * 判断两个对象是否相等
     * 
     * 参数 obj - 另一个对象
     */
    public func equals (obj: Object)
    
    /*
     * 返回子类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public override func toString (): String
}
```
##### 2.2.1.3 LocalFolderExtractor
```cangjie
public class LocalFolderExtractor<: ExtractDestination {
    
    /*
     * 构造 LocalFolderExtractor
     * 
     * 参数 destination - Path
     */
    public init (destination: Path)
    
    /*
     * 创建文件夹
     * 
     * 参数 fh - FileHeader
     * 返回值 ?File - ?File
     * 异常 - 创建path异常时, 会抛 IllegalArgumentException 
     */
    public override func createDirectory (fh: FileHeader): ?File
    
    /*
     * 解压函数
     * 
     * 参数 arch - archive存档类型
     * 参数 fileHeader - fileHeader
     * 返回值 File - File
     */
    public override func extract (arch: Archive, fileHeader: FileHeader): File
}
```
##### 2.2.1.4 Junrar
```cangjie
public class Junrar {
    
    /*
     * 解压文件到目标文件夹中
     * 
     * 参数 rarPath - rar文件的路径
     * 参数 destinationPath - 解压输出路径
     * 返回值 ArrayList<File> - ArrayList<File>
     */
    public static func extract (rarPath: String, destinationPath: String): ArrayList<File>
    
    /*
     * 解压文件到目标文件夹中
     * 
     * 参数 rar - File
     * 参数 destinationFolder - Path
     * 返回值 ArrayList<File> - ArrayList<File>
     */
    public static func extract (rar: File, destinationFolder: Path): ArrayList<File>
    
    /*
     * 解压函数
     * 
     * 参数 destination - ExtractDestination
     * 参数 volumeManager - VolumeManager
     * 返回值 ArrayList<File> - ArrayList<File>
     */
    public static func extract (destination: ExtractDestination, volumeManager: VolumeManager): ArrayList<File>
    
    /*
     * 获取 ContentsDescription 内容描述
     * 
     * 参数 rar - File
     * 返回值 ArrayList<ContentDescription> - ArrayList<ContentDescription>
     */
    public static func getContentsDescription (rar: File): ArrayList<ContentDescription>
}
```
#### 2.2.2 archivers.rar.volume 包
##### 2.2.2.1 VolumeHelper
```cangjie
public class VolumeHelper {
    
    /*
     * 迭代下一卷的名称
     * 
     * 参数 archiveName - 档案名称
     * 参数 oldNumber - 旧编号
     * 返回值 ?String - ?String
     */
    public static func nextVolumeName (archiveName: String, oldNumber: Bool): ?String
}
```
#### 2.2.3 archivers.rar.unpack 包
##### 2.2.3.1 UnpackFilter
```cangjie
// 解压缩筛选器
public class UnpackFilter {
    
    /*
     * 构造 UnpackFilter 对象
     * 
     */
    public init ()

    /*
     * 获取块长度
     * 
     * 返回值 Int32 - 长度值
     */
    public func getBlockLength (): Int32
    
    /*
     * 设置块长度
     * 
     * 参数 blockLength - 长度值
     */
    public func setBlockLength (blockLength: Int32)
    
    /*
     * 获取块开始值
     * 
     * 返回值 Int32 - 值
     */
    public func getBlockStart (): Int32
    
    /*
     * 设置块开始值
     * 
     * 参数 blockStart - 值
     */
    public func setBlockStart (blockStart: Int32)
    
    /*
     * 获取执行计数
     * 
     * 返回值 Int32 - 计数值
     */
    public func getExecCount (): Int32
    
    /*
     * 设置执行计数
     * 
     * 参数 execCount - 计数值
     */
    public func setExecCount (execCount: Int32)
    
    /*
     * 是否是下一个窗口
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isNextWindow (): Bool
    
    /*
     * 设置下一个窗口
     * 
     * 参数 nextWindow - 布尔值值
     */
    public func setNextWindow (nextWindow: Bool)
    
    /*
     * 获取父筛选器
     * 
     * 返回值 Int32 - value值
     */
    public func getParentFilter (): Int32
    
    /*
     * 设置父筛选器
     * 
     * 参数 parentFilter - value值
     */
    public func setParentFilter (parentFilter: Int32)
    
    /*
     * 获取 VMPreparedProgram对象
     * 
     * 返回值 VMPreparedProgram - 对象
     */
    public func getPrg (): VMPreparedProgram
    
    /*
     * 设置 VM PreparedProgram对象
     * 
     * 参数 prg - 对象
     */
    public func setPrg (prg: VMPreparedProgram)
}
```
#### 2.2.4 archivers.rar.unpack.ppm 包
##### 2.2.4.1 RarNode Class API Description
```cangjie
public class RarNode<: Pointer {
    
    /*
     * 构造函数
     * 
     * 参数 mem - array数据
     * 异常 mem参数为空时，后续调用其他成员方法会抛越界异常
     */
    public init (mem: ?Array<Byte>)
    
    /*
     * 获取 next
     * 
     * 返回值 Int32 - value值
     */
    public func getNext (): Int32
    
    /*
     * 设置 next
     * 
     * 参数 next - RarNode 值
     */
    public func setNext (next: RarNode)
    
    /*
     * 设置 next 值
     * 
     * 参数 next - value值
     */
    public func setNext (next: Int32)
    
    /*
     * 返回子类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
##### 2.2.4.2 StateRef Class API Description
```cangjie
public class StateRef {
    
    /*
     * 构造一个 StateRef 对象
     * 
     */
    public init ()
    
    /*
     * 获取 symbol 
     * 
     * 返回值 Int32 - value值
     */
    public func getSymbol (): Int32
    
    /*
     * 设置 symbol 
     * 
     * 参数 symbol - value值
     */
    public func setSymbol (symbol: Int32)
    
    /*
     * 获取 freq
     * 
     * 返回值 Int32 - value值
     */
    public func getFreq (): Int32
    
    /*
     * 设置 freq
     * 
     * 参数 freq - value值
     */
    public func setFreq (freq: Int32)
    
    /*
     * 自增
     * 
     * 参数 dFreq - value值
     */
    public func incFreq (dFreq: Int32)
    
    /*
     * 自减
     * 
     * 参数 dFreq - value值
     */
    public func decFreq (dFreq: Int32)
    
    /*
     * 设置 State
     * 
     * 参数 statePtr - State值
     */
    public func setValues (statePtr: State)
    
    /*
     * 获取 successor
     * 
     * 返回值 Int32 - value值
     */
    public func getSuccessor (): Int32
    
    /*
     * 设置 successor
     * 
     * 参数 successor - value值
     */
    public func setSuccessor (successor: PPMContext)
    
    /*
     * 设置 successor
     * 
     * 参数 successor - value值
     */
    public func setSuccessor (successor: Int32)
    
    /*
     * 返回子类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
##### 2.2.4.3 FreqData Class API Description
```cangjie
public class FreqData<: Pointer {
    
    /*
     * 构造函数
     * 
     * 参数 mem - array数据
     * 异常 mem参数为空时，后续调用其他成员方法会抛越界异常
     */
    public init (mem: ?Array<Byte>)
    
    /*
     * 初始化对象
     * 
     * 参数 mem - 数组对象
     * 返回值 FreqData - 类本身对象
     */
    public func initClass (mem: Array<Byte>): FreqData
    
    /*
     * 获取summfreq
     * 
     * 返回值 Int32 - value
     */
    public func getSummFreq (): Int32
    
    /*
     * 设置summfreq
     * 
     * 参数 summRreq - value值
     */
    public func setSummFreq (summRreq: Int32)
    
    /*
     * 自增summfreq
     * 
     * 参数 dSummFreq - value值
     */
    public func incSummFreq (dSummFreq: Int32)
    
    /*
     * 获取stats
     * 
     * 返回值 Int32 - stats值
     */
    public func getStats (): Int32
    
    /*
     * 设置stats值
     * 
     * 参数 state - stats值
     */
    public func setStats (state: Int32)
    
    /*
     * 设置stats对象
     * 
     * 参数 state - stats对象
     */
    public func setStats (state: State)

    
    /*
     * 返回子类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
##### 2.2.4.4 Archive Class API Description
```cangjie
public class Archive<: Iterable<FileHeader> {
    
    /*
     * 构造 Archive 
     * 
     * 参数 volumeManager - volumeManager
     */
    public init (volumeManager: VolumeManager)
    
    /*
     * 构造 Archive
     * 
     * 参数 volumeManager - VolumeManager
     * 参数 unrarCallback - 解压回调函数
     */
    public init (volumeManager: VolumeManager, unrarCallback: ?UnrarCallback)
    
    /*
     * 构造 Archive
     * 
     * 参数 firstVolume - File
     * 参数 unrarCallback - 解压回调函数
     */
    public init (firstVolume: File, unrarCallback: ?UnrarCallback)
    
    /*
     * 构造 Archive
     * 
     * 参数 rarAsStream - 输入流
     */
    public init (rarAsStream: InputStream)
    
    /*
     * bytesRead 读取函数
     * 
     * 参数 count - count
     */
    public func bytesReadRead (count: Int32)
    
    /*
     * 获取 Rof
     * 
     * 返回值 IReadOnlyAccess - IReadOnlyAccess
     */
    public func getRof (): IReadOnlyAccess
    
    /*
     * 获取 Headers
     * 
     * 返回值 ArrayList<BaseBlock> - ArrayList<BaseBlock>
     */
    public func getHeaders ()
    
    /*
     * 获取 FileHeaders
     * 
     * 返回值 ArrayList<FileHeader> - ArrayList<FileHeader>
     */
    public func getFileHeaders (): ArrayList<FileHeader>
    
    /*
     * 迭代下一个 
     * 
     * 返回值 ?FileHeader - 文件头
     */
    public func nextFileHeader (): ?FileHeader
    
    /*
     * 获取 UnrarCallback
     * 
     * 返回值 ?UnrarCallback - 解压回调函数
     */
    public func getUnrarCallback (): ?UnrarCallback
    
    /*
     * 判断 Encrypted
     * 
     * 返回值 Bool - 布尔值 
     */
    public func isEncrypted (): Bool
    
    /*
     * 解压文件
     * 
     * 参数 hd - 文件头
     * 参数 os - 输出流
     * 异常 - 解码fileHeader失败时, 会抛HeaderNotInArchiveException
     */
    public func extractFile (hd: FileHeader, os: OutputStream)
    
    /*
     * 获取 MainHeader
     * 
     * 返回值 ?MainHeader - MainHeader类型
     */
    public func getMainHeader (): ?MainHeader
    
    /*
     * 判断 OldFormat
     * 
     * 返回值 Bool - 布尔值
     */
    public func isOldFormat (): Bool
    
    /*
     * 关闭资源
     * 
     */
    public func close ()
    
    /*
     * 获取 VolumeManager
     * 
     * 返回值 VolumeManager - VolumeManager类型
     */
    public func getVolumeManager (): VolumeManager
    
    /*
     * 设置 VolumeManager
     * 
     * 参数 volumeManager - VolumeManager类型
     */
    public func setVolumeManager (volumeManager: VolumeManager)
    
    /*
     * 获取 Volume
     * 
     * 返回值 ?Volume - ?Volume类型
     */
    public func getVolume (): ?Volume
    
    /*
     * 设置 Volume
     * 
     * 参数 volume - Volume类型
     */
    public func setVolume (volume: Volume)
    
    /*
     * 返回此类的迭代器. 
     * 
     * 返回值 Iterator<FileHeader> - Iterator<FileHeader>
     */
    public func iterator (): Iterator<FileHeader>
}
```
##### 2.2.4.5 RangeCoder Class API Description
```cangjie
public class RangeCoder {
    
    /*
     * 获取 SubRange
     * 
     * 返回值 SubRange - SubRange类型
     */
    public func getSubRange (): SubRange
    
    /*
     * 初始化 解码器
     * 
     * 参数 unpackRead - Unpack类型
     * 返回值 Unit - 类型Unit
     */
    public func initDecoder (unpackRead: Unpack): Unit
    
    /*
     * 获取 CurrentCount
     * 
     * 返回值 Int32 - CurrentCount值
     */
    public func getCurrentCount (): Int32
    
    /*
     * 获取 CurrentShiftCount
     * 
     * 参数 SHIFT - 改变量
     * 返回值 Int64 - CurrentShiftCount
     */
    public func getCurrentShiftCount (SHIFT: Int32): Int64
    
    /*
     * 将RangeCoder进行解码
     * 
     */
    public func decode (): Unit
    
    /*
     * 将RangeCoder进行标准化解码
     * 
     */
    public func ariDecNormalize (): Unit
    
    /*
     * 返回子类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
##### 2.2.4.6 SubRange Class API Description
```cangjie
public class SubRange {

    /*
     * 获取 HighCount
     * 
     * 返回值 Int64 - HighCount值
     */
    public func getHighCount (): Int64
    
    /*
     * 设置 HighCount
     * 
     * 参数 highCount - HighCount值
     */
    public func setHighCount (highCount: Int64)
    
    /*
     * 获取 LowCount
     * 
     * 返回值 Int64 - LowCount值
     */
    public func getLowCount (): Int64
    
    /*
     * 设置 LowCount
     * 
     * 参数 lowCount - LowCount值
     */
    public func setLowCount (lowCount: Int64)
    
    /*
     * 获取 Scale
     * 
     * 返回值 Int64 - Scale值
     */
    public func getScale (): Int64
    
    /*
     * 设置 Scale
     * 
     * 参数 scale - Scale值
     */
    public func setScale (scale: Int64)
    
    /*
     * 增加 Scale
     * 
     * 参数 dScale - 增加值
     */
    public func incScale (dScale: Int32)

    
    /*
     * 返回子类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
##### 2.2.4.7 InputStreamVolumeManager Class API Description
```cangjie
public class InputStreamVolumeManager<: FileVolumeManager {
    
    /*
     * 构造 InputStreamVolumeManager
     * 
     * 参数 input - 输入流
     */
    public init (input: InputStream)
    
    /*
     * 迭代下一个 
     * 
     * 参数 archive - archive存档类型
     * 参数 lastVolume - 最后一个volume
     * 返回值 ?Volume - ?volume类型
     */
    public override func nextArchive (archive: Archive, lastVolume: ?Volume): ?Volume
}
```
##### 2.2.4.8 Pointer Class API Description
```cangjie
public abstract class Pointer<: ToString {
    
    /*
     * 构造函数
     * 
     * 参数 mem - Option<Array<Byte>>.None  
     * 异常 mem参数为空时，后续调用其他成员方法会抛越界异常
     */
    public init (mem: ?Array<Byte>)
    
    /*
     * 获取地址,索引位置
     * 
     * 返回值 Int32 - 索引位置
     */
    public open func getAddress (): Int32
    
    /*
     * 设置索引位置
     * 
     * 参数 pos - 设置位置索引
     */
    public open func setAddress (pos: Int32)
    
    /*
     * 返回子类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public open func toString (): String
}
```
##### 2.2.4.9 Unpack20 Class API Description
```cangjie
public abstract class Unpack20<: Unpack15 {
}
```
##### 2.2.4.10 ModelPPM Class API Description
```cangjie
public class ModelPPM {
    
    /*
     * 构造 ModelPPM
     * 
     */
    public init ()
    
    /*
     * 获取 SubAlloc
     * 
     * 返回值 SubAllocator - SubAllocator值
     */
    public func getSubAlloc (): SubAllocator
    
    /*
     * 解码初始化函数
     * 
     * 参数 unpackRead - Unpack 类
     * 参数 escCh - esc字符
     * 返回值 Bool - 解码初始化成功返回true.
     */
    public func decodeInit (unpackRead: Unpack, escCh: Int32): Bool
    
    /*
     * 解析字符函数
     * 
     * 返回值 Int32 - 被解析出字符的数字表示
     */
    public open func decodeChar (): Int32
    
    /*
     * 获取 SEE2Cont
     * 
     * 返回值 Array<Array<SEE2Context>> - 数组
     */
    public func getSEE2Cont (): Array<Array<SEE2Context>>
    
    /*
     * 获取 DummySEE2Cont
     * 
     * 返回值 ?SEE2Context - Option<SEE2Context>
     */
    public func getDummySEE2Cont (): ?SEE2Context
    
    /*
     * 获取 InitRL
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getInitRL (): Int32
    
    /*
     * 设置 EscCount
     * 
     * 参数 escCount - Esc计数
     */
    public func setEscCount (escCount: Int32)
    
    /*
     * 获取 EscCount
     * 
     * 返回值 Int32 - Esc计数
     */
    public func getEscCount (): Int32
    
    /*
     * 增加 EscCount
     * 
     * 参数 dEscCount - 增加的值
     */
    public func incEscCount (dEscCount: Int32)
    
    /*
     * 获取 CharMask
     * 
     * 返回值 Array<Int32> - 数组
     */
    public func getCharMask (): Array<Int32>
    
    /*
     * 获取 NumMasked
     * 
     * 返回值 Int32 - NumMasked
     */
    public func getNumMasked (): Int32
    
    /*
     * 设置 NumMasked
     * 
     * 参数 numMasked - NumMasked
     */
    public func setNumMasked (numMasked: Int32)
    
    /*
     * 设置 PrevSuccess
     * 
     * 参数 prevSuccess - prevSuccess
     */
    public func setPrevSuccess (prevSuccess: Int32)
    
    /*
     * 获取 InitEsc
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getInitEsc (): Int32
    
    /*
     * 设置 InitEsc
     * 
     * 参数 initEsc - initEsc
     */
    public func setInitEsc (initEsc: Int32)
    
    /*
     * 设置 RunLength
     * 
     * 参数 runLength - runLength
     */
    public func setRunLength (runLength: Int32)
    
    /*
     * 获取 RunLength
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getRunLength (): Int32
    
    /*
     * 增加 RunLength
     * 
     * 参数 dRunLength - 增加的RunLength值
     */
    public func incRunLength (dRunLength: Int32)
    
    /*
     * 获取 PrevSuccess
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getPrevSuccess (): Int32
    
    /*
     * 获取 HiBitsFlag
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getHiBitsFlag (): Int32
    
    /*
     * 设置 HiBitsFlag
     * 
     * 参数 hiBitsFlag - hiBitsFlag
     */
    public func setHiBitsFlag (hiBitsFlag: Int32)
    
    /*
     * 获取 BinSumm
     * 
     * 返回值 Array<Array<Int32>> - Array<Array<Int32>
     */
    public func getBinSumm (): Array<Array<Int32>>
    
    /*
     * 获取 Coder
     * 
     * 返回值 RangeCoder - RangeCoder
     */
    public func getCoder (): RangeCoder
    
    /*
     * 获取 HB2Flag
     * 
     * 返回值 Array<Int32> - Array<Int32>
     */
    public func getHB2Flag (): Array<Int32>
    
    /*
     * 获取 NS2BSIndx
     * 
     * 返回值 Array<Int32> - Array<Int32>
     */
    public func getNS2BSIndx (): Array<Int32>
    
    /*
     * 获取 NS2Indx
     * 
     * 返回值 Array<Int32> - Array<Int32>
     */
    public func getNS2Indx (): Array<Int32>
    
    /*
     * 获取 FoundState
     * 
     * 返回值 ?State - ?State类型
     */
    public func getFoundState (): ?State
    
    /*
     * 获取 Heap
     * 
     * 返回值 ?Array<Byte> - array数据
     */
    public func getHeap (): ?Array<Byte>
    
    /*
     * 获取 OrderFall
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getOrderFall (): Int32
    
    /*
     * 返回子类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
##### 2.2.4.11 SEE2Context Class API Description
```cangjie
public class SEE2Context<: ToString {

    /*
     * 构造函数
     * 
     */
    public init ()

    /*
     * 构造 SEE2Context 对象
     * 
     * 参数 initVal - value值
     */
    public func initClass (initVal: Int32)
    
    /*
     * 获取 mean
     * 
     * 返回值 Int32 - value值
     */
    public func getMean (): Int32
    
    /*
     * 更新对象内部字段的值
     * 
     */
    public func update ()
    
    /*
     * 获取 count
     * 
     * 返回值 Int32 - value值
     */
    public func getCount (): Int32
    
    /*
     * 设置 count
     * 
     * 参数 count - value值
     */
    public func setCount (count: Int32)
    
    /*
     * 获取 shift
     * 
     */
    public func getShift ()
    
    /*
     * 设置 shift
     * 
     * 参数 shift - value值
     */
    public func setShift (shift: Int32)
    
    /*
     * 获取 summ
     * 
     * 返回值 Int32 - value值
     */
    public func getSumm (): Int32
    
    /*
     * 设置 summ
     * 
     * 参数 summ - value值
     */
    public func setSumm (summ: Int32)
    
    /*
     * 自增 dSumm
     * 
     * 参数 dSumm - value值
     */
    public func incSumm (dSumm: Int32)
    
    /*
     * 返回子类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
##### 2.2.4.12 RarMemBlock Class API Description
```cangjie
public class RarMemBlock<: Pointer {
    
    /*
     * 构造 
     * 
     * 参数 mem - 数据
     * 异常 mem参数为空时，后续调用其他成员方法会抛越界异常
     */
    public init (mem: ?Array<Byte>)
    
    /*
     * 插入 
     * 
     * 参数 p - RarMemBlock块
     */
    public func insertAt (p: RarMemBlock)
    
    /*
     * 
     * 
     */
    public func remove ()
    
    /*
     * 获取 Next
     * 
     * 返回值 Int32 - next大小
     */
    public func getNext (): Int32
    
    /*
     * 设置 Next
     * 
     * 参数 next - next大小
     */
    public func setNext (next: RarMemBlock)
    
    /*
     * 设置 Next
     * 
     * 参数 next - next大小
     */
    public func setNext (next: Int32)
    
    /*
     * 获取 NU
     * 
     * 返回值 Int32 - NU值
     */
    public func getNU (): Int32
    
    /*
     * 设置 NU
     * 
     * 参数 nu - NU值
     */
    public func setNU (nu: Int32)
    
    /*
     * 获取 Prev
     * 
     * 返回值 Int32 - Prev值
     */
    public func getPrev (): Int32
    
    /*
     * 设置 Prev
     * 
     * 参数 prev - Prev值
     */
    public func setPrev (prev: RarMemBlock)
    
    /*
     * 设置 Prev
     * 
     * 参数 prev - Prev值
     */
    public func setPrev (prev: Int32)
    
    /*
     * 获取 Stamp
     * 
     * 返回值 Int32 - Stamp值
     */
    public func getStamp (): Int32
    
    /*
     * 设置 Stamp
     * 
     * 参数 stamp - Stamp值
     */
    public func setStamp (stamp: Int32)
}
```
##### 2.2.4.13 FileVolumeManager Class API Description
```cangjie
open class FileVolumeManager<: VolumeManager {
    
    /*
     * 构造 
     * 
     * 参数 firstVolume - 第一个vloume
     */
    public init (firstVolume: File)
    
    /*
     * 迭代下一个 
     * 
     * 参数 archive - 存档类
     * 参数 last - 最后一个volume
     * 返回值 ?Volume - 下一个volume
     */
    public open func nextArchive (archive: Archive, last: ?Volume): ?Volume
}
```
##### 2.2.4.14 ComprDataIO Class API Description
```cangjie
public class ComprDataIO {
    
    /*
     * 构造 
     * 
     * 参数 arc - archive存档类型存档器
     */
    public init (arc: Archive)
    
    /*
     * 初始化 ComprDataIO 
     * 
     * 参数 outputStream - 输出流
     */
    public func initClass (outputStream: OutputStream)
    
    /*
     * 初始化类
     * 
     * 参数 hd - FileHeader文件头
     */
    public func initClass (hd: FileHeader)
    
    /*
     * 解码 Read 方法
     * 
     * 参数 addr - 数据
     * 参数 off - 偏移量
     * 参数 cou - 长度
     * 返回值 Int32 - 读取的长度
     */
    public func unpRead (addr: Array<Byte>, off: Int32, cou: Int32): Int32
    
    /*
     * 解码 write 方法
     * 
     * 参数 addr - 数据
     * 参数 offset - 偏移量
     * 参数 count - 长度
     */
    public func unpWrite (addr: Array<Byte>, offset: Int32, count: Int32)
    
    /*
     * 设置 PackedSizeToRead
     * 
     * 参数 size - 大小
     */
    public func setPackedSizeToRead (size: Int64):　Unit
    
    /*
     * 设置 TestMode
     * 
     * 参数 mode - 模式
     */
    public func setTestMode (mode: Bool): Unit
    
    /*
     * 设置 SkipUnpCRC
     * 
     * 参数 skip - 是否跳过
     */
    public func setSkipUnpCRC (skip: Bool): Unit
    
    /*
     * 设置 SubHeader
     * 
     * 参数 hd - 子header
     */
    public func setSubHeader (hd: FileHeader)
    
    /*
     * 获取 CurPackRead
     * 
     * 返回值 Int64 - 读取的值
     */
    public func getCurPackRead (): Int64
    
    /*
     * 设置 CurPackRead
     * 
     * 参数 curPackRead - 读取的值
     */
    public func setCurPackRead (curPackRead: Int64)
    
    /*
     * 获取 CurPackWrite
     * 
     * 返回值 Int64 - 写的值
     */
    public func getCurPackWrite (): Int64
    
    /*
     * 设置 CurPackWrite
     * 
     * 参数 curPackWrite - 写的值
     */
    public func setCurPackWrite (curPackWrite: Int64)
    
    /*
     * 获取 CurUnpRead
     * 
     * 返回值 Int64 - unp的值
     */
    public func getCurUnpRead (): Int64
    
    /*
     * 设置 CurUnpRead
     * 
     * 参数 curUnpRead - unp的值
     */
    public func setCurUnpRead (curUnpRead: Int64)
    
    /*
     * 获取 CurUnpWrite
     * 
     * 返回值 Int64 - UnpWrite的值
     */
    public func getCurUnpWrite (): Int64
    
    /*
     * 设置 CurUnpWrite
     * 
     * 参数 curUnpWrite - UnpWrite的值
     */
    public func setCurUnpWrite (curUnpWrite: Int64)
    
    /*
     * 获取 Decryption
     * 
     * 返回值 Int32 - 解密值
     */
    public func getDecryption (): Int32
    
    /*
     * 设置 Decryption
     * 
     * 参数 decryption - 解密值
     */
    public func setDecryption (decryption: Int32)
    
    /*
     * 获取 Encryption
     * 
     * 返回值 Int32 - 密码值
     */
    public func getEncryption (): Int32
    
    /*
     * 设置 Encryption
     * 
     * 参数 encryption - 密码值
     */
    public func setEncryption (encryption: Int32)
    
    /*
     * 判断 下一个volume是否是缺失
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isNextVolumeMissing (): Bool
    
    /*
     * 设置 NextVolumeMissing
     * 
     * 参数 nextVolumeMissing - 布尔值值
     */
    public func setNextVolumeMissing (nextVolumeMissing: Bool)
    
    /*
     * 获取 PackedCRC
     * 
     * 返回值 Int64 - 包crc值
     */
    public func getPackedCRC (): Int64
    
    /*
     * 设置 PackedCRC
     * 
     * 参数 packedCRC - 包crc值
     */
    public func setPackedCRC (packedCRC: Int64)
    
    /*
     * 获取 PackFileCRC
     * 
     * 返回值 Int64 - 文件的crc值
     */
    public func getPackFileCRC (): Int64
    
    /*
     * 设置 PackFileCRC
     * 
     * 参数 packFileCRC - 文件的crc值
     */
    public func setPackFileCRC (packFileCRC: Int64)
    
    /*
     * 判断 PackVolume
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isPackVolume (): Bool
    
    /*
     * 设置 PackVolume
     * 
     * 参数 packVolume - 包装体积
     */
    public func setPackVolume (packVolume: Bool)
    
    /*
     * 获取 ProcessedArcSize
     * 
     * 返回值 Int64 - 已处理的Arc大小
     */
    public func getProcessedArcSize (): Int64
    
    /*
     * 设置 ProcessedArcSize
     * 
     * 参数 processedArcSize - 已处理的Arc大小
     */
    public func setProcessedArcSize (processedArcSize: Int64)
    
    /*
     * 获取 TotalArcSize
     * 
     * 返回值 Int64 - 总的arc大小
     */
    public func getTotalArcSize (): Int64
    
    /*
     * 设置 TotalArcSize
     * 
     * 参数 totalArcSize - 总的arc大小
     */
    public func setTotalArcSize (totalArcSize: Int64)
    
    /*
     * 获取 TotalPackRead
     * 
     * 返回值 Int64 - 总包读数
     */
    public func getTotalPackRead (): Int64
    
    /*
     * 设置 TotalPackRead
     * 
     * 参数 totalPackRead - 总包读数
     */
    public func setTotalPackRead (totalPackRead: Int64)
    
    /*
     * 获取 UnpArcSize
     * 
     * 返回值 Int64 - Unp的arc大小
     */
    public func getUnpArcSize (): Int64
    
    /*
     * 设置 UnpArcSize
     * 
     * 参数 unpArcSize - Unp的arc大小
     */
    public func setUnpArcSize (unpArcSize: Int64)
    
    /*
     * 获取 UnpFileCRC
     * 
     * 返回值 Int64 - UnpFile的crc值
     */
    public func getUnpFileCRC (): Int64
    
    /*
     * 设置 UnpFileCRC
     * 
     * 参数 unpFileCRC - UnpFile的crc值
     */
    public func setUnpFileCRC (unpFileCRC: Int64)
    
    /*
     * 判断 UnpVolume
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isUnpVolume (): Bool
    
    /*
     * 设置 UnpVolume
     * 
     * 参数 unpVolume - 布尔值值
     */
    public func setUnpVolume (unpVolume: Bool)
    
    /*
     * 获取 SubHeader
     * 
     * 返回值 ?FileHeader - FileHeader的Option
     */
    public func getSubHeader (): ?FileHeader
}
```
##### 2.2.4.15 State Class API Description
```cangjie
public class State<: Pointer {
    
    /*
     * 构造函数
     * 
     * 参数 mem - array数据
     * 异常 mem参数为空时，后续调用其他成员方法会抛越界异常
     */
    public init (mem: ?Array<Byte>)
    
    /*
     * 初始化对象
     * 
     * 参数 mem - 数组对象
     * 返回值 State - 类本身对象
     */
    public func initClass (mem: Array<Byte>): State
    
    /*
     * 获取 symbol 
     * 
     * 返回值 Int32 - value值
     */
    public func getSymbol (): Int32
    
    /*
     * 设置 symbol 
     * 
     * 参数 symbol - value值
     */
    public func setSymbol (symbol: Int32)
    
    /*
     * 获取 freq
     * 
     * 返回值 Int32 - value值
     */
    public func getFreq (): Int32
    
    /*
     * 设置 freq
     * 
     * 参数 freq - value值
     */
    public func setFreq (freq: Int32)
    
    /*
     * 自增
     * 
     * 参数 dFreq - value值
     */
    public func incFreq (dFreq: Int32)
    
    /*
     * 获取 successor
     * 
     * 返回值 Int32 - value值
     */
    public func getSuccessor (): Int32
    
    /*
     * 设置 successor
     * 
     * 参数 successor - value值
     */
    public func setSuccessor (successor: PPMContext)
    
    /*
     * 设置 successor
     * 
     * 参数 successor - value值
     */
    public func setSuccessor (successor: Int32)
    
    /*
     * 设置 State
     * 
     * 参数 state - StateRef 值
     */
    public func setValues (state: StateRef)
    
    /*
     * 设置 State
     * 
     * 参数 ptr - State值
     */
    public func setValues (ptr: State)
    
    /*
     * 自减
     * 
     * 返回值 State - State 自身对象
     */
    public func decAddress (): State
    
    /*
     * 自增
     * 
     * 返回值 State - State 自身对象
     */
    public func incAddress (): State
    
    /*
     * 将两个 state的men内存互换
     * 
     * 参数 ptr1 - State
     * 参数 ptr2 - State
     */
    public static func ppmdSwap (ptr1: State, ptr2: State)
    
    /*
     * 返回此类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
##### 2.2.4.16 Unpack Class API Description
```cangjie
open class Unpack<: Unpack20 {
    
    /*
     * 构造 Unpack
     * 
     * 参数 DataIO - ComprDataIO
     */
    public init (DataIO: ComprDataIO)
    
    /*
     * 初始化 类
     * 
     * 参数 win - Option<Array<Byte>>
     */
    public func initClass (win: ?Array<Byte>)
    
    /*
     * 解码函数
     * 
     * 参数 method - 解码的数字表示方式
     * 参数 solid - 布尔值
     */
    public func doUnpack (method: Int32, solid: Bool)
    
    /*
     * 判断 FileExtracted
     * 
     * 返回值 Bool - FileExtracted
     */
    public func isFileExtracted (): Bool
    
    /*
     * 设置 DestSize
     * 
     * 参数 destSize - DestSize
     */
    public func setDestSize (destSize: Int64)
    
    /*
     * 设置 Suspended
     * 
     * 参数 suspended - Suspended
     */
    public func setSuspended (suspended: Bool)
    
    /*
     * 获取 Char
     * 
     * 返回值 Int32 - Char
     */
    public func getChar (): Int32
    
    /*
     * 获取 PpmEscChar
     * 
     * 返回值 Int32 - PpmEscChar
     */
    public func getPpmEscChar (): Int32
    
    /*
     * 设置 PpmEscChar
     * 
     * 参数 ppmEscChar - PpmEscChar
     */
    public func setPpmEscChar (ppmEscChar: Int32)
    
    /*
     * 清理函数
     * 
     */
    public func cleanUp ()
}
```
##### 2.2.4.17 PPMContext Class API Description
```cangjie
open class PPMContext<: Pointer {
    
    /*
     * 构造函数
     * 
     * 参数 mem - array数据
     * 异常 mem参数为空时，后续调用其他成员方法会抛越界异常
     */
    public init (mem: ?Array<Byte>)
    
    /*
     * 初始化对象
     * 
     * 参数 mem - 数组对象
     * 返回值 FreqData - 类本身对象
     */
    public func initClass (mem: Array<Byte>): PPMContext
    
    /*
     * 获取FreqData
     * 
     * 返回值 Int32 - value
     */
    public func getFreqData (): FreqData
    
    /*
     * 设置 FreqData
     * 
     * 参数 FreqData - value值
     */
    public func setFreqData (freqData: FreqData)
    
    /*
     * 获取summfreq
     * 
     * 返回值 Int32 - value
     */
    public func getNumStats (): Int32
    
    /*
     * 设置summfreq
     * 
     * 参数 summRreq - value值
     */
    public func setNumStats (numStats: Int32)
    
    /*
     * 获取OneState
     * 
     * 返回值 State - 对象
     */
    public func getOneState (): State
    
    /*
     * 设置 oneState
     * 
     * 参数 oneState - 对象
     */
    public func setOneState (oneState: StateRef)
    
    /*
     * 获取 suffix
     * 
     * 返回值 Int32 - value值
     */
    public func getSuffix (): Int32
    
    /*
     * 设置 suffix 
     * 
     * 参数 suffix - PPMContext对象
     */
    public func setSuffix (suffix: PPMContext)
    
    /*
     * 设置 suffix 
     * 
     * 参数 suffix - value值
     */
    public func setSuffix (suffix: Int32)
    
    /*
     * 设置 地址位置
     * 
     * 参数 pos - value值
     */
    public func setAddress (pos: Int32)
    
    /*
     * 创建子项函数
     * 
     * 参数 model - 模式l
     * 参数 pStats - pStats
     * 参数 firstState - firstState
     * 返回值 Int32 - 返回子项的postion
     */
    public func createChild (model: ModelPPM, pStats: State, firstState: StateRef): Int32
    
    /*
     * 重新调整函数
     * 
     * 参数 model - 模式l
     */
    public func rescale (model: ModelPPM)
    
    /*
     * 获取一个中间的数
     * 
     * 参数 summ - summ
     * 参数 shift - shift
     * 参数 round - round
     * 返回值 Int32 - ((summ + (1 << (shift - round))) >>> (shift))的值
     */
    public func getMean (summ: Int32, shift: Int32, round: Int32): Int32
    
    /*
     * 解码二进制符号
     * 
     * 参数 model - 模式l
     */
    public func decodeBinSymbol (model: ModelPPM)
    
    /*
     * 更新函数1
     * 
     * 参数 model - 模式l
     * 参数 p - potsion
     */
    public func update1 (model: ModelPPM, p: Int32)
    
    /*
     * 解码符号2
     * 
     * 参数 model - 模式l
     * 返回值 Bool - 解码成功返回true, 失败返回false
     */
    public func decodeSymbol2 (model: ModelPPM): Bool
    
    /*
     * 更新函数2
     * 
     * 参数 model - 模式l
     * 参数 p - potsion
     */
    public func update2 (model: ModelPPM, p: Int32)
    
    /*
     * 解码符号1
     * 
     * 参数 model - 模式l
     * 返回值 Bool - 解码成功返回true, 失败返回false
     */
    public func decodeSymbol1 (model: ModelPPM): Bool
    
    /*
     * 返回此类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
##### 2.2.4.18 InputStreamVolume Class API Description
```cangjie
public class InputStreamVolume<: FileVolume {
    
    /*
     * 构造 InputStreamVolume
     * 
     * 参数 archive - archive存档类型
     * 参数 inputStream - 输入流
     */
    public init (archive: Archive, inputStream: InputStream)
    
    /*
     * 获取 ReadOnlyAccess
     * 
     * 返回值 IReadOnlyAccess - IReadOnlyAccess
     */
    public func getReadOnlyAccess (): IReadOnlyAccess
    
    /*
     * 获取输入流的 Length
     * 
     * 返回值 Int64 - Length
     */
    public func getLength (): Int64
    
    /*
     * 获取 Archive
     * 
     * 返回值 Archive - archive存档类型
     */
    public func getArchive (): Archive
}
```
##### 2.2.4.19 FileVolume Class API Description
```cangjie
open public class FileVolume<: Volume {
    
    /*
     * 构造 FileVolume
     * 
     * 参数 archive - archive存档类型
     * 参数 file - 文件
     */
    public init (archive: Archive, file: File)
    
    /*
     * 获取 ReadOnlyAccess
     * 
     * 返回值 IReadOnlyAccess - IReadOnlyAccess
     */
    public override func getReadOnlyAccess (): IReadOnlyAccess
    
    /*
     * 获取 Length
     * 
     * 返回值 Int64 - Length
     */
    public override func getLength (): Int64
    
    /*
     * 获取 Archive
     * 
     * 返回值 Archive - archive存档类型
     */
    public override func getArchive (): Archive
    
    /*
     * 获取 File
     * 
     * 返回值 File - File
     */
    public func getFile (): File
}
```
##### 2.2.4.20 SubAllocator Class API Description
```cangjie
open class SubAllocator {
    
    /*
     * 构造 SubAllocator
     * 
     */
    public init ()
    
    /*
     * 清理函数
     * 
     */
    public func clean ()
    
    /*
     * 自增 pText +1 
     * 
     */
    public func incPText ()
    
    /*
     * 停止并清空 SubAllocator
     * 
     */
    public func stopSubAllocator ()
    
    /*
     * 获取 AllocatedMemory
     * 
     * 返回值 Int32 - AllocatedMemory
     */
    public func GetAllocatedMemory (): Int32
    
    /*
     * 开始 SubAllocator
     * 
     * 参数 SASize - SASize
     * 返回值 Bool - 布尔值
     */
    public func startSubAllocator (SASize: Int32): Bool
    
    /*
     * 分配单元函数
     * 
     * 参数 NU - 单元数
     * 返回值 Int32 - 类型int32
     * 异常 如果运算结果超出Int32的范围，会抛RarException
     */
    public func allocUnits (NU: Int32): Int32
    
    /*
     * 分配上下文
     * 
     * 返回值 Int32 - 类型int32
     */
    public func allocContext (): Int32
    
    /*
     * 展开单元
     * 
     * 参数 oldPtr - 类型int32
     * 参数 OldNU - 类型int32
     * 返回值 Int32 - 类型int32
     * 异常 如果运算结果超出Int32的范围，会抛RarException
     */
    public func expandUnits (oldPtr: Int32, OldNU: Int32): Int32
    
    /*
     * 收缩单元
     * 
     * 参数 oldPtr - 老索引值
     * 参数 oldNU - 老NU值
     * 参数 newNU - 新的NU值
     * 返回值 Int32 - 收缩单元的值
     * 异常 如果运算结果超出Int32的范围，会抛RarException
     */
    public func shrinkUnits (oldPtr: Int32, oldNU: Int32, newNU: Int32): Int32
    
    /*
     * 释放单元
     * 
     * 参数 ptr - 类型int32
     * 参数 OldNU - 类型int32
     * 异常 如果运算结果超出Int32的范围，会抛RarException
     */
    public func freeUnits (ptr: Int32, OldNU: Int32)
    
    /*
     * 获取 FakeUnitsStart
     * 
     * 返回值 Int32 - FakeUnitsStart
     */
    public func getFakeUnitsStart (): Int32
    
    /*
     * 设置 FakeUnitsStart
     * 
     * 参数 fakeUnitsStart - FakeUnitsStart
     */
    public func setFakeUnitsStart (fakeUnitsStart: Int32)
    
    /*
     * 获取 HeapEnd
     * 
     * 返回值 Int32 - HeapEnd
     */
    public func getHeapEnd (): Int32
    
    /*
     * 获取 PText
     * 
     * 返回值 Int32 - PText
     */
    public func getPText (): Int32
    
    /*
     * 设置 PText
     * 
     * 参数 text - PText
     */
    public func setPText (text: Int32)
    
    /*
     * 相减 ptext
     * 
     * 参数 dPText - 被相减的值
     */
    public func decPText (dPText: Int32)
    
    /*
     * 获取 UnitsStart
     * 
     * 返回值 Int32 - 类型UnitsStart
     */
    public func getUnitsStart (): Int32
    
    /*
     * 设置 UnitsStart
     * 
     * 参数 unitsStart - 类型UnitsStart
     */
    public func setUnitsStart (unitsStart: Int32)
    
    /*
     * 初始化 SubAllocator
     * 
     * 返回值 Unit - 类型Unit
     */
    public func initSubAllocator (): Unit
    
    /*
     * 获取 Heap
     * 
     * 返回值 ?Array<Byte> - Heap
     */
    public func getHeap (): ?Array<Byte>

    
    /*
     * 返回此类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
#### 2.2.5 archivers.rar.unpack.decode 包
##### 2.2.5.1 CodeType Enum API Description
```cangjie
public enum CodeType {
    | CODE_HUFFMAN
    | CODE_LZ
    | CODE_LZ2
    | CODE_REPEATLZ
    | CODE_CACHELZ
    | CODE_STARTFILE
    | CODE_ENDFILE
    | CODE_VM
    | CODE_VMDATA
}
```
##### 2.2.5.2 AudioVariables Class API Description
```cangjie
// 音频变量
public class AudioVariables {
    public mut prop byteCount: Int32 
    public mut prop d1: Int32 
    public mut prop d2: Int32 
    public mut prop d3: Int32 
    public mut prop d4: Int32 
    public mut prop dif: Array<Int32> 
    public mut prop k1: Int32 
    public mut prop k2: Int32 
    public mut prop k3: Int32 
    public mut prop k4: Int32 
    public mut prop k5: Int32 
    public mut prop lastChar: Int32 
    public mut prop lastDelta: Int32 
}
```
##### 2.2.5.3 Decode Class API Description
```cangjie
// 用于存储lz解码的信息
open class Decode {
}
```
##### 2.2.5.4 DistDecode Class API Description
```cangjie
// 用于存储lz解码的区域信息
public class DistDecode<: Decode {
    
    /*
     * 构造 DistDecode
     * 
     */
    public init ()
}
```
##### 2.2.5.5 MultDecode Class API Description
```cangjie
// 用于存储lz解码的复合信息
public class MultDecode<: Decode {
    
    /*
     * 构造 MultDecode
     * 
     */
    public init ()
}
```
##### 2.2.5.6 LowDistDecode Class API Description
```cangjie
// 用于存储lz解码的低密度信息
public class LowDistDecode<: Decode {
    
    /*
     * 构造 LowDistDecode
     * 
     */
    public init ()
}
```
##### 2.2.5.7 FilterType Enum API Description
```cangjie
public enum FilterType {
    | FILTER_NONE
    | FILTER_PPM
    | FILTER_E8
    | FILTER_E8E9
    | FILTER_UPCASETOLOW
    | FILTER_AUDIO
    | FILTER_RGB
    | FILTER_DELTA
    | FILTER_ITANIUM
    | FILTER_E8E9V2
}
```
##### 2.2.5.8 LitDecode Class API Description
```cangjie
// 用于存储lz解码的字母表信息
public class LitDecode<: Decode {
    
    /*
     * 构造 LitDecode
     * 
     */
    public init ()
}
```
##### 2.2.5.9 RepDecode Class API Description
```cangjie
// 用于存储lz解码的rep信息
public class RepDecode<: Decode {
    
    /*
     * 构造 RepDecode
     * 
     */
    public init ()
}
```
##### 2.2.5.10 BitDecode Class API Description
```cangjie
// 用于存储lz解码的bit信息
public class BitDecode<: Decode {
    
    /*
     * 构造 
     * 
     */
    public init ()
}
```
##### 2.2.5.11 Compress Class API Description
```cangjie
public class Compress {
    public static let CODEBUFSIZE: Int32 = 0x4000
    public static let MAXWINSIZE: Int32 = 0x400000
    public static let MAXWINMASK: Int32 = (MAXWINSIZE - 1)
    public static let MAXWINMASK64: Int64 = Int64(MAXWINSIZE - 1)
    public static let LOW_DIST_REP_COUNT: Int32 = 16
    public static let NC: Int32 = 299 
    public static let DC: Int32 = 60
    public static let LDC: Int32 = 17
    public static let RC: Int32 = 28
    public static let HUFF_TABLE_SIZE: Int32 = (NC + DC + RC + LDC)
    public static let BC: Int32 = 20
    public static let NC20: Int32 = 298 
    public static let DC20: Int32 = 48
    public static let RC20: Int32 = 28
    public static let BC20: Int32 = 19
    public static let MC20: Int32 = 257
}
```
##### 2.2.5.12 BitInput Class API Description
```cangjie
open public class BitInput {
    
    /*
     * 构造 BitInput 对象
     * 
     */
    public init ()

    /*
     * 初始化对象的字段缓存
     * 
     */
    public open func InitBitInput ()
    
    /*
     * 添加一个int32的bits
     * 
     * 参数 Bits - bits 值
     */
    public open func addbits (Bits: Int32)
    
    /*
     * 获取一个bits值
     * 
     * 返回值 Int32 - bits 值 
     */
    public open func getbits (): Int32
    
    /*
     * 添加一个bits 值
     * 
     * 参数 Bits - bits 值
     */
    public open func faddbits (Bits: Int32)
    
    /*
     * 获取一个bits值
     * 
     * 返回值 Int32 - bits 值 
     */
    public open func fgetbits (): Int32
    
    /*
     * 判断 incPtr 的入参是否溢出
     * 
     * 参数 IncPtr - 判断的值
     * 返回值 Bool - 溢出返回true
     */
    public open func Overflow (IncPtr: Int32): Bool
    
    /*
     * 返回内部的数组缓存对象
     * 
     * 返回值 Array<Byte> - 数组缓存对象
     */
    public open func getInBuf (): Array<Byte>
}
```
#### 2.2.6 archivers.rar.unpack.vm 包
##### 2.2.6.1 VMStandardFilterSignature Class API Description
```cangjie
public class VMStandardFilterSignature {
    
    /*
     * VMStandardFilterSignature构造函数
     * 
     * 参数 length - 筛选器的长度
     * 参数 crc - crc 校验签名值
     * 参数 myType - VM标准筛选器
     */
    public init (length: Int32, crc: Int32, myType: VMStandardFilters)
    
    /*
     * 获取 CRC 校验签名值
     * 
     * 返回值 Int32 - value值
     */
    public func getCRC (): Int32
    
    /*
     * 设置CRC校验签名值
     * 
     * 参数 crc - value值
     */
    public func setCRC (crc: Int32)
    
    /*
     * 获取筛选器的长度
     * 
     * 返回值 Int32 - 长度值
     */
    public func getLength (): Int32
    
    /*
     * 设置筛选器的长度
     * 
     * 参数 length - 长度值
     */
    public func setLength (length: Int32)
    
    /*
     * 获取筛选器枚举
     * 
     * 返回值 VMStandardFilters - VMStandardFilters
     */
    public func getType (): VMStandardFilters
    
    /*
     * 设置筛选器枚举
     * 
     * 参数 mytype - 标准筛选器
     */
    public func setType (mytype: VMStandardFilters)
}
```
##### 2.2.6.2 VMPreparedCommand Class API Description
```cangjie
public class VMPreparedCommand<: Equatable<VMPreparedCommand> {

    /*
     * 构造 VMPreparedCommand 对象
     * 
     */
    public init ()
    
    /*
     * 返回 byteMode 字节模式
     * 
     * 返回值 Bool - 布尔值 值
     */
    public func isByteMode (): Bool
    
    /*
     * 设置 byteMode 字节模式
     * 
     * 参数 byteMode - 布尔值 值
     */
    public func setByteMode (byteMode: Bool)
    
    /*
     * 返回VM准备的操作数对象
     * 
     * 返回值 VMPreparedOperand - 对象
     */
    public func getOp1 (): VMPreparedOperand
    
    /*
     * 设置VM准备的操作数对象1的值
     * 
     * 参数 op1 - 对象
     */
    public func setOp1 (op1: VMPreparedOperand)
    
    /*
     * 获取VM准备的操作数的对象1
     * 
     * 返回值 VMPreparedOperand - 对象
     */
    public func getOp2 (): VMPreparedOperand
    
    /*
     * 设置VM准备的操作数对象2的值
     * 
     * 参数 op2 - 对象
     */
    public func setOp2 (op2: VMPreparedOperand)
    
    /*
     * 返回操作数代码对象
     * 
     * 返回值 VMCommands - 对象
     */
    public func getOpCode (): VMCommands
    
    /*
     * 设置操作数代码对象
     * 
     * 参数 opCode - 对象
     */
    public func setOpCode (opCode: VMCommands)
    
    /*
     * 判断两个VMPreparedCommand是否相等
     * 
     * 参数 rhs - VMPreparedCommand
     * 返回值 Bool - 相等返回true
     */
    public operator func == (rhs: VMPreparedCommand): Bool
    
    /*
     * 判断两个VMPreparedCommand是否相等
     * 
     * 参数 rhs - VMPreparedCommand
     * 返回值 Bool - 不相等返回true
     */
    public operator func != (rhs: VMPreparedCommand): Bool
}
```
##### 2.2.6.3 VMPreparedProgram Class API Description
```cangjie
public class VMPreparedProgram {
    
    /*
     * 返回 alt 的指令集合
     * 
     * 返回值 ArrayList<VMPreparedCommand> - 集合
     */
    public func getAltCmd (): ArrayList<VMPreparedCommand>
    
    /*
     * 设置 alt 的指令集合
     * 
     * 参数 altCmd - 集合
     */
    public func setAltCmd (altCmd: ArrayList<VMPreparedCommand>)
    
    /*
     * 获取 指令集合
     * 
     * 返回值 ArrayList<VMPreparedCommand> - 集合
     */
    public func getCmd (): ArrayList<VMPreparedCommand>
    
    /*
     * 设置 指令集合
     * 
     * 参数 cmd - 集合
     */
    public func setCmd (cmd: ArrayList<VMPreparedCommand>)
    
    /*
     * 获取 指令个数
     * 
     * 返回值 Int32 - 指令数
     */
    public func getCmdCount (): Int32
    
    /*
     * 设置指令个数
     * 
     * 参数 cmdCount - 指令个数
     */
    public func setCmdCount (cmdCount: Int32)
    
    /*
     * 获取筛选的数据偏移
     * 
     * 返回值 Int32 - 值
     */
    public func getFilteredDataOffset (): Int32
    
    /*
     * 设置筛选的数据偏移
     * 
     * 参数 filteredDataOffset - 值
     */
    public func setFilteredDataOffset (filteredDataOffset: Int32)
    
    /*
     * 获取 筛选的数据大小
     * 
     * 返回值 Int32 - 值
     */
    public func getFilteredDataSize (): Int32
    
    /*
     * 设置筛选的数据大小
     * 
     * 参数 filteredDataSize - 值
     */
    public func setFilteredDataSize (filteredDataSize: Int32)
    
    /*
     * 获取全局数据
     * 
     * 返回值 ArrayList<Byte> - 集合
     */
    public func getGlobalData (): ArrayList<Byte>
    
    /*
     * 设置全局数据
     * 
     * 参数 globalData - 集合
     */
    public func setGlobalData (globalData: ArrayList<Byte>)
    
    /*
     * 获取全局数据
     * 
     * 返回值 Array<Int32> - 数组值
     */
    public func getInitR (): Array<Int32>
    
    /*
     * 设置InitR的值
     * 
     * 参数 initR - 值
     */
    public func setInitR (initR: Array<Int32>)
    
    /*
     * 获取静态数据
     * 
     * 返回值 ArrayList<Byte> - 集合
     */
    public func getStaticData (): ArrayList<Byte>
    
    /*
     * 设置静态数据
     * 
     * 参数 staticData - 集合
     */
    public func setStaticData (staticData: ArrayList<Byte>)
}
```
##### 2.2.6.4 VMOpType Enum API Description
```cangjie
public enum VMOpType<: Equatable<VMOpType> {
    | VM_OPREG
    | VM_OPINT
    | VM_OPREGMEM
    | VM_OPNONE

    /*
     * 获取OpType的数字表示
     * 
     * 返回值 Int32 - 数字表示
     */
    public func getOpType(): Int32

    /*
     * 返回是否和数字表示相同
     * 
     * 参数 bb - 类型int32
     * 返回值 Bool - 相同返回true,否则返回false
     */
    public func equals(opType: Int32): Bool

    /*
     * 使用数字表示查找VMOpType
     * 
     * 参数 Int32 - 类型int32
     * 返回值 ?VMOpType - ?VMOpType类型
     */
    public static func findOpType(opType: Int32): ?VMOpType

    /*
     * 返回是否和VMOpType相同
     * 
     * 参数 VMOpType - VMOpType
     * 返回值 Bool - 相同返回true,否则返回false
     */
    public operator func ==(rhs: VMOpType): Bool

    /*
     * 返回是否和VMOpType相同
     * 
     * 参数 VMOpType - VMOpType
     * 返回值 Bool - 相同返回false, 否则返回true
     */
    public operator func !=(rhs: VMOpType): Bool 
}
```
##### 2.2.6.5 VMStandardFilters Enum API Description
```cangjie
public enum VMStandardFilters<: Equatable<VMStandardFilters> {
    | VMSF_NONE
    | VMSF_E8
    | VMSF_E8E9
    | VMSF_ITANIUM
    | VMSF_RGB
    | VMSF_AUDIO
    | VMSF_DELTA
    | VMSF_UPCASE

    /*
     * 获取VMStandardFilters的数字表示
     * 
     * 返回值 Int32 - 数字表示
     */
    public func getFilter(): Int32

    /*
     * 返回是否和数字表示相同
     * 
     * 参数 bb - 类型int32
     * 返回值 Bool - 相同返回true,否则返回false
     */
    public func equals(filter: Int32): Bool 

    /*
     * 使用数字表示查找 VMStandardFilters
     * 
     * 参数 Int32 - 类型int32
     * 返回值 ?VMStandardFilters - ?VMStandardFilters
     */
    public static func findFilter(filter: Int32): ?VMStandardFilters

    /*
     * 返回是否和VMStandardFilters相同
     * 
     * 返回值 Bool - 相同返回true, 否则返回false
     */
    public operator func ==(rhs: VMStandardFilters): Bool

    /*
     * 返回是否和VMStandardFilters相同
     * 
     * 返回值 Bool - 相同返回false, 否则返回true
     */
    public operator func !=(rhs: VMStandardFilters): Bool 
}
```
##### 2.2.6.6 RarVM Class API Description
```cangjie
open class RarVM<: BitInput {
    
    /*
     * RarVM 的构造函数
     */
    public init ()
    
    /*
     * 初始化RarVM 对象的函数
     */
    public func initClass ()
    
    /*
     * 在men的offest位置设置小端的值
     * 
     * 参数 mem - men数组
     * 参数 offset - 偏移量
     * 参数 value - value 值
     */
    public func setLowEndianValue (mem: Array<Byte>, offset: Int32, value: Int32)
    
    /*
     * 在men的offest位置设置小端的值
     * 
     * 参数 mem - men集合
     * 参数 offset - 偏移量
     * 参数 value - value 值
     */
    public func setLowEndianValue (mem: ArrayList<Byte>, offset: Int32, value: Int32)
    
    /*
     * 执行VMPreparedProgram参数的函数
     * 
     * 参数 prg - VMPreparedProgram对象
     */
    public func execute (prg: VMPreparedProgram)
    
    /*
     * 获取mem数组
     * 
     * 返回值 Array<Byte> - 数组
     */
    public func getMem (): Array<Byte>
    
    /*
     * VM的准备函数
     * 
     * 参数 code - 代码集合
     * 参数 codelen - 代码长度
     * 参数 prg - VMPreparedProgram预备程序对象
     */
    public func prepare (code: Array<Byte>, codelen: Int32, prg: VMPreparedProgram)
    
    /*
     * 读取data函数
     * 
     * 参数 rarVM - BitInput
     * 返回值 Int32 - 返回value值
     */
    public static func ReadData (rarVM: BitInput): Int32
    
    /*
     * 设置data数组在pos索引及其pos后续的值
     * 
     * 参数 position - pos位置索引
     * 参数 data - data源数据
     * 参数 offset - 偏移量
     * 参数 dataSize - 数据范围
     */
    public func setMemory (position: Int32, data: Array<Byte>, offset: Int32, dataSize: Int32)
}
```
##### 2.2.6.7 VMCmdFlags Class API Description
```cangjie
public class VMCmdFlags {
}
```
##### 2.2.6.8 VMCommands Enum API Description
```cangjie
public enum VMCommands {
    | VM_MOV
    | VM_CMP
    | VM_ADD
    | VM_SUB
    | VM_JZ
    | VM_JNZ
    | VM_INC
    | VM_DEC
    | VM_JMP
    | VM_XOR
    | VM_AND
    | VM_OR
    | VM_TEST
    | VM_JS
    | VM_JNS
    | VM_JB
    | VM_JBE
    | VM_JA
    | VM_JAE
    | VM_PUSH
    | VM_POP
    | VM_CALL
    | VM_RET
    | VM_NOT
    | VM_SHL
    | VM_SHR
    | VM_SAR
    | VM_NEG
    | VM_PUSHA
    | VM_POPA
    | VM_PUSHF
    | VM_POPF
    | VM_MOVZX
    | VM_MOVSX
    | VM_XCHG
    | VM_MUL
    | VM_DIV
    | VM_ADC
    | VM_SBB
    | VM_PRINT
    | VM_MOVB
    | VM_MOVD
    | VM_CMPB
    | VM_CMPD
    | VM_ADDB
    | VM_ADDD
    | VM_SUBB
    | VM_SUBD
    | VM_INCB
    | VM_INCD
    | VM_DECB
    | VM_DECD
    | VM_NEGB
    | VM_NEGD
    | VM_STANDARD

    /*
     * 获取VMCommands的数字表示
     * 
     * 返回值 Int32 - 数字表示
     */
    public func getVMCommand(): Int32 

    /*
     * 返回是否和数字表示相同
     * 
     * 参数 bb - 类型int32
     * 返回值 Bool - 相同返回true,否则返回false
     */
    public func equals(vmCommand: Int32): Bool

    /*
     * 使用数字表示查找 VMCommands
     * 
     * 参数 Int32 - 类型int32
     * 返回值 ?VMCommands - ?VMCommands
     */
    public static func findVMCommand(vmCommand: Int32): ?VMCommands
}
```
##### 2.2.6.9 VMFlags Enum API Description
```cangjie
public enum VMFlags {
    | VM_FC
    | VM_FZ
    | VM_FS

    /*
     * 使用数字表示查找 VMFlags
     * 
     * 参数 Int32 - 类型int32
     * 返回值 VMFlags - VMFlags
     */
    public static func findFlag(flag: Int32): ?VMFlags

    /*
     * 返回是否和数字表示相同
     * 
     * 返回值 Bool - 相同返回true,否则返回false
     */
    public func equals(flag: Int32): Bool

    /*
     * 获取VMFlags的数字表示
     * 
     * 返回值 Int32 - 数字表示
     */
    public func getFlag(): Int32
}
```
##### 2.2.6.10 VMPreparedOperand Class API Description
```cangjie
public class VMPreparedOperand {
    
    /*
     * 构造 VMPreparedOperand 对象
     */
    public init ()

    /*
     * 获取base的值
     * 
     * 返回值 Int32 - value 值
     */
    public func getBase (): Int32
    
    /*
     * 设置base的值
     * 
     * 参数 base - value值
     */
    public func setBase (base: Int32)
    
    /*
     * 获取data值
     * 
     * 返回值 Int32 - data值
     */
    public func getData (): Int32
    
    /*
     * 设置data 值
     * 
     * 参数 data - data值 
     */
    public func setData (data: Int32)
    
    /*
     * 获取 VMOpType 值
     * 
     * 返回值 VMOpType - value值
     */
    public func getType (): VMOpType
    
    /*
     * 设置 VMOpType 值
     * 
     * 参数 t - 值
     */
    public func setType (t: VMOpType)
    
    /*
     * 获取偏移量值
     * 
     * 返回值 Int32 - value值
     */
    public func getOffset (): Int32
    
    /*
     * 设置偏移量值
     * 
     * 参数 offset - value值
     */
    public func setOffset (offset: Int32)
}
```
#### 2.2.7 archivers.rar.exception 包
##### 2.2.7.1 EOFException Class API Description
```cangjie
public class EOFException<: Exception {
    
    /*
     * 构造 EOFException
     * 
     */
    public init ()
    
    /*
     * 构造 EOFException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
    
    /*
     * 异常打印信息
     * 
     * 返回值 message - 字符串
     */
    public override func toString(): String
}
```
##### 2.2.7.2 RarException Class API Description
```cangjie
open class RarException<: Exception {
    
    /*
     * 构造 RarException
     * 
     */
    public init ()
    
    /*
     * 构造 RarException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
    
    /*
     * 异常打印信息
     * 
     * 返回值 message - 字符串
     */
    public override func toString(): String
}
```
##### 2.2.7.3 BadRarArchiveException Class API Description
```cangjie
public class BadRarArchiveException<: RarException {
    
    /*
     * 构造 BadRarArchiveException
     * 
     */
    public init ()
    
    /*
     * 构造 BadRarArchiveException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
}
```
##### 2.2.7.4 CorruptHeaderException Class API Description
```cangjie
public class CorruptHeaderException<: RarException {
    
    /*
     * 构造 CorruptHeaderException
     * 
     */
    public init ()
    
    /*
     * 构造 CorruptHeaderException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
}
```
##### 2.2.7.5 CrcErrorException Class API Description
```cangjie
public class CrcErrorException<: RarException {
    
    /*
     * 构造 CrcErrorException
     * 
     */
    public init ()
    
    /*
     * 构造 CrcErrorException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
}
```
##### 2.2.7.6 HeaderNotInArchiveException Class API Description
```cangjie
public class HeaderNotInArchiveException<: RarException {
    
    /*
     * 构造 HeaderNotInArchiveException
     * 
     */
    public init ()
    
    /*
     * 构造 HeaderNotInArchiveException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
}
```
##### 2.2.7.7 InitDeciphererFailedException Class API Description
```cangjie
public class InitDeciphererFailedException<: RarException {
    
    /*
     * 构造 InitDeciphererFailedException
     * 
     */
    public init ()
    
    /*
     * 构造 InitDeciphererFailedException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
}
```
##### 2.2.7.8 MainHeaderNullException Class API Description
```cangjie
public class MainHeaderNullException<: RarException {
    
    /*
     * 构造 MainHeaderNullException
     * 
     */
    public init ()
    
    /*
     * 构造 MainHeaderNullException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
}
```
##### 2.2.7.9 NotRarArchiveException Class API Description
```cangjie
public class NotRarArchiveException<: RarException {
    
    /*
     * 构造 NotRarArchiveException
     * 
     */
    public init ()
    
    /*
     * 构造 NotRarArchiveException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
}
```
##### 2.2.7.10 UnsupportedRarEncryptedException Class API Description
```cangjie
public class UnsupportedRarEncryptedException<: RarException {
    
    /*
     * 构造 UnsupportedRarEncryptedException
     * 
     */
    public init ()
    
    /*
     * 构造 UnsupportedRarEncryptedException
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
}
```
##### 2.2.7.11 UnsupportedRarV5Exception Class API Description
```cangjie
public class UnsupportedRarV5Exception<: RarException {
    
    /*
     * 构造 UnsupportedRarV5Exception
     * 
     */
    public init ()
    
    /*
     * 构造 UnsupportedRarV5Exception
     * 
     * 参数 message - 字符串
     */
    public init (message: String)
}
```
#### 2.2.8 archivers.rar.crc 包
##### 2.2.8.1 RarCRC Class API Description
```cangjie
public class RarCRC {
    
    /*
     * 校验CRC
     * 
     * 参数 startCrc - CRC起始值
     * 参数 data - 数组
     * 参数 offset - 偏移量
     * 参数 count - 计数器
     * 返回值 Int32 - CRC值
     */
    public static func checkCrc (startCrc: Int32, data: Array<Byte>, offset: Int64, count: Int64): Int32
    
    /*
     * 校验旧CRC
     * 
     * 参数 startCrc - CRC起始值
     * 参数 data - 数组
     * 参数 count - 计数器
     * 返回值 Int16 - CRC值
     */
    public static func checkOldCrc (startCrc: Int16, data: Array<Byte>, count: Int64): Int16
}
```
#### 2.2.9 archivers.rar.io 包
##### 2.2.9.1 RandomAccessFile Class API Description
```cangjie
open class RandomAccessFile<: Resource {
    
    /*
     * 创建一个RandomAccessFile
     * 
     * 参数 path - 文件路径
     * 参数 mode - 文件读写方式
     */
    public init (path: String, mode: OpenOption)
    
    /*
     * 构造 RandomAccessFile
     * 
     * 参数 file - Option<File>
     */
    public init (file: ?File)
    
    /*
     * 获取文件读的位置
     * 
     * 返回值 Int64 - 索引位置
     */
    public func getFilePointer (): Int64
    
    /*
     * 设置文件读的位置
     * 
     * 参数 loc - 设置索引
     * 返回值 Unit - 类型Unit
     */
    public func seek (loc: Int64): Unit
    
    /*
     * 读到buffer中
     * 
     * 参数 buffer - 数组
     * 返回值 Int64 - 字节数
     */
    public func read (buffer: Array<Byte>): Int64
    
    /*
     * 读到buffer中
     * 
     * 参数 buffer - 数组
     * 参数 off - 偏移量
     * 参数 count - 字节数
     * 返回值 Int64 - 字节数
     */
    public func read (buffer: Array<Byte>, off: Int64, count: Int64): Int64
    
    /*
     * 全量读到buffer中
     * 
     * 参数 b - 数组
     * 返回值 Unit - 类型Unit
     */
    public func readFully (b: Array<Byte>): Unit
    
    /*
     * buffer中数据写到资源中
     * 
     * 参数 buffer - 数据数组
     * 返回值 Unit - 类型Unit
     */
    public func write (buffer: Array<Byte>): Unit
    
    /*
     * 刷写数据
     * 
     * 返回值 Unit - 类型Unit
     */
    public func flush (): Unit
    
    /*
     * 关闭资源
     * 
     * 返回值 Unit - 类型Unit
     */
    public func close (): Unit
    
    /*
     * 判断 是否关闭
     * 
     * 返回值 Bool - 布尔值
     */
    public func isClosed (): Bool
}
```
##### 2.2.9.2 ReadOnlyAccessByteArray Class API Description
```cangjie
public class ReadOnlyAccessByteArray<: IReadOnlyAccess {
    /*
     * 构造ReadOnlyAccessByteArray对象函数
     * 
     * 参数 file - 文件对象
     */
    public init(let file: Array<Byte>)

    /*
     * 获取ReadOnlyAccessByteArray当前读的位置
     * 
     * 返回值 Int64 - 位置索引
     */
    public func getPosition (): Int64
    
    /*
     * 设置ReadOnlyAccessByteArray读的索引位置
     * 
     * 参数 pos - 索引位置
     * 返回值 Unit - 类型Unit
     */
    public func setPosition (pos: Int64): Unit
    
    /*
     * 读一个字节
     * 
     * 返回值 Byte - 返回一个字节
     */
    public override func readByte (): Byte
    
    /*
     * 读取数据到buffer数组中
     * 
     * 参数 buffer - 目标数组
     * 参数 off - 偏移量
     * 参数 count - 字节数
     * 返回值 Int64 - 返回字节数
     */
    public override func read (buffer: Array<Byte>, off: Int64, count: Int64): Int64
    
    /*
     * 全量读取数据到buffer数组中
     * 
     * 参数 bytes - 目标数组
     * 参数 count - 字节数
     * 返回值 Int64 - 返回字节数
     * 异常 - count等于0时, 会抛 IllegalArgumentException. 
     */
    public override func readFully (bytes: Array<Byte>, count: Int64): Int64
    
    /*
     * 关闭资源
     * 
     * 返回值 Unit - 类型Unit
     */
    public override func close (): Unit
}
```
##### 2.2.9.3 RandomAccessStream Class API Description
```cangjie
public class RandomAccessStream<: InputStream {
    
    /*
     * 构建RandomAccessStream
     * 
     * 参数 inputstream - 输入流
     */
    public init (inputstream: InputStream)
    
    /*
     * 构建RandomAccessStream
     * 
     * 参数 ras - 文件流
     */
    public init (ras: RandomAccessFile)
    
    /*
     * 获取文件读取的位置
     * 
     * 返回值 Int64 - 位置
     */
    public func getLongFilePointer (): Int64
    
    /*
     * 设置文件读取位置
     * 
     * 参数 loc - 位置
     * 返回值 Unit - 类型Unit
     * 异常 - ioc 小于零或者大于内部缓存的大小, 抛 IllegalArgumentException 异常
     */
    public func seek (loc: Int64): Unit
    
    /*
     * 读取一个字节
     * 
     * 返回值 Int64 - 字节
     */
    public func readByte (): Int64
    
    /*
     * 读取数据到buffer中
     * 
     * 参数 buffer - 数组
     * 返回值 Int64 - 字节数
     */
    public override func read (buffer: Array<Byte>): Int64
    
    /*
     * 读取数据到buffer中
     * 
     * 参数 buffer - 数组
     * 参数 off - 偏移量
     * 参数 count - 字节数
     * 返回值 Int64 - 字节数
     */
    public func read (buffer: Array<Byte>, off: Int64, count: Int64): Int64
    
    /*
     * 全量读取数据到bytes中
     * 
     * 参数 bytes - 数组
     * 参数 len - 字节数
     * 返回值 Unit - 类型Unit
     */
    public func readFully (bytes: Array<Byte>, len: Int64): Unit
    
    /*
     * 读取一个int32数据
     * 
     * 返回值 Int32 - 数据
     * 异常 - 读取数据时小于0, 会抛 EOFException. 
     */
    public func readInt (): Int32
    
    /*
     * 读取一个int64数据
     * 
     * 返回值 Int64 - 数字类型
     * 异常 - 读取数据时小于0, 会抛 EOFException. 
     */
    public func readLong (): Int64
    
    /*
     * 读取一个int16数据
     * 
     * 返回值 Int16 - 类型int16
     * 异常 - 读取数据时小于0, 会抛 EOFException. 
     */
    public func readShort (): Int16
    
    /*
     * 关闭资源
     * 
     * 返回值 Unit - 类型Unit
     */
    public func close (): Unit
}
```
##### 2.2.9.4 ReadOnlyAccessInputStream Class API Description
```cangjie
public class ReadOnlyAccessInputStream<: InputStream {
    /*
     * 创建ReadOnlyAccessInputStream
     * 
     * 参数 file - IReadOnlyAccess实现类
     * 参数 startPos - 起始位置
     * 参数 endPos - 结束位置
     */
    public init(let file: IReadOnlyAccess, let startPos: Int64, let endPos: Int64) 

    /*
     * 读取数据到buffer数组中
     * 
     * 参数 buffer - 数组
     * 参数 off - 偏移量
     * 参数 len - 字节数
     * 返回值 Int64 - 字节数
     */
    public func read (buffer: Array<Byte>, off: Int64, len: Int64): Int64
    
    /*
     * 读取数据到b数组中
     * 
     * 参数 b - 数组
     * 返回值 Int64 - 字节数
     */
    public func read (b: Array<Byte>): Int64
    
    /*
     * 关闭资源
     * 
     * 返回值 Unit - 类型Unit
     */
    public func close (): Unit
}
```
##### 2.2.9.5 ReadOnlyAccessFile Class API Description
```cangjie
public class ReadOnlyAccessFile<: RandomAccessFile&IReadOnlyAccess {
    
    /*
     * 创建ReadOnlyAccessFile
     * 
     * 参数 file - Option<FIle>
     */
    public init (file: ?File)
    
    /*
     * 全量读取到bytes中
     * 
     * 参数 bytes - 数组
     * 参数 count - 字节数
     * 返回值 Int64 - 数字类型
     */
    public override func readFully (bytes: Array<Byte>, count: Int64): Int64
    
    /*
     * 获取读取位置
     * 
     * 返回值 Int64 - 位置
     */
    public override func getPosition (): Int64
    
    /*
     * 设置读取位置
     * 
     * 参数 pos - 位置
     * 返回值 Unit - 类型Unit
     */
    public override func setPosition (pos: Int64): Unit
    
    /*
     * 读取一个字节
     * 
     * 返回值 Byte - 字节
     */
    public override func readByte (): Byte
}
```
##### 2.2.9.6 InputStreamReadOnlyAccessFile Class API Description
```cangjie
public class InputStreamReadOnlyAccessFile<: IReadOnlyAccess&InputStream {
    
    /*
     * 创建InputStreamReadOnlyAccessFile
     * 
     * 参数 input - 输入流
     */
    public init (input: InputStream)
    
    /*
     * 获取读的位置
     * 
     * 返回值 Int64 - 位置
     */
    public func getPosition (): Int64
    
    /*
     * 设置读的位置
     * 
     * 参数 pos - 位置
     * 返回值 Unit - 类型Unit
     */
    public func setPosition (pos: Int64): Unit
    
    /*
     * 读取一个字节
     * 
     * 返回值 Byte - 字节
     */
    public override func readByte (): Byte
    
    /*
     * 读取到buffer中
     * 
     * 参数 buffer - 数组
     * 返回值 Int64 - 字节数
     */
    public override func read (buffer: Array<Byte>): Int64
    
    /*
     * 读取到buffer中
     * 
     * 参数 buffer - 数组
     * 参数 off - 偏移量
     * 参数 count - 字节数
     * 返回值 Int64 - 字节数
     */
    public func read (buffer: Array<Byte>, off: Int64, count: Int64): Int64
    
    /*
     * 全量读取到buffer中
     * 
     * 参数 buffer - 数组
     * 参数 count - 字节数
     * 返回值 Int64 - 字节数
     */
    public func readFully (buffer: Array<Byte>, count: Int64): Int64
    
    /*
     * 关闭资源
     * 
     * 返回值 Unit - 类型Unit
     */
    public func close (): Unit
}
```
##### 2.2.9.7 Raw Class API Description
```cangjie
public class Raw {
    
    /*
     * 将指定位置的value增加指定值
     * 
     * 参数 array - 目标数组
     * 参数 pos - 指定位置
     * 参数 dv - 增加的值
     * 返回值 Unit - 类型Unit
     */
    public static func incShortLittleEndian (array: Array<Byte>, pos: Int64, dv: Int64)
    
    /*
     * 按照大端的方式, 从array数组指定位置读取int16
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 返回值 Int16 - 类型int16
     */
    public static func readShortBigEndian (array: Array<Byte>, pos: Int64): Int16
    
    /*
     * 按照大端的方式, 从array数组指定位置读取int32
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 返回值 Int32 - 读取的Int32
     */
    public static func readIntBigEndian (array: Array<Byte>, pos: Int64): Int32
    
    /*
     * 按照大端的方式, 从array数组指定位置读取int64
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 返回值 Int64 - 读取的Int64
     */
    public static func readLongBigEndian (array: Array<Byte>, pos: Int64): Int64
    
    /*
     * 从array数组指定位置读取int16
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 返回值 Int16 - 类型int16
     */
    public static func readShortLittleEndian (array: Array<Byte>, pos: Int64): Int16
    
    /*
     * 从array数组指定位置读取int32
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 返回值 Int32 - 类型int32
     */
    public static func readIntLittleEndian (array: Array<Byte>, pos: Int64): Int32
    
    /*
     * 按照小端的方式, 从array数组指定位置读取4个字节的int32数, 值的类型以int64表示.
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 返回值 Int64 - 数字类型
     */
    public static func readIntLittleEndianAsLong (array: Array<Byte>, pos: Int64): Int64
    
    /*
     * 从array数组指定位置读取int64
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 返回值 Int32 - 数字类型
     */
    public static func readLongLittleEndian (array: Array<Byte>, pos: Int64): Int64
    
    /*
     * 按照大端的方式, 在array数组指定位置写入int16
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 参数 value - 写入值
     * 返回值 Unit - 类型Unit
     */
    public static func writeShortBigEndian (array: Array<Byte>, pos: Int64, value: Int16): Unit
    
    /*
     * 按照大端的方式, 在array数组指定位置写入int32
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 参数 value - 写入值
     * 返回值 Unit - 类型Unit
     */
    public static func writeIntBigEndian (array: Array<Byte>, pos: Int64, value: Int32): Unit
    
    /*
     * 按照大端的方式, 在array数组指定位置写入int64
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 参数 value - 写入值
     * 返回值 Unit - 类型Unit
     */
    public static func writeLongBigEndian (array: Array<Byte>, pos: Int64, value: Int64): Unit
    
    /*
     * 在array数组指定位置写入int16
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 参数 value - 写入值
     * 返回值 Unit - 类型Unit
     */
    public static func writeShortLittleEndian (array: Array<Byte>, pos: Int64, value: Int16): Unit
    
    /*
     * 在array数组指定位置写入int32
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 参数 value - 写入值
     * 返回值 Unit - 类型Unit
     */
    public static func writeIntLittleEndian (array: Array<Byte>, pos: Int64, value: Int32): Unit
    
    /*
     * 在array数组指定位置写入int64
     * 
     * 参数 array - 目标数组
     * 参数 pos - 偏移量
     * 参数 value - 写入值
     * 返回值 Unit - 类型Unit
     */
    public static func writeLongLittleEndian (array: Array<Byte>, pos: Int64, value: Int64): Unit
}
```
#### 2.2.10 archivers.rar.rarfile 包
##### 2.2.10.1 UnrarHeaderType Enum API Description
```cangjie
public enum UnrarHeaderType {
    | MainHead
    | MarkHead
    | FileHead
    | CommHead
    | AvHead
    | SubHead
    | ProtectHead
    | SignHead
    | NewSubHead
    | EndArcHead
    | Null

    /*
     * 获取UnrarHeaderType的数字表示
     * 
     * 返回值 Byte - 数字表示
     */
    public func getValue(): Byte

    /*
     * 使用数字表示查找 UnrarHeaderType
     * 
     * 参数 Byte - 字节
     * 返回值 UnrarHeaderType - 头类型
     */
    public static func findType(heaType: Byte): UnrarHeaderType

    /*
     * 返回是否和数字表示相同
     * 
     * 参数 bb - 字节 
     * 返回值 Bool - 相同返回true,否则返回false
     */
    public func equals(head: Byte): Bool

    /*
     * 获取HostSystem的字符串表示
     * 
     * 返回值 String - 字符串表示
     */
    public func toString(): String
}
```
##### 2.2.10.2 EndArcHeader Class API Description
```cangjie
public class EndArcHeader<: BaseBlock {
    
    /*
     * 构造方法
     * 
     * 参数 bb - 基础块的类型
     * 参数 endArcHeader - 数据头
     */
    public init (bb: BaseBlock, endArcHeader: Array<Byte>)
}
```
##### 2.2.10.3 MainHeader Class API Description
```cangjie
public class MainHeader<: BaseBlock {
    /*
     * 构造方法
     * 
     * 参数 bb - 基础块的类型类
     * 参数 mainHeader - mainHeader数据
     */
    public init (bb: BaseBlock, mainHeader: Array<Byte>)
    
    /* 
     * 判断旧的CMT块存在
     * 
     * 返回值 Bool - 如果有CMT块，则为True
     */
    public func hasArchCmt (): Bool
    
    /*
     * 获取加密的版本
     * 
     * 返回值 Byte - 获取加密的版本
     */
    public func getEncryptVersion (): Byte
    
    /*
     * 获取 HighPosAv
     * 
     * 返回值 Int16 - HighPosAv
     */
    public func getHighPosAv (): Int16
    
    /*
     * 获取 PosAv
     * 
     * 返回值 Int32 - PosAv
     */
    public func getPosAv (): Int32
    
    /*
     * 返回存档是否加密
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isEncrypted (): Bool
    
    /*
     * 归档是否是多卷归档
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isMultiVolume (): Bool
    
    /*
     * 如果归档是多卷归档，则此方法返回此实例是否是多卷归档的第一部分
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isFirstVolume (): Bool
    
    /*
     * 输出属性
     * 
     */
    public func print ()
    
    /*
     * 档案是否可靠
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isSolid (): Bool
    
    /*
     * 是否被锁定
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isLocked (): Bool
    
    /*
     * 是否被保护
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isProtected (): Bool
    
    /*
     * 是否关闭
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isAV (): Bool
    
    /*
     * 是否是新的编号
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isNewNumbering (): Bool
}
```
##### 2.2.10.4 HostSystem Enum API Description
```cangjie
public enum HostSystem {
    | msdos
    | os2
    | win32
    | unix
    | macos
    | beos
    | Null

    /*
     * 获取HostSystem的数字表示
     * 
     * 返回值 Byte - 数字表示
     */
    public func getValue(): Byte

    /*
     * 使用数字表示查找 HostSystem
     * 
     * 参数 Int32 - 类型int32
     * 返回值 HostSystem - HostSystem
     */
    public static func findHostSystem(ByteHost: Byte): HostSystem

    /*
     * 返回是否和数字表示相同
     * 
     * 参数 bb - 类型int16 
     * 返回值 Bool - 相同返回true,否则返回false
     */
    public func equals(ByteHost: Byte): Bool
    
    /*
     * 获取HostSystem的字符串表示
     * 
     * 返回值 String - 字符串表示
     */
    public func toString(): String 
}
```
##### 2.2.10.5 ProtectHeader Class API Description
```cangjie
public class ProtectHeader<: BlockHeader {
    
    /*
     * 构造 ProtectHeader
     * 
     * 参数 bh - 块头
     * 参数 protectHeader - protectHeader
     */
    public init (bh: BlockHeader, protectHeader: Array<Byte>)
    
    /*
     * 获取 Mark
     * 
     * 返回值 Byte - 字节
     */
    public func getMark (): Byte
}
```
##### 2.2.10.6 EAHeader Class API Description
```cangjie
public class EAHeader<: SubBlockHeader {
    
    /*
     * 构造方法
     * 
     * 参数 sb - SubBlockHeader
     * 参数 eaheader - eaheader
     */
    public init (sb: SubBlockHeader, eaheader: Array<Byte>)
    
    /*
     * 获取 earar
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getEACRC (): Int32
    
    /*
     * 获取 method
     * 
     * 返回值 Byte - 字节
     */
    public func getMethod (): Byte
    
    /*
     * 获取 unpSize
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getUnpSize (): Int32
    
    /*
     * unpVer
     * 
     * 返回值 Byte - 字节
     */
    public func getUnpVer (): Byte
    
    /*
     * 打印
     * 
     */
    public func print ()
}
```
##### 2.2.10.7 UnixOwnersHeader Class API Description
```cangjie
public class UnixOwnersHeader<: SubBlockHeader {
    
    /*
     * 构造方法
     * 
     * 参数 sb - SubBlockHeader
     * 参数 uheader - UnixOwnersHeader的数组
     */
    public init (sb: SubBlockHeader, uheader: Array<Byte>)
    
    /*
     * 获取组名
     * 
     * 返回值 String - 字符串
     */
    public func getGroup (): String
    
    /*
     * 获取所有者
     * 
     * 返回值 String - 字符串
     */
    public func getOwner (): String
    
    /*
     * 获取所有者名称长度
     * 
     * 返回值 Int64 - 长度
     */
    public func getOwnerNameSize (): Int64
    
    /*
     * 获取组名称长度
     * 
     * 返回值 Int64 - 长度
     */
    public func getGroupNameSize (): Int64
    
    /*
     * 输出属性
     * 
     */
    public func print (): Unit
}
```
##### 2.2.10.8 NewSubHeaderType Class API Description
```cangjie
public class NewSubHeaderType {
    
    /*
     * 如果给定的字节数组与此头文件的内部字节数组匹配，则返回true
     * 
     * 参数 toCompare - toCompare
     * 返回值 Bool - 布尔值
     */
    public func byteEquals (toCompare: Array<Byte>): Bool
    
    /*
     * 返回此类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
}
```
##### 2.2.10.9 FileHeader Class API Description
```cangjie
open class FileHeader<: BlockHeader {
    
    /*
     * 构造 FileHeader
     * 
     * 参数 bh - 块头
     * 参数 fileHeader - fileHeader
     */
    public init (bh: BlockHeader, fileHeader: Array<Byte>)
    
    /*
     * 打印方法
     * 
     */
    public func print (): Unit
    
    /*
     * 获取 ArcTime
     * 
     * 返回值 DateTime - 文件修改时间
     */
    public func getArcTime (): DateTime
    
    /*
     * 设置 ArcTime
     * 
     * 参数 arcTime - arcTime
     */
    public func setArcTime (arcTime: DateTime)
    
    /*
     * 获取 ATime
     * 
     * 返回值 DateTime - 文件修改时间
     */
    public func getATime (): DateTime
    
    /*
     * 设置 ATime
     * 
     * 参数 tm - 文件修改时间
     */
    public func setATime (tm: DateTime)
    
    /*
     * 获取 CTime
     * 
     * 返回值 DateTime - 文件修改时间
     */
    public func getCTime (): DateTime
    
    /*
     * 设置 CTime
     * 
     * 参数 tm - 文件修改时间
     */
    public func setCTime (tm: DateTime)
    
    /*
     * 获取 FileAttr
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getFileAttr (): Int32
    
    /*
     * 设置 FileAttr
     * 
     * 参数 filea - 类型int32
     */
    public func setFileAttr (filea: Int32)
    
    /*
     * 获取 FileCRC
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getFileCRC (): Int32
    
    /*
     * 获取 HostOS
     * 
     * 返回值 HostSystem - HostSystem
     */
    public func getHostOS (): HostSystem
    
    /*
     * 获取 MTime
     * 
     * 返回值 DateTime - 文件修改时间
     */
    public func getMTime (): DateTime
    
    /*
     * 设置 MTime
     * 
     * 参数 tm - 文件修改时间
     */
    public func setMTime (tm: DateTime)
    
    /*
     * 获取 Salt
     * 
     * 返回值 Array<Byte> - Array<Byte>
     */
    public func getSalt (): Array<Byte>
    
    /*
     * 获取 UnpMethod
     * 
     * 返回值 Byte - UnpMethod
     */
    public func getUnpMethod (): Byte
    
    /*
     * 获取 UnpSize
     * 
     * 返回值 Int64 - UnpSize
     */
    public func getUnpSize (): Int64
    
    /*
     * 获取 UnpVersion
     * 
     * 返回值 Byte - UnpVersion
     */
    public func getUnpVersion (): Byte
    
    /*
     * 获取 FullPackSize
     * 
     * 返回值 Int64 - FullPackSize
     */
    public func getFullPackSize (): Int64
    
    /*
     * 获取 FullUnpackSize
     * 
     * 返回值 Int64 - FullUnpackSize
     */
    public func getFullUnpackSize (): Int64
    
    /*
     * 判断 是否是切割之后
     * 
     * 返回值 Bool - 布尔值
     */
    public func isSplitAfter (): Bool
    
    /*
     * 判断 是否是切割之前
     * 
     * 返回值 Bool - 布尔值
     */
    public func isSplitBefore (): Bool
    
    /*
     * 该文件是否被压缩为实体文件(所有文件作为一个文件处理) 
     * 
     * 返回值 Bool - 布尔值
     */
    public func isSolid (): Bool
    
    /*
     * 判断文件是加密的
     * 
     * 返回值 Bool - 布尔值
     */
    public func isEncrypted (): Bool
    
    /*
     * 判断文件名存在于unicode中
     * 
     * 返回值 Bool - 布尔值
     */
    public func isUnicode (): Bool
    
    /*
     * 判断是文件头 
     * 
     * 返回值 Bool - 布尔值
     */
    public func isFileHeader (): Bool
    
    /*
     * 判断是否有盐
     * 
     * 返回值 Bool - 布尔值
     */
    public func hasSalt (): Bool
    
    /*
     * 判断是大块 
     * 
     * 返回值 Bool - 布尔值
     */
    public func isLargeBlock (): Bool
    
    /*
     * 判断此文件头是否表示目录 
     * 
     * 返回值 Bool - 布尔值
     */
    public func isDirectory (): Bool
    
    /*
     * 获取 文件名
     * 
     * 返回值 String - 字符串
     */
    public func getFileNameW (): String
    
    /*
     * 设置 文件名
     * 
     * 参数 fileNW - 字符串
     */
    public func setFileNameW (fileNW: String)
    
    /*
     * 获取 文件名
     * 
     * 返回值 String - 字符串
     */
    public func getFileNameString (): String
}
```
##### 2.2.10.10 FileNameDecoder Class API Description
```cangjie
public class FileNameDecoder {
    
    /*
     * 获取 Char
     * 
     * 参数 name - 名称
     * 参数 pos - pos
     * 返回值 Int64 - 数字类型
     */
    public static func getChar (name: Array<Byte>, pos: Int64): Int64
    
    /*
     * 解码
     * 
     * 参数 name - 名称
     * 参数 encPos - encPos
     * 返回值 String - 字符串
     */
    public static func decode (name: Array<Byte>, encPos: Int64): String
}
```
##### 2.2.10.11 MacInfoHeader Class API Description
```cangjie
public class MacInfoHeader<: SubBlockHeader {
    
    /*
     * 构造方法 
     * 
     * 参数 sb - SubBlockHeader
     * 参数 macheader - macheader
     */
    public init (sb: SubBlockHeader, macheader: Array<Byte>)
    
    /*
     * 获取文件编写者
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getFileCreator (): Int32
    
    /*
     * 获取文件类型
     * 
     * 返回值 Int32 - 类型int32
     */
    public func getFileType (): Int32
    
    /*
     * 打印
     */
    public func print ()
}
```
##### 2.2.10.12 BaseBlock Class API Description
```cangjie
open class BaseBlock<: Equatable<BaseBlock> {
    
    /*
     * 构造 
     * 
     */
    public init ()
    
    /*
     * 构造 
     * 
     * 参数 bb - 基础块的类型
     */
    public init (bb: BaseBlock)
    
    /*
     * 构造 
     * 
     * 参数 baseBlockHeader - 基础块的类型Header
     */
    public init (baseBlockHeader: Array<Byte>)
    
    /*
     * 是否有存档数据CRC
     * 
     * 返回值 Bool - 布尔值
     */
    public func hasArchiveDataCRC (): Bool
    
    /*
     * 是否有数据卷编号
     * 
     * 返回值 Bool - 布尔值
     */
    public func hasVolumeNumber (): Bool
    
    /*
     * 是否有加密版本
     * 
     * 返回值 Bool - 布尔值
     */
    public func hasEncryptVersion (): Bool
    
    /*
     * 是否为子块 
     * 
     * 返回值 Bool - 布尔值
     */
    public func isSubBlock (): Bool
    
    /*
     * 获取文件中的位置
     * 
     * 返回值 Int64 - 数字类型
     */
    public func getPositionInFile (): Int64
    
    /*
     * 获取标志
     * 
     * 返回值 Int16 - 类型int16
     */
    public func getFlags (): Int16
    
    /*
     * 获取 HeadCRC
     * 
     * 返回值 Int16 - 类型int16
     */
    public func getHeadCRC (): Int16
    
    /*
     * 获取 HeaderSize
     * 
     * 返回值 Int16 - 类型int16
     */
    public func getHeaderSize (): Int16
    
    /*
     * 获取 HeaderType
     * 
     * 返回值 UnrarHeaderType - 头类型
     */
    public func getHeaderType (): UnrarHeaderType
    
    /*
     * 设置文件位置
     * 
     * 参数 positionInFile - positionInFile
     */
    public func setPositionInFile (positionInFile: Int64)
    
    /*
     * 打印方法
     * 
     */
    public open func print ()
    
    /*
     * 判等
     * 
     * 参数 rhs - 基础块的类型
     */
    public operator func == (rhs: BaseBlock)
    
    /*
     * 判不等
     * 
     * 参数 rhs - 基础块的类型
     */
    public operator func != (rhs: BaseBlock)
}
```
##### 2.2.10.13 CommentHeader Class API Description
```cangjie
public class CommentHeader<: BaseBlock {
    
    /*
     * 构造 
     * 
     * 参数 bb - 基础块的类型
     * 参数 commentHeader - commentHeader
     */
    public init (bb: BaseBlock, commentHeader: Array<Byte>)
}
```
##### 2.2.10.14 RARVersion Enum API Description
```cangjie
public enum RARVersion {
    | OLD
    | V4
    | V5
    | Null

    /*
     * 是否是一个老的rar格式
     * 
     * 参数 RARVersion - RARVersion
     * 返回值 bool - 老的返回true
     */
    public static func isOldFormat(version: RARVersion): Bool 

    /*
     * 获取RARVersion的字符串表示
     * 
     * 返回值 String - 字符串表示
     */
    public func toString(): String
}
```
##### 2.2.10.15 AVHeader Class API Description
```cangjie
public class AVHeader<: BaseBlock {
    
    /*
     * 构造 
     * 
     * 参数 bb - 基础块的类型
     * 参数 avHeader - avHeader
     */
    public init (bb: BaseBlock, avHeader: Array<Byte>)
}
```
##### 2.2.10.16 SubBlockHeaderType Enum API Description
```cangjie
public enum SubBlockHeaderType {
    | EA_HEAD
    | UO_HEAD
    | MAC_HEAD
    | BEEA_HEAD
    | NTACL_HEAD
    | STREAM_HEAD
    | Null

    /*
     * 获取SubBlockHeaderType的数字表示
     * 
     * 返回值 Byte - 数字表示
     */
    public func getValue(): Int16

    /*
     * 返回是否和数字表示相同
     * 
     * 返回值 Bool - 相同返回true,否则返回false
     */
    public static func findSubblockHeaderType(sbType: Int16): SubBlockHeaderType

    /*
     * 返回是否和数字表示相同
     * 
     * 参数 bb - 类型int16 
     * 返回值 Bool - 相同返回true,否则返回false
     */
    public func equals(sbtype: Int16): Bool

    /*
     * 获取SubBlockHeaderType的字符串表示
     * 
     * 返回值 String - 字符串表示
     */
    public func toString(): String
}
```
##### 2.2.10.17 SignHeader Class API Description
```cangjie
public class SignHeader<: BaseBlock {
    
    /*
     * 构造 
     * 
     * 参数 bb - 基础块的类型
     * 参数 signHeader - signHeader
     */
    public init (bb: BaseBlock, signHeader: Array<Byte>)
}
```
##### 2.2.10.18 MarkHeader Class API Description
```cangjie
public class MarkHeader<: BaseBlock {
    
    /*
     * 构造 
     * 
     * 参数 bb - 基础块的类型
     */
    public init (bb: BaseBlock)
    
    /*
     * 判断 头文件是否有效
     * 
     * 返回值 Bool - 布尔值
     */
    public func isValid (): Bool
    
    /*
     * 判断 版本是否有效
     * 
     * 返回值 Bool - 布尔值
     */
    public func isSignature (): Bool
    
    /*
     * 判断 是否为旧格式
     * 
     * 返回值 Bool - 布尔值
     */
    public func isOldFormat (): Bool
    
    /*
     * 获取 Version
     * 
     * 返回值 RARVersion - RARVersion
     */
    public func getVersion (): RARVersion
    
    /*
     * 打印
     * 
     */
    public func print ()
}
```
##### 2.2.10.19 SubBlockHeader Class API Description
```cangjie
open class SubBlockHeader<: BlockHeader {
    
    /*
     * 构造 
     * 
     * 参数 sbh - SubBlockHeader
     */
    public init (sbh: SubBlockHeader)
    
    /*
     * 构造 
     * 
     * 参数 bh - 块头
     * 参数 subblock - subblock
     */
    public init (bh: BlockHeader, subblock: Array<Byte>)
    
    /*
     * 获取 Level
     * 
     * 返回值 Byte - 字节
     */
    public func getLevel (): Byte
    
    /*
     * 获取 SubType
     * 
     * 返回值 SubBlockHeaderType - SubBlockHeaderType
     */
    public func getSubType (): SubBlockHeaderType
    
    /*
     * 打印
     * 
     */
    public open func print ()
}
```
##### 2.2.10.20 BlockHeader Class API Description
```cangjie
open class BlockHeader<: BaseBlock {
    
    /*
     * 构造 
     * 
     */
    public init ()
    
    /*
     * 构造 
     * 
     * 参数 bh - 块头
     */
    public init (bh: BlockHeader)
    
    /*
     * 构造 
     * 
     * 参数 bb - 基础块的类型
     * 参数 blockHeader - 块头
     */
    public init (bb: BaseBlock, blockHeader: Array<Byte>)
    
    /*
     * 获取 DataSize
     * 
     * 返回值 Int64 - 数字类型
     */
    public func getDataSize (): Int64
    
    /*
     * 获取 PackSize
     * 
     * 返回值 Int64 - 数字类型
     */
    public func getPackSize (): Int64
    
    /*
     * 打印
     * 
     */
    public open func print ()
}
```
#### 2.2.11 archivers.rar.unsigned 包
##### 2.2.11.1 UnsignedByte Class API Description
```cangjie
public class UnsignedByte {
    
    /*
     * Int64转换为 Int8
     * 
     * 参数 unsignedInt64 - 数字类型
     * 返回值 Int8 - 类型int8
     */
    public static func Int64ToByte (unsignedInt64: Int64): Int8
    
    /*
     * Int32转换为 Int8
     * 
     * 参数 unsignedInt32 - 类型int32
     * 返回值 Int8 - 类型int8
     */
    public static func Int32ToByte (unsignedInt32: Int32): Int8
    
    /*
     * Int16转换为 Int8
     * 
     * 参数 unsignedInt16 - 类型int16
     * 返回值 Int8 - 类型int8
     */
    public static func Int16ToByte (unsignedInt16: Int16): Int8
    
    /*
     * 两个 Int8 值相加 
     * 
     * 参数 unsignedInt8_1 - 类型int8
     * 参数 unsignedInt8_2 - 类型int8
     * 返回值 Int16 - 类型int8
     */
    public static func add (unsignedInt8_1: Int8, unsignedInt8_2: Int8): Int16
    
    /*
     * 两个 Int8 值相减
     * 
     * 参数 unsignedInt8_1 - 类型int8
     * 参数 unsignedInt8_2 - 类型int8
     * 返回值 Int16 - 类型int8
     */
    public static func sub (unsignedInt8_1: Int8, unsignedInt8_2: Int8): Int16
}
```
#### 2.2.12 archivers.tar 包
##### 2.2.12.1 TarArchiveOutputStream Class API Description
```cangjie
public class TarArchiveOutputStream<: ArchiveOutputStream {
    
    /*
     * 构造 TarArchiveOutputStream
     * 
     * 参数 out - 输出流
     * 参数 blockSize - 要使用的块大小。必须是512字节的倍数。
     */
    public init (out: OutputStream, blockSize: Int64)
    
    /*
     * 构造 TarArchiveOutputStream
     * 
     * 参数 out - 输出流
     */
    public init (out: OutputStream)
    
    /*
     * 构造 TarArchiveOutputStream
     * 
     * 参数 out - 输出流
     * 参数 encoding - 用于文件名的编码的名称
     */
    public init (out: OutputStream, encoding: String)
    
    /*
     * 构造 TarArchiveOutputStream
     * 
     * 参数 out - 输出流
     * 参数 blockSize - 要使用的块大小。必须是512字节的倍数。
     * 参数 encoding - 用于文件名的编码的名称
     * 异常 - 类型record的大小 不等于 512 时，抛IllegalArgumentException
     */
    public init (out: OutputStream, blockSize: Int64, recordSize: Int64)
    
    /*
     * 构造 TarArchiveOutputStream
     * 
     * 参数 out - 输出流
     * 参数 blockSize - 要使用的块大小。必须是512字节的倍数。
     * 参数 recordSize - 要使用的记录大小。必须是512字节。
     * 参数 encoding - 要用于文件名的编码的名称
     * 异常 - 类型record的大小 不等于 512 时，抛IllegalArgumentException
     */
    public init (out: OutputStream, blockSize: Int64, recordSize: Int64, encoding: ?String)
    
    /*
     * 构造 TarArchiveOutputStream
     * 
     * 参数 out - 输出流
     * 参数 blockSize - 要使用的块大小。必须是512字节的倍数。
     * 参数 encoding - 要使用的记录大小。必须是512字节。
     */
    public init (out: OutputStream, blockSize: Int64, encoding: ?String)
    
    /*
     * 关闭OutputStream。
     * 
     * 返回值 Unit - 类型Unit 
     */
    public func finish (): Unit
    
    /*
     * 关闭OutputStream。
     * 
     * 返回值 Unit - 类型Unit 
     */
    public override func close (): Unit
    
    /*
     * 在输出流上放入一个条目。这将写入条目的头记录，并定位输出流以写入条目的内容。
     * 一旦调用了这个方法，流就可以调用write（）来写入条目的内容了。写入内容后，必须调用closeArchiveEntry（）以确保所有缓冲的数据都完全写入输出流。
     * 
     * 参数 archiveEntry - 要写入档案的TarEntry。
     * 返回值 Unit - 类型Unit 
     */
    public override func putArchiveEntry (archiveEntry: ArchiveEntry): Unit
    
    /*
     * 关闭一个条目。必须为所有包含数据的文件项调用此方法。原因是我们必须缓冲写入流的数据，以满足缓冲区基于记录的写入。
     * 因此，在关闭该条目和写入下一个条目之前，可能仍有数据片段在组装中，必须将其写入输出流。
     * 
     * 返回值 Unit - 类型Unit 
     */
    public override func closeArchiveEntry (): Unit
    
    /*
     * 以给定的映射作为内容写入PAX扩展标头。
     * 
     * 参数 entry - 文件条目
     * 参数 entryName - 文件名称
     * 参数 headers - 文件和文件头映射
     * 返回值 Unit - 类型Unit
     */
    public func writePaxHeaders (entry: TarArchiveEntry, entryName: String, headers: Map<String,String>): Unit
    
    /*
     * 实现接口的方法，调用outputstrea的flush方法
     * 
     * 返回值 Unit - 类型Unit 
     */
    public func flush (): Unit
    
    /*
     * 实现接口的方法，将数据写入到outputstream中
     * 
     * 参数 record - 数据
     */
    public override func write (record: Array<Byte>)： Unit
    
    /*
     * 设置 BigNumberMode
     * 
     * 参数 bigNumberMode - BigNumberMode
     */
    public func setBigNumberMode (bigNumberMode: Int64)： Unit
    
    /*
     * 获取 RecordSize
     * 
     * 返回值 Int64 - 类型record的大小
     */
    public func getRecordSize (): Int64
    
    /*
     * 设置 LongFileMode
     * 
     * 参数 longFileMode - 文件模式的整型
     */
    public func setLongFileMode (longFileMode: Int64)： Unit
    
    /*
     * 设置 AddPaxHeadersForNonAsciiNames
     * 
     * 参数 b - 布尔值
     */
    public func setAddPaxHeadersForNonAsciiNames (b: Bool)： Unit
}
```
##### 2.2.12.2 TarConstants Class API Description
```cangjie
open class TarConstants {
}
```
##### 2.2.12.3 TarArchiveInputStream Class API Description
```cangjie
public class TarArchiveInputStream<: ArchiveInputStream {
    
    /*
     * 构造 TarArchiveInputStream
     * 
     * 参数 inputStream - 输入流
     */
    public init (inputStream: InputStream)
    
    /*
     * 构造 TarArchiveInputStream
     * 
     * 参数 inputStream - 输入流
     * 参数 encoding - 用于文件名的编码的名称
     */
    public init (inputStream: InputStream, encoding: ?String)
    
    /*
     * 构造 TarArchiveInputStream
     * 
     * 参数 inputStream - 输入流
     * 参数 blockSize - 要使用的块大小
     * 参数 recordSize - 要使用的记录大小
     */
    public init (inputStream: InputStream, blockSize: Int64, recordSize: Int64)
    
    /*
     * 构造 TarArchiveInputStream
     * 
     * 参数 inputStream - 输入流
     * 参数 blockSize - 要使用的块大小
     * 参数 recordSize - 要使用的记录大小
     * 参数 encoding - 用于文件名的编码的名称
     */
    public init (inputStream: InputStream, blockSize: Int64, recordSize: Int64, encoding: ?String)
    
    /*
     * 构造 TarArchiveInputStream
     * 
     * 参数 inputStream - 输入流
     * 参数 blockSize - 要使用的块大小
     * 参数 recordSize - 要使用的记录大小
     * 参数 encoding - 用于文件名的编码的名称
     * 参数 lenient - 当组/userid、模式、设备号和时间戳设置为true非法值时，将忽略这些字段，并将这些字段设置为TarArchiveEntry.UNKNOWN。当设置为false时，这些非法字段会导致异常。
     */
    public init (inputStream: InputStream, blockSize: Int64, recordSize: Int64, encoding: ?String, lenient: Bool)
    
    /*
     * 实现接口方法，读取数据到buffer中。
     * 
     * 参数 buffer - 缓存fer
     * 返回值 Int64 - 读取字节数
     */
    public func read (buffer: Array<Byte>): Int64
    
    /*
     * 获取 RecordSize
     * 
     * 返回值 Int64 - 数字类型
     */
    public func getRecordSize (): Int64
    
    /*
     * 获取可以从存档中的当前条目中读取的可用数据。这并不表示整个归档中还有多少数据，只表示当前条目中的数据。
     * 
     * 返回值 Int64 - 数字类型
     */
    public func available (): Int64
    
    /*
     * 获取 NextTarEntry
     * 
     * 返回值 ?TarArchiveEntry - ?TarArchiveEntry
     */
    public func getNextTarEntry (): ?TarArchiveEntry
    
    /*
     * 获取 NextEntry
     * 
     * 返回值 ?ArchiveEntry - ?ArchiveEntry
     */
    public override func getNextEntry (): ?ArchiveEntry
    
    /*
     * 从当前tar存档项中读取字节。
     * 
     * 参数 buf - 将读取的字节放入其中的缓冲区。
     * 参数 offset - 放置读取字节的偏移量。
     * 参数 numToRead - 要读取的字节数
     * 返回值 Int64 - 读取的字节数，或EOF时为-1
     * 异常 如果读取的偏移量和长度参数不匹配b的范围， 会抛越界异常。
     */
    public func readBytesOffset (buf: Array<Byte>, offset: Int64, numToRead: Int64): Int64
    
    /*
     * 这个类是否能够读取给定的条目。
     * 
     * 参数 ae - archive存档类型Entry
     * 返回值 Bool - 布尔值
     */
    public func canReadEntryData (ae: ArchiveEntry): Bool
    
    /*
     * 获取 CurrentEntry
     * 
     * 返回值 TarArchiveEntry - tar文件条目对象
     */
    public func getCurrentEntry (): TarArchiveEntry
}
```
##### 2.2.12.4 TarIOException Class API Description
```cangjie
public class TarIOException<: Exception {
    
    /*
     * 构造 TarIOException
     * 
     * 参数 message - 异常提示信息
     */
    public init (message: String)
    
    /*
     * 异常打印信息
     * 
     * 返回值 message - 字符串
     */
    public override func toString(): String
}
```
##### 2.2.12.5 TarArchiveStructSparse Class API Description
```cangjie
// 此类表示Tar存档中的结构稀疏。
public class TarArchiveStructSparse {
    
    /*
     * 构造 TarArchiveStructSparse
     * 
     * 参数 offset - 偏移量
     * 参数 numbytes - 序号的字节长度
     */
    public init (offset: Int64, numbytes: Int64)
    
    /*
     * 判等
     * 
     * 参数 that - TarArchiveStructSparse类对象
     * 返回值 Bool - 判等
     */
    public func equals (that: TarArchiveStructSparse): Bool
    
    /*
     * 返回子类的字符串表示. 
     * 
     * 返回值 String - 字符串
     */
    public func toString (): String
    
    /*
     * 获取 Offset
     * 
     * 返回值 Int64 - 偏移量
     */
    public func getOffset (): Int64
    
    /*
     * 获取 Numbytes
     * 
     * 返回值 Int64 - 序号的字节长度
     */
    public func getNumbytes (): Int64
}
```
##### 2.2.12.6 TarArchiveSparseEntry Class API Description
```cangjie
// 这个类表示Tar档案中的一个稀疏文件条目
public class TarArchiveSparseEntry {
    
    /*
     * 构造 TarArchiveSparseEntry
     * 
     * 参数 headerBuf - 文件头缓冲数组
     */
    public init (headerBuf: Array<Byte>)
    
    /*
     * 指示在oldgnu稀疏文件的情况下，是否后面跟着扩展稀疏标头。 
     * 
     * 返回值 Bool - 布尔值
     */
    public func isExtendedFollows (): Bool
    
    /*
     * 获取 SparseHeaders
     * 
     * 返回值 ArrayList<TarArchiveStructSparse> - SparseHeaders
     */
    public func getSparseHeaders (): ArrayList<TarArchiveStructSparse>
}
```
##### 2.2.12.7 TarArchiveEntry Class API Description
```cangjie
public class TarArchiveEntry<: TarConstants&ArchiveEntry&EntryStreamOffsets {
    
    /*
     * 构造 TarArchiveEntry
     * 
     */
    public init ()
    
    /*
     * 构造一个只有名称的条目
     * 
     * 参数 name - 名称
     */
    public init (name: String)
    
    /*
     * 构造一个空条目并准备标头值。
     * 
     * 参数 preserveAbsolutePath - 保留绝对路径
     */
    public init (preserveAbsolutePath: Bool)
    
    /*
     * 构造一个只有名称的条目 
     * 
     * 参数 name - 名称
     * 参数 linkFlag - 连接标记
     */
    public init (name: String, linkFlag: UInt8)
    
    /*
     * 构造一个只有名称的条目。
     * 条目的名称将是name参数的值，所有文件分隔符都由正斜杠替换。如果preserveAbsolutePath为false，则删除前导斜杠和Windows驱动器号。
     * 
     * 参数 name - 名称
     * 参数 linkFlag - 连接标记
     * 参数 preserveAbsolutePath - 保留绝对路径
     */
    public init (name: String, linkFlag: UInt8, preserveAbsolutePath: Bool)
    
    /*
     * 构造一个只有名称的条目
     * 条目的名称将是name参数的值，所有文件分隔符都由正斜杠替换。如果preserveAbsolutePath为false，则删除前导斜杠和Windows驱动器号。
     * 
     * 参数 name - 名称
     * 参数 preserveAbsolutePath - 保留绝对路径
     */
    public init (name: String, preserveAbsolutePath: Bool)
    
    /*
     * 为文件构造一个条目。并且头是根据文件中的信息构建的。
     * 
     * 参数 file - 文件路径
     * 参数 fileName - 文件名称
     */
    public init (file: Path, fileName: String)
    
    /*
     * 给条目设置文件路径和文件名称。
     * 
     * 参数 file - 文件路径
     * 参数 fileName - 文件名称
     * 返回值 Unit - 类型Unit
     */
    public func tarArchiveEntryFile (file: Path, fileName: String): Unit
    
    /*
     * 从存档的头字节构造一个条目
     * 
     * 参数 headerBuf - 来自tar归档条目的标头字节
     * 参数 encoding - 用于文件名的编码
     * 参数 lenient - 当设置为组/用户ID、模式、设备号和时间戳的真正非法值时，将忽略这些值，并将字段设置为UNKNOWN。当设置为false时，这些非法字段会导致异常。
     * 返回值 Unit - 类型Unit
     */
    public func tarArchiveEntryBuf (headerBuf: Array<Byte>, encoding: ZipEncoding, lenient: Bool): Unit
    
    /*
     * tar存档条目链接标记
     * 
     * 参数 name - 名称
     * 参数 linkFlag - 连接标记
     * 返回值 Unit - 类型Unit
     */
    public func tarArchiveEntryLinkFlag (name: String, linkFlag: Byte): Unit
    
    /*
     * 设置tarArchiveEntry
     * 
     * 参数 name - 名称
     * 参数 linkFlag - 连接标记
     * 参数 preserveAbsolutePath - 是否允许名称中的前导斜杠或驱动器号。
     * 返回值 Unit - 类型Unit
     */
    public func tarArchiveEntryPreserveAbsolutePath (name: String, linkFlag: UInt8, preserveAbsolutePath: Bool): Unit
    
    /*
     * 设置tarArchiveEntry
     * 
     * 参数 name - 名称
     * 参数 preserveAbsolutePath - 是否允许名称中的前导斜杠或驱动器号。
     * 返回值 Unit - 类型Unit
     */
    public func tarArchiveEntryPreserveAbsolutePath (name: String, preserveAbsolutePath: Bool): Unit
    
    /*
     * 设置tarArchiveEntry
     * 
     * 参数 file - 路径
     * 参数 fileName - 文件名称
     * 返回值 Unit - 类型Unit
     */
    public func tarArchiveEntryLinkOptions (file: Path, fileName: String): Unit
    
    /*
     * 判等
     * 
     * 参数 it - tar文件条目对象类型对象
     * 返回值 Bool - 判等
     */
    public func equals (it: TarArchiveEntry): Bool
    
    /*
     * 判等
     * 
     * 参数 it - Object类
     * 返回值 Bool - 是否相等
     */
    public func equalsObject (it: Object): Bool
    
    /*
     * 判断 Descendent
     * 
     * 参数 desc - tar的条目
     * 返回值 Bool - 布尔值值
     */
    public func isDescendent (desc: TarArchiveEntry): Bool
    
    /*
     * 获取 Name
     * 
     * 返回值 String - 名称
     */
    public func getName (): String
    
    /*
     * 设置 Name
     * 
     * 参数 name - 名称
     * 返回值 Unit - 类型Unit 
     */
    public func setName (name: String): Unit
    
    /*
     * 设置 Mode
     * 
     * 参数 mode - 模式
     * 返回值 Unit - 类型Unit 
     */
    public func setMode (mode: Int64): Unit
    
    /*
     * 获取 LinkName
     * 
     * 返回值 String - 链接名称
     */
    public func getLinkName (): String
    
    /*
     * 设置 LinkName
     * 
     * 参数 link - 链接名称
     * 返回值 Unit - 类型Unit
     */
    public func setLinkName (link: String): Unit
    
    /*
     * 设置 UserId
     * 
     * 参数 userId - 用户id
     * 返回值 Unit - 类型Unit
     */
    public func setUserId (userId: Int64): Unit
    
    /*
     * 获取 LongUserId
     * 
     * 返回值 Int64 - 长整型的用户id
     */
    public func getLongUserId (): Int64
    
    /*
     * 设置 UserIdLong
     * 
     * 参数 userId - 长整型的用户id
     * 返回值 Unit - 类型Unit
     */
    public func setUserIdLong (userId: Int64): Unit
    
    /*
     * 设置 GroupId
     * 
     * 参数 groupId - 组的id
     * 返回值 Unit - 类型Unit
     */
    public func setGroupId (groupId: Int64): Unit
    
    /*
     * 获取 LongGroupId
     * 
     * 返回值 Int64 - 长整型组的id
     */
    public func getLongGroupId (): Int64
    
    /*
     * 设置 GroupIdLong
     * 
     * 参数 groupId - 长整型组的id
     * 返回值 Unit - 类型Unit 
     */
    public func setGroupIdLong (groupId: Int64): Unit
    
    /*
     * 获取 UserName
     * 
     * 返回值 String - 用户名称
     */
    public func getUserName (): String
    
    /*
     * 设置 UserName
     * 
     * 参数 userName - 用户名称
     * 返回值 Unit - 类型Unit 
     */
    public func setUserName (userName: String): Unit
    
    /*
     * 获取 GroupName
     * 
     * 返回值 String - 组名称
     */
    public func getGroupName (): String
    
    /*
     * 设置 GroupName
     * 
     * 参数 groupName - 组名称
     * 返回值 Unit - 类型Unit 
     */
    public func setGroupName (groupName: String): Unit
    
    /*
     * 设置 Ids
     * 
     * 参数 userId - 用户id
     * 参数 groupId - 组的id
     * 返回值 Unit - 类型Unit 
     */
    public func setIds (userId: Int64, groupId: Int64): Unit
    
    /*
     * 设置 文件所有者Names
     * 
     * 参数 userName - 用户名称
     * 参数 groupName - 组名称
     * 返回值 Unit - 类型Unit 
     */
    public func setNames (userName: String, groupName: String): Unit
    
    /*
     * 设置 文件修改时间
     * 
     * 参数 time - 文件修改时间
     * 返回值 Unit - 类型Unit 
     */
    public func setModTime (time: Int64): Unit
    
    /*
     * 设置 文件修改时间日期
     * 
     * 参数 time - 文件修改时间
     * 返回值 Unit - 类型Unit 
     */
    public func setModTimeDate (time: DateTime): Unit
    
    /*
     * 获取 文件修改时间
     * 
     * 返回值 DateTime - 修改时间
     */
    public func getModTime (): DateTime
    
    /*
     * 获取 最后一次修改时间
     * 
     * 返回值 DateTime - 文件修改时间
     */
    public func getLastModifiedDate (): DateTime
    
    /*
     * 判断 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isCheckSumOK (): Bool
    
    /*
     * 获取 File
     * 
     * 返回值 ?Path - 文件的路径
     */
    public func getFile (): ?Path
    
    /*
     * 获取 Path
     * 
     * 返回值 ?Path - 文件的路径
     */
    public func getPath (): ?Path
    
    /*
     * 获取 Mode
     * 
     * 返回值 Int64 - 数字类型
     */
    public func getMode (): Int64
    
    /*
     * 获取 Size
     * 
     * 返回值 Int64 - 数字类型
     */
    public func getSize (): Int64
    
    /*
     * 设置此条目的稀疏标头
     * 
     * 参数 sparseHeaders - 稀疏标头
     * 返回值 Unit - 类型Unit 
     */
    public func setSparseHeaders (sparseHeaders: ArrayList<TarArchiveStructSparse>): Unit
    
    /*
     * 获取 此条目的稀疏标头
     * 
     * 返回值 ArrayList<TarArchiveStructSparse> - 稀疏标头
     */
    public func getSparseHeaders (): ArrayList<TarArchiveStructSparse>
    
    /*
     * 获取此条目是否为1.X PAX格式的稀疏文件
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isPaxGNU1XSparse (): Bool
    
    /*
     * 设置 Size
     * 
     * 参数 size - 范围大小
     * 返回值 Unit - 类型Unit 
     * 异常 - 范围大小 小于0时, 会抛 IllegalArgumentException
     */
    public func setSize (size: Int64): Unit
    
    /*
     * 获取此条目的主要设备编号。
     * 
     * 返回值 Int64 - 主要设备编号
     */
    public func getDevMajor (): Int64
    
    /*
     * 设置此条目的主要设备编号。
     * 
     * 参数 devNo - 主要设备编号
     * 返回值 Unit - 类型Unit
     */
    public func setDevMajor (devNo: Int64): Unit
    
    /*
     * 获取此条目的次要设备编号。
     * 
     * 返回值 Int64 - 次要设备编号
     */
    public func getDevMinor (): Int64
    
    /*
     * 设置此条目的次要设备编号。
     * 
     * 参数 devNo - 次要设备编号
     * 返回值 Unit - 类型Unit 
     */
    public func setDevMinor (devNo: Int64): Unit
    
    /*
     * 指示在稀疏文件的情况下，是否后面跟着扩展稀疏标头。
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isExtendedFollows (): Bool
    
    /*
     * 获取 RealSize
     * 
     * 返回值 Int64 - RealSize的值
     */
    public func getRealSize (): Int64
    
    /*
     * 如果是稀疏文件，请获取此条目的实际文件大小。
     * 
     * 返回值 Int64 - RealSize实际文件大小。
     */
    public func getRealSize (): Int64
    
    /*
     * 指示此条目是否为GNU稀疏块。
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isGNUSparse (): Bool
    
    /*
     * 指示此条目是使用oldgnu格式的GNU还是星形稀疏块。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isOldGNUSparse (): Bool
    
    /*
     * 指示此条目是否是使用PAX格式之一的GNU稀疏块。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isPaxGNUSparse (): Bool
    
    /*
     * 指示此条目是否是使用PAX标头的星形稀疏块。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isStarSparse (): Bool
    
    /*
     * 指示此条目是否为GNU长链接名块 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isGNULongLinkEntry (): Bool
    
    /*
     * 指示此条目是否为GNU长名称块 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isGNULongNameEntry (): Bool
    
    /*
     * 检查这是否为帕克斯标头。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isPaxHeader (): Bool
    
    /*
     * 检查这是否为帕克斯标头。  
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isGlobalPaxHeader (): Bool
    
    /*
     * 返回此条目是否表示目录。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isDirectory (): Bool
    
    /*
     * 返回此条目是否表示文件。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isFile (): Bool
    
    /*
     * 检查这是否是一个符号链接条目。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isSymbolicLink (): Bool
    
    /*
     * 返回此条目是否表示链接。  
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isLink (): Bool
    
    /*
     * 检查这是否是字符设备条目。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isCharacterDevice (): Bool
    
    /*
     * 检查这是否是块设备条目。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isBlockDevice (): Bool
    
    /*
     * 检查这是否是FIFO（管道）条目。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isFIFO (): Bool
    
    /*
     * 检查这是否是稀疏条目。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isSparse (): Bool
    
    /*
     * 获取 DataOffset
     * 
     * 返回值 Int64 - 数据偏移量值
     */
    public func getDataOffset (): Int64
    
    /*
     * 设置 DataOffset
     * 
     * 参数 dataOffset - 数据偏移量
     * 返回值 Unit - 类型Unit 
     */
    public func setDataOffset (dataOffset: Int64): Unit
    
    /*
     * 指示流是否连续，即不在几个存档部分之间拆分，是否穿插控制块等。 
     * 
     * 返回值 Bool - 布尔值值
     */
    public func isStreamContiguous (): Bool
    
    /*
     * 获取 ExtraPaxHeaders
     * 
     * 返回值 Map<String,String> - 额外pax头的map对象
     */
    public func getExtraPaxHeaders (): Map<String,String>
    
    /*
     * 清除所有多余的PAX标头。
     * 
     * 返回值 Unit - 类型Unit 
     */
    public func clearExtraPaxHeaders (): Unit
    
    /*
     * 将PAX标头添加到此条目。如果标题对应于条目中的现有字段，则会设置该字段；否则，标头将添加到extraPaxHeaders映射中 
     * 
     * 参数 name - 名称
     * 参数 value - 值
     * 返回值 Unit - 类型Unit 
     */
    public func addPaxHeader (name: String, value: String): Unit
    
    /*
     * 获取 ExtraPaxHeader
     * 
     * 参数 name - 标头名
     * 返回值 String - ExtraPaxHeader的字符串 
     */
    public func getExtraPaxHeader (name: String): String
    
    /*
     * 将条目的标头信息写入标头缓冲区。
     * 
     * 参数 outbuf - 标头缓冲区
     * 返回值 Unit - 类型Unit 
     */
    public func writeEntryHeader (outbuf: Array<Byte>): Unit
    
    /*
     * 将条目的标头信息写入标头缓冲区。
     * 
     * 参数 outbuf - 标头缓冲区
     * 参数 encoding - 编码格式
     * 参数 starMode - 如果数值字段的值不符合标准tar档案的最大大小，是否对其使用star/GNU tar/BSD tar扩展
     * 返回值 Unit - 类型Unit 
     */
    public func writeEntryHeader (outbuf: Array<Byte>, encoding: ZipEncoding, starMode: Bool): Unit
    
    /*
     * 从存档的头字节构造一个条目
     * 
     * 参数 header - 头字节的数据
     * 返回值 Unit - 类型Unit 
     */
    public func parseTarHeader (header: Array<Byte>): Unit
    
    /*
     * 获取 OrderedSparseHeaders
     * 
     * 返回值 Array<TarArchiveStructSparse> - Array<TarArchiveStructSparse>
     * 异常 - tar包 解析失败, 会抛 Exception 
     */
    public func getOrderedSparseHeaders (): Array<TarArchiveStructSparse>
}
```
##### 2.2.12.8 TarUtils Class API Description
```cangjie
public class TarUtils {
    
    /*
     * 对于PAX格式0.0，稀疏标头（GNU.spare.offset和GNU.s稀疏.numbytes）可能会出现多次，它们看起来像：GNU.s稀疏.size=size GNU.s稀疏.numblocks=numblocks repeat numblockstimes GNU.s稀疏.foffset=offset GNU.s稀疏.mumbytes=numytes end repeat对于PAX格式化0.1，稀疏标头存储在一个变量中：GNU.sparse.map GNU.sparce.map非空数据块的映射。它是一个由逗号分隔的值“offset，size[，offset-1，size-1…]”组成的字符串
     * 
     * 参数 inputStream - 输入流
     * 参数 sparseHeaders - 用于PAX格式化，因为它可能会出现多次，所以稀疏标头需要存储在数组中，而不是映射中
     * 参数 globalPaxHeaders - tar存档的全局PAX头  // HashMap<String,String>()
     * 返回值 Map<String,String> - 在当前（本地或全局）PAX标头tar条目中找到的PAX标头值的映射。
     */
    public static func parsePaxHeaders (inputStream: InputStream, sparseHeaders: ?ArrayList<TarArchiveStructSparse>, globalPaxHeaders: Map<String,String>): Map<String,String>
    
    /*
     * 对于PAX格式0.0，稀疏标头（GNU.spare.offset和GNU.s稀疏.numbytes）可能会出现多次，它们看起来像：GNU.s稀疏.size=size GNU.s稀疏.numblocks=numblocks repeat numblockstimes GNU.s稀疏.foffset=offset GNU.s稀疏.mumbytes=numytes end repeat对于PAX格式化0.1，稀疏标头存储在一个变量中：GNU.sparse.map GNU.sparce.map非空数据块的映射。它是一个由逗号分隔的值“offset，size[，offset-1，size-1…]”组成的字符串
     * 
     * 参数 inputStream - 输入流
     * 参数 sparseHeaders - 用于PAX格式化，因为它可能会出现多次，所以稀疏标头需要存储在数组中，而不是映射中
     * 参数 globalPaxHeaders - tar存档的全局PAX头
     * 参数 headerSize - PAX标头的总大小，如果为负数，将被忽略
     * 返回值 Map<String,String> - 在当前（本地或全局）PAX标头tar条目中找到的PAX标头值的映射。
     */
    public static func parsePaxHeaders (inputStream: InputStream, sparseHeaders: ?ArrayList<TarArchiveStructSparse>, globalPaxHeaders: Map<String,String>, headerSize: Int64): Map<String,String>
    
    /*
     * 对于PAX格式0.1，稀疏标头存储在单个变量中：GNU.sparse.map GNU.sparce.map非空数据块的映射。它是一个由逗号分隔的值“offset，size[，offset-1，size-1…]”组成的字符串
     * 
     * 参数 sparseMap - 由逗号分隔的值“offset，size[，offset-1，size-1…]”组成的稀疏映射字符串
     * 返回值 ArrayList<TarArchiveStructSparse> - 从稀疏映射解析稀疏标头
     */
    public static func parsePAX01SparseHeaders (sparseMap: String): ArrayList<TarArchiveStructSparse>
    
    /*
     * 对于PAX格式0.1，稀疏标头存储在单个变量中：GNU.sparse.map GNU.sparce.map非空数据块的映射。它是一个由逗号分隔的值“offset，size[，offset-1，size-1…]”组成的字符串
     * 
     * 参数 sparseMap - 类型sparseMap对象
     * 返回值 ArrayList<TarArchiveStructSparse> - 稀疏标头的集合
     */
    public static func parseFromPAX01SparseHeaders (sparseMap: String): ArrayList<TarArchiveStructSparse>
    
    /*
     * 对于PAX格式0.1，稀疏标头存储在单个变量中：GNU.sparse.map GNU.sparce.map非空数据块的映射。它是一个由逗号分隔的值“offset，size[，offset-1，size-1…]”组成的字符串
     * 
     * 参数 inputStream - 输入流
     * 参数 recordSize - 类型record的大小
     * 返回值 ArrayList<TarArchiveStructSparse> - 稀疏标头的集合
     */
    public static func parsePAX1XSparseHeaders (inputStream: InputStream, recordSize: Int64): ArrayList<TarArchiveStructSparse>
    
    /*
     * 将名称复制到缓冲区中。从指定的偏移量开始，将名称中的字符复制到缓冲区中。如果缓冲区比名称长，则缓冲区将填充尾随的NUL。如果名称比缓冲区长，则输出将被截断。
     * 
     * 参数 name - 要从中复制字符的标头名称
     * 参数 buf - 存储名称的缓冲区
     * 参数 offset - 进入缓冲区的起始偏移量
     * 参数 length - 要复制的最大标头字节数
     * 返回值 Int64 - 更新后的偏移量，即偏移量+长度
     */
    public static func formatNameBytes (name: String, buf: Array<Byte>, offset: Int64, length: Int64): Int64
    
    /*
     * 将名称复制到缓冲区中。从指定的偏移量开始，将名称中的字符复制到缓冲区中。如果缓冲区比名称长，则缓冲区将填充尾随的NUL。如果名称比缓冲区长，则输出将被截断。
     * 
     * 参数 name - 要从中复制字符的标头名称
     * 参数 buf - 存储名称的缓冲区
     * 参数 offset - 进入缓冲区的起始偏移量
     * 参数 length - 要复制的最大标头字节数 
     * 参数 encoding - 用于文件名的编码的名称
     * 返回值 Int64 - 更新后的偏移量，即偏移量+长度
     */
    public static func formatNameBytes (name: String, buf: Array<Byte>, offset: Int64, length: Int64, encoding: ZipEncoding): Int64
    
    /*
     * 计算tar条目标头的校验和。
     * 
     * 参数 buf - tar条目的标头缓冲区。
     * 返回值 Int64 - 计算的校验和。
     */
    public static func computeCheckSum (buf: Array<Byte>): Int64
    
    /*
     * 将八进制值写入缓冲区。使用formatUnsignedOctalString将值格式化为带前导零的八进制字符串。转换后的数字后面跟着NUL，然后是空格。
     * 
     * 参数 value - 要转换的值
     * 参数 buf - 目标缓冲区
     * 参数 offset - 缓冲区的起始偏移量。
     * 参数 length - 缓冲区的大小。
     * 返回值 Int64 - 偏移量的更新值，即偏移量+长度
     */
    public static func formatCheckSumOctalBytes (value: Int64, buf: Array<Byte>, offset: Int64, length: Int64): Int64
    
    /*
     * 用无符号八进制数填充缓冲区，并用前导零填充。
     * 
     * 参数 value - 要转换为八进制的数字-视为无符号
     * 参数 buffer - 目的地缓冲区
     * 参数 offset - 缓冲区中的起始偏移
     * 参数 length - 要填充的缓冲区长度
     * 返回值 Unit - 类型Unit 
     */
    public static func formatUnsignedOctalString (value: Int64, buffer: Array<Byte>, offset: Int64, length: Int64): Unit
    
    /*
     * 将一个八进制长整数写入缓冲区。使用formatUnsignedOctalString将值格式化为带前导零的八进制字符串。转换后的数字后面跟一个空格。
     * 
     * 参数 value - 以八进制形式写入的值
     * 参数 buf - 目的缓冲区。
     * 参数 offset - 缓冲区的起始偏移量。
     * 参数 length - 缓冲区的长度
     * 返回值 Int64 - 更新的偏移
     */
    public static func formatLongOctalBytes (value: Int64, buf: Array<Byte>, offset: Int64, length: Int64): Int64
    
    /*
     * 分析缓冲区中的条目名称。当找到NUL或达到缓冲区长度时，解析将停止。
     * 
     * 参数 buffer - 要解析的缓冲区。
     * 参数 offset - 要解析的缓冲区的偏移量。
     * 参数 length - 要解析的最大字节数。
     * 返回值 String - 条目名称。
     */
    public static func parseName (buffer: Array<Byte>, offset: Int64, length: Int64): String
    
    /*
     * 分析缓冲区中的条目名称。当找到NUL或达到缓冲区长度时，解析将停止。
     * 
     * 参数 buffer - 要解析的缓冲区。
     * 参数 offset - 要解析的缓冲区的偏移量。
     * 参数 length - 要解析的最大字节数
     * 参数 encoding - 用于文件名的编码的名称
     * 返回值 String - 条目名称。
     */
    public static func parseNameEncoding (buffer: Array<Byte>, offset: Int64, length: Int64, encoding: ZipEncoding): String
    
    /*
     * 计算字节缓冲区中包含的值。如果缓冲区中第一个字节的最高有效位被设置，则该位被忽略，缓冲区的其余部分被解释为二进制数。否则，根据上面的parseOctal函数，缓冲区将被解释为八进制数。
     * 
     * 参数 buffer - 要解析的缓冲区。
     * 参数 offset - 要解析的缓冲区的偏移量。
     * 参数 length - 要解析的最大字节数。
     * 返回值 Int64 - 八进制或二进制字符串的长值。
     */
    public static func parseOctalOrBinary (buffer: Array<Byte>, offset: Int64, length: Int64): Int64
    
    /*
     * 分析缓冲区中的八进制字符串。
     * 前导空格被忽略。缓冲区必须包含尾随空格或NUL，并且可能包含额外的尾随空格或NULL。
     * 允许输入缓冲区包含所有NUL，在这种情况下，方法返回0L（这允许丢失字段）。
     * 为了解决一些插入前导NUL的tar实现，如果该方法自Commons Compress 1.4以来检测到前导NUL，则返回0。
     * 
     * 参数 buffer - 要解析的缓冲区。
     * 参数 offset - 要解析的缓冲区的偏移量。
     * 参数 length - 要分析的最大字节数-必须至少为2个字节。
     * 返回值 Int64 - 八进制字符串的长值。
     */
    public static func parseOctal (buffer: Array<Byte>, offset: Int64, length: Int64): Int64
    
    /*
     * 维基百科说：
     * 校验和是通过将头块的无符号字节值与八个校验和字节的总和作为ASCII空间（十进制值32）来计算的。它被存储为一个六位数的八进制数，前面有零，后面是NUL，然后是空格。各种实现方式都不遵循这种格式。为了更好的兼容性，请忽略前导和尾随空格，并获取前六位数字。此外，一些历史上的tar实现将字节视为已签名字节。实现通常以两种方式计算校验和，如果有符号或无符号的和与包含的校验和匹配，则将其视为良好。
     * 这种方法的返回值应该被视为尽力而为的启发式方法，而不是绝对的最终真理。校验和验证逻辑很可能随着时间的推移而演变，因为遇到了更多的特殊情况。
     * 
     * 参数 header - 文件tar的头
     * 返回值 Bool - 校验和是否合理良好
     */
    public static func verifyCheckSum (header: Array<Byte>): Bool
    
    /*
     * 分析缓冲区中的布尔字节。忽略前导空格和NUL。缓冲区可能包含尾随空格或NUL。
     * 
     * 参数 buffer - 要解析的缓冲区
     * 参数 offset - 要解析的缓冲区的偏移量。
     * 返回值 Bool - 字节的布尔值。
     */
    public static func parseBoolean (buffer: Array<Byte>, offset: Int64): Bool
    
    /*
     * 如果合适的话，将一个长整数以八进制字符串的形式写入缓冲区，否则以二进制数字的形式写入。使用formatUnsignedOctalString将值格式化为带前导零的八进制字符串。转换后的数字后面跟一个空格。
     * 
     * 参数 value - 要写入缓冲区的值。
     * 参数 buf - 目标缓冲区
     * 参数 offset - 缓冲区的起始偏移量
     * 参数 length - 缓冲区的长度。
     * 返回值 Int64 - 更新后的偏移量。
     */
    public static func formatLongOctalOrBinaryBytes (value: Int64, buf: Array<Byte>, offset: Int64, length: Int64): Int64
    
    /*
     * 分析PAX 1.0稀疏块的内容。
     * 
     * 参数 buffer - 要解析的缓冲区
     * 参数 offset - 要解析的缓冲区的偏移量。
     * 返回值 TarArchiveStructSparse - 已解析的稀疏结构
     */
    public static func parseSparse (buffer: Array<Byte>, offset: Int64): TarArchiveStructSparse
}
```
#### 2.2.13 archivers.zip 包
##### 2.2.13.1 ZipArchiveInputStream Class API Description
```cangjie
public open class ZipArchiveInputStream<: ArchiveInputStream {
    
    /*
     * 构造方法
     * 
     * 参数 String - 文件路径
     */
    public init(filePath: String)
    
    /**
     * 解压操作
     *
     * 参数 Array<Byte>
     * 返回值 Int64 解压后的字节数
     * 异常 IllegalArgumentException - arr为空时抛出异常
     */
    public func read(arr: Array): Int64
    
    /**
     * 设置输出路径
     *
     * 参数 outPath - 输出路径 压缩操作前设置为压缩结果文件的路径和名称, 解压缩操作前设置为解压结果存放路径
     */
    public func setOutPath(outPath: String): Unit
}
```
##### 2.2.13.2 ZipFile Class API Description
```cangjie
public open class ZipFile {
    /*
     * 空参构造函数
     *
     * 返回值 ZipFile对象
     */
    public init()

    /*
     * 构造函数
     *
     * 参数 filePath - 文件路径
     * 返回值 ZipFile对象
     */
    public init(filePath: String)
    
    /*
     * 添加文件
     *
     * 参数 file - 文件路径
     * 异常 ZipException - 文件找不到时抛出
     */
    public func addFile(file: String): Unit
    
    /*
     * 添加文件集合
     *
     * 参数 files - 文件路径集合
     * 异常 ZipException - 集合为空时抛出
     *                     任意文件找不到时抛出
     */
    public func addFiles(files: HashSet<String>): Unit
    
    /*
     * 进行压缩操作
     *
     * 异常 ZipException - 输出路径后缀名不是zip时抛出
     */
    public func writeZip ()
    
    /*
     * 
     * 
     */
    public func zipDIr ()
    
    /*
     * 解压操作
     * 
     * 返回值 ArrayList<LocalFileHeader> - 本地文件头集合, 要解压的文件路径为空/无效时抛出, 解压结果存放路径为空时抛出
     */
    public func extractAll (): ArrayList<LocalFileHeader>
    
    /*
     * 获取被压缩文件的名称列表
     * 
     * 返回值 Array<String> - 被压缩文件的名称列表
     */
    public func nameList (): Array<String>
    
    /*
     * 设置 OutPath
     * 
     * 参数 outPath - 输出流Path
     */
    public func setOutPath (outPath: String)
    
    /*
     * 设置 FilePath
     * 
     * 参数 filePath - 文件路径
     */
    public func setFilePath (filePath: String)
}
```
##### 2.2.13.3 ZipArchiveOutputStream Class API Description
```cangjie
public open class ZipArchiveOutputStream<: ArchiveOutputStream {
    
    /*
     * 构造 ZipArchiveOutputStream
     * 
     * 参数 outPath - 输出流Path
     */
    public init (outPath: String)
    
    /*
     * 写入数据到 ZipArchiveOutputStream
     * 
     * 参数 arr - 需要写的数据
     */
    public func write (arr: Array<Byte>)
    
    /*
     * 添加 文件
     * 
     * 参数 file - 文件路径
     */
    public func addFile (file: String)
}
```
##### 2.2.13.4 ZipException Class API Description
```cangjie
public class ZipException<: Exception {
    
    /*
     * 构造 ZipException
     * 
     * 参数 message - 异常提示信息
     */
    public init (message: String)
    
    /*
     * 异常打印信息
     * 
     * 返回值 message - 字符串
     */
    public override func toString(): String
}
```
##### 2.2.13.5 FileUtils Class API Description
```cangjie
public open class FileUtils {
    
    /*
     * 获取 FilePath
     * 
     * 参数 outPath - 输出路径
     * 参数 fileName - 文件名称
     * 返回值 String - 文件路径
     */
    public static func getFilePath (outPath: String, fileName: String): String
    
    /*
     * 获取 FileName
     * 
     * 参数 path - 文件路径
     * 返回值 String - 文件名称
     */
    public static func getFileName (path: String): String
    
    /*
     * 读取文件
     * 
     * 参数 path - 文件路径
     * 返回值 Array<UInt8> - 文件名的uint8数组
     */
    public static func readFile (path: String): Array<UInt8>
    
    /*
     * 写入数组到文件
     * 
     * 参数 outData - 数据
     * 参数 filePath - 文件路径
     */
    public static func writeFile (outData: Array<UInt8>, filePath: String)
}
```
#### 2.2.14 compressors.bzip2 包
##### 2.2.14.1 BZip2CompressorOutputStream
```cangjie
public class BZip2CompressorOutputStream<: CompressorOutputStream&BZip2Constants {

    /*
     * 根据给定的数据长度给出BlockSize大小。
     * 
     * 参数 inputLength - 将由 BZip2CompressorOutputStream 压缩的数据的长度。
     * 返回值 Int64 - 块大小，在 MIN_BLOCKSIZE 和 MAX_BLOCKSIZE 之间，两者都包含。对于负的 inputLlength，此方法始终返回 MAX_BLOCKSIZE。
     */
    public static func chooseBlockSize (inputLength: Int64): Int64
    
    /*
     * 构造一个新的 BZip2CompressorOutputStream，块大小为900k。
     * 
     * 参数 out - 目标流
     */
    public init (out: OutputStream)
    
    /*
     * 构造具有指定块大小的新 BZip2CompressorOutputStream。
     * 
     * 参数 out - 目标流
     * 参数 blockSize - 块大小
     */
    public init (out: OutputStream, blockSize: Int64)
    
    /*
     * 压缩完成, 只能 `写完成` 后调用
     */
    public func close (): Unit
    
    /*
     * 压缩是否完成
     * 
     * 返回值 Bool - true已完成 false未完成
     */
    public func isClosed (): Bool
    
    /*
     * 若传入的out需要flush, 此方法调用out.flush()
     */
    public override func flush (): Unit
    
    /*
     * 获取 BlockSize
     * 
     * 返回值 Int64 - 块大小
     */
    public func getBlockSize (): Int64
    
    /*
     * 写入需要压缩的数据
     * 
     * 参数 buffer - 需要压缩的数据
     */
    public override func write (buffer: Array<Byte>): Unit

}
```
##### 2.2.14.2 BZip2CompressorInputStream
```cangjie
public class BZip2CompressorInputStream<: CompressorInputStream&BZip2Constants&InputStreamStatistics {
    
    /*
     * 检查是否为bzip2文件
     * 
     * 参数 signature - 包含头信息的字节
     * 参数 length - 检查的长度
     * 返回值 Bool - true是bzip2字节头 false非bzip2字节头
     */
    public static func matches (signature: Array<UInt8>, length: Int64): Bool
    
    /*
     * 构造一个新的BZip2CompressorInputStream，用于解压从指定流中读取的字节。它不支持解压分卷的.bz2文件。
     * 
     * 参数 inputStream - 输入流
     */
    public init (inputStream: InputStream)
    
    /*
     * 构建一个新的BZip2CompressorInputStream，用于从指定的流中解压缩读取的字节。
     * 
     * 参数 inputStream - 这个对象应该从哪个输入流创建。
     * 参数 decompressConcatenated - 如果为true，则解压缩到输入的末尾；如果为false，则在第一个.bz2流之后停止。
     */
    public init (inputStream: InputStream, decompressConcatenated: Bool)
    
    /*
     * 获取已压缩大小
     * 
     * 返回值 Int64 - 已压缩大小
     */
    public override func getCompressedCount (): Int64
    
    /*
     * 读取已解压数据
     * 
     * 参数 buffer - 保存已解压数据
     * 返回值 Int64 - 已读取数量
     */
    public override func read (buffer: Array<Byte>): Int64
}
```
##### 2.2.14.3 BZip2Utils
```cangjie
public abstract class BZip2Utils {
    
    /*
     * 获取压缩后文件名
     * 
     * 参数 fileName - 源文件名
     * 返回值 String - 压缩后文件名
     */
    public static func getCompressedFilename (fileName: String): String

    /*
     * 获取解压后文件名
     * 
     * 参数 fileName - 压缩文件名
     * 返回值 String - 解压后文件名
     */
    public static func getUncompressedFilename (fileName: String): String
    
    /*
     * 判断是否压缩文件
     * 
     * 参数 fileName - 文件名
     * 返回值 Bool - true是 false否
     */
    public static func isCompressedFilename (fileName: String): Bool
}
```
#### 2.2.15 compressors.zlib 包
##### 2.2.15.1 Deflate
```cangjie
public class Deflate<: Stream {
    
    /*
    * 调用 init()，初始化 deflate. 
    */ 
    public init() 
    
    /*
    * 初始化压缩参数、状态，为内部缓冲区申请内存 
    * 参数 wrap - 压缩数据外包装类型，默认值为 ZLIB (压缩文件格式：ZLIB/DEFLATE) 
    * 参数 level - 压缩级别，默认值为 LEVEL_DEFAULT_COMPRESSION 
    *              (压缩级别 0-9， LEVEL_DEFAULT_COMPRESSION = 6) 
    * 参数 wbits - 窗口位数，默认值为 DEF_WINDOW_BITS (窗口位数 8-15： 
    *              DEF_WINDOW_BITS = 15) 
    * 参数 mlevel - 内存级别，默认值为 DEF_MEM_LEVEL (内存级别： 
    *               memlevel 1-9, DEF_MEM_LEVEL = 8) 
    * 参数 strategy - 压缩策略，默认值为 Z_DEFAULT_STRATEGY，取值为： 
    *                 Z_DEFAULT_STRATEGY：20 
    *                 Z_FILTERED：21 
    *                 Z_HUFFMAN_ONLY：22 
    *                 Z_RLE：23 
    *                 Z_FIXED：24 
    *
    * 返回值 UInt32 - 返回执行 deflateInit 函数的状态值，如果正常执行完 
    *                成返回值为 Z_OK (函数的返回值: Z_OK = 0) 
    */ 
    public func deflateInit(wrap!: WrapType = ZLIB, level!: UInt32 = LEVEL_DEFAULT_COMPRESSION, wbits!: UInt32 = DEF_WINDOW_BITS, mlevel!: UInt32 = DEF_MEM_LEVEL, strategy!: UInt32 = Z_DEFAULT_STRATEGY): UInt32

    /*
     * 压缩数据 
     *
     * 参数 flush - 缓冲处理类型 
     *              Z_NO_FLUSH: 计算输出结果前需要缓冲的数据量，以此来最大化压缩率。 
     *              Z_SYNC_FLUSH：所有未输出的结果会被 flush 到输出缓冲区，并以字节为单 位进行对齐。 
     *              Z_PARTIAL_FLUSH：所有未输出的结果会被 flush 到输出缓冲区中，但是输出 不会以字节为单位进行对齐。 
     *              Z_BLOCK：一个 deflate 块已经结束并释放，解压程序可能需要等待下一个块 被吐出时才能进行解压。这个 
     *                      特性可以用于控制 deflate 块的吞吐。 
     *              Z_FULL_FLUSH：所有的输出都会像 Z_SYNC_FLUSH 一样被冲出，并且压缩状 态会被重置，这样如果之前的 
     *                          已压缩数据被损坏或有随机访问需求，那么解压程序可以从这 个点重新开始运行。 
     *              Z_FINISH：所有的输入都会被处理并且所有的输出都会被冲出。 
     *
     * 返回值 UInt32 - 返回执行 deflate 函数的状态值 
     */ 
    public func deflate(flush: UInt32): UInt32

    /*
     * deflate 结束
     *
     * 返回值 UInt32 - 返回执行 deflateEnd 函数的状态值
     */
    public func deflateEnd(): UInt32

    /*
     * 根据源数据大小，计算压缩后数据最大尺寸，必须在 deflateInit 和 setGzipHeader 之后调用 
     *
     * 参数 sourceLen - 源数据大小 
     *
     * 返回值 Int64 - 压缩后数据最大尺寸 
     */
    public func deflateBound(sourceLen: Int64): Int64

    /*
     * 压缩开始前预设字典 (deflateInit 之后， deflate 之前调用) 
     * 设置的字典必须和要压缩的信息有关联关系，且字典中有效信息长度大于2，否则此设置无效。
     *
     * 参数 dict - 将数组 dict 中的数据设置为字典 
     *
     * 返回值 UInt32 - 返回执行 setDictionary 函数的状态值，如果正常执行完成返回值 
     *                 为 Z_OK (函数的返回值: Z_OK = 0) 
     */ 
    public func setDictionary(dict: Array<UInt8>): UInt32
}
```
##### 2.2.15.2 Zlib
```cangjie
public class Zlib {
    
    /*
     * 将数组 inbuf 中的数据进行压缩
     *
     * 参数 inbuf - 将数组 inbuf 中的数据进行压缩
     * 参数 level - 压缩文件等级，默认是6, 0是不压缩, 10是最好的压缩
     * 参数 wrap - 压缩文件格式，ZLIB_TYPE
     *
     * 返回值 Array<UInt8> - 压缩后的数据封装成 Array<UInt8> 格式返回 
     */
    public static func compress(inbuf: Array<UInt8>, level!: UInt32 = LEVEL_DEFAULT_COMPRESSION, wrap!: WrapTypeEnum = ZLIB_TYPE): Array<UInt8> 

    /*
     * 将数组 inbuf 中的数据进行解压 
     *
     * 参数 inbuf - 将数组 inbuf 中的数据进行解压 
     * 参数 wrap - 压缩文件格式，ZLIB_TYPE
     *
     * 返回值 Array<UInt8> - 解压后的数据封装成 Array<UInt8> 格式返回 
     */ 
    public static func uncompress(inbuf: Array<UInt8>, wrap: ZLIB): Array<UInt8>
}
```
##### 2.2.15.3 ZLibCompressorOutputStream
```cangjie
public class ZLibCompressorOutputStream<: CompressorOutputStream {
    
    /*
     * 构造 ZLibCompressorOutputStream
     * 
     * 参数 output - 输出流
     */
    public init (output: OutputStream)
    
    /*
     * 写入数据ZLibCompressorOutputStream中
     * 
     * 参数 buffer - 数据
     */
    public func write (buffer: Array<Byte>)
    
    /*
     * 写入数据到ZLibCompressorOutputStream
     * 
     * 参数 buf - 数据
     * 参数 off - 偏移
     * 参数 len - 长度
     */
    public func write (buf: Array<Byte>, off: Int32, len: Int32)
}
```
##### 2.2.15.4 ZLibCompressorInputStream
```cangjie
public class ZLibCompressorInputStream<: CompressorInputStream {
    
    /*
     * 构造 ZLibCompressorInputStream
     * 
     * 参数 input - 输入流
     */
    public init (input: InputStream)
    
    /*
     * 读取数据到buf中
     * 
     * 参数 buf - 目标数据
     * 返回值 Int64 - 长度
     */
    public func read (buf: Array<Byte>): Int64
    
    /*
     * 匹配sign长度
     * 
     * 参数 sign - 匹配数据
     * 参数 len - 长度
     * 返回值 Bool - 匹配成功返回true
     */
    public static func matches (sign: Array<Byte>, len: Int32): Bool
}
```
##### 2.2.15.5 Inflate
```cangjie
public class Inflate<: Stream {
    
    /*
     * 调用 init()，初始化 inflate. 
     */ 
    public init() 
    
    /*
     * 初始化解压参数、状态、内部缓冲区 
     *
     * 参数 wrap - 压缩数据外包装类型，默认值为 ZLIB (压缩文件格式: ZLIB/DEFLATE) 
     * 参数 wbits - 窗口位数，默认值为 DEF_WINDOW_BITS (窗口位数: 
     *              DEF_WINDOW_BITS = 15) 
     * 参数 ischeck - 是否检查原始数据检验值和 gzip 头的检验值 
     *
     * 返回值 UInt32 - 返回执行 inflateInit 函数的状态值，如果正常执行完成返回值 
     *                 为 Z_OK (函数的返回值: Z_OK = 0) 
     */ 
    public func inflateInit(wrap!: WrapType = ZLIB, wbits!:UInt32 = DEF_WINDOW_BITS, ischeck!: Bool = true): UInt32

    /*
     * 解压数据 
     *
     * 参数 flush - 缓冲处理类型 
     *              Z_SYNC_FLUSH: 要求 inflate() 冲出尽可能多的输出到输出缓冲区中。 
     *              Z_BLOCK：这个参数选项在处理合并 deflate 流或者往已有的流中添加内容时 派上用场，可以用来检查当前已消耗的数据的位数。 
     *              Z_TREES：可以用来检查 deflate 块头部的长度，以便之后在 deflate 块中 进行随机访问。 
     *              Z_FINISH：所有的解压操作都需要在一次调用中完成，所有的输入都会被处理并 且所有的输出都会被冲出。
     *
     * 返回值 UInt32 - 返回执行 inflate 函数的状态值 
     */ 
    public func inflate(flush: UInt32): UInt32

    /*
     * inflate 结束
     *
     * 返回值 UInt32 - 返回执行 inflateEnd 函数的状态值
     */
    public func inflateEnd(): UInt32

    /*
     * 设置字典，当 WrapType (压缩文件格式: ZLIB/DEFLATE) 类型为 DEFLATE 格式时， 在 inflateInit 之后 inflate 之前调用； 
     *          当 WrapType 类型为 ZLIB 格式时，在 inflate 返回 Z_NEED_DICT(函数的返回值: Z_NEED_DICT = 2) 后调用；  
     * 如果压缩时设置了字典，则解压前必须设置相同的字典 
     *
     * 参数 dict - 将数组 dict 中的数据设置为字典 
     *
     * 返回值 UInt32 - 返回执行 setDictionary 函数的状态值，如果正常执行完成 
     *                 返回值为 Z_OK (函数的返回值: Z_OK = 0) 
     */ 
    public func setDictionary(dict: Array<UInt8>): UInt32 

    /*
     * 解压时跳过非法的压缩数据，从下一个全刷新点开始解压，压缩时选择 
     * Z_FULL_FLUSH (冲洗类型: Z_FULL_FLUSH = 13) 策略会生成带全刷 
     * 新点的压缩数据 
     *
     * 返回值 UInt32 - 返回执行 inflateSync 函数的状态值，如果正常执行完成返回 
     *                 值为 Z_OK (函数的返回值: Z_OK = 0) 
     */ 
    public func inflateSync(): UInt32
}
```
##### 2.2.15.6 Stream
```cangjie
open class Stream {
    
    /*
     * 调用 init()，初始化 Stream 
     */
    public init()

    /*
     * 判断内部输出缓冲区是否为空，为空后，调用解压缩接口前需先重新设置输入数据 
     * 返回值 Bool - 判断内部输出缓冲区是否为空，如果是空返回 true，否则返回 false 
     */
    public func isInbufEmpty(): Bool

    /*
     * 检查输出缓冲区中是否有数据 
     * 返回值 Bool - 如果输出缓冲区中有数据，则返回 true，否则返回 false 
     */ 
    public func isHaveOutData(): Bool 

    /*
     * 设置输入缓冲区 
     * 参数 buf - 将 buf 设置为输入缓冲区 
     * 返回值 Int64 - 返回输入缓冲区实际剩余空间 
     */ 
    public func setInBuf(buf: Array<UInt8>): Int64 

    /*
     * 设置输入缓冲区 
     * 参数 buf - 将 buf 设置为输入缓冲区 
     * 参数 start - 将 start 设置为输入缓冲区当前光标位置 
     * 参数 len - 将 len 设置为输入缓冲区剩余空间 
     * 返回值 Int64 - 返回输入缓冲区实际剩余空间 
     */ 
    public func setInBuf(buf: Array<UInt8>, start: Int64, len: Int64): Int64 

    /*
     * 设置输出缓冲区 
     * 参数 buf - 将 buf 设置为输出缓冲区 
     * 返回值 Int64 - 返回输出缓冲区实际剩余空间 
     */ 
    public func setOutBuf(buf: Array<UInt8>): Int64 

    /*
     * 设置输出缓冲区 
     * 参数 buf - 将 buf 设置为输出缓冲区 
     * 参数 start - 将 start 设置为输出缓冲区当前光标位置 
     * 参数 len - 将 len 设置为输出缓冲区剩余空间 
     * 返回值 Int64 - 返回输出缓冲区实际剩余空间 
     */ 
    public func setOutBuf(buf: Array<UInt8>, start: Int64, len: Int64): Int64 
    
    /*
     * 重置输出缓冲区状态 
     */ 
    public func resetOutBuf(): Unit

    /*
     * 返回输出缓冲区中数据长度 
     * 返回值 Int64 - 返回输出缓冲区中数据长度 
     */ 
    public func getOutDataLength(): Int64 

    /*
     * 返回总输出数数据字节数 
     * 返回值 Int64 - 返回总输出数数据字节数 
     */ 
    public func getTotalOut(): Int64 

    /*
     * 返回总输入数据字节数 
     * 返回值 Int64 - 返回总输入数据字节数 
     */ 
    public func getTotalIn(): Int64
}
```
#### 2.2.16 compressors.deflate 包
##### 2.2.16.1 DeflateCompressorInputStream
```cangjie
public class DeflateCompressorInputStream<: CompressorInputStream {
    
    /*
     * 构造 DeflateCompressorInputStream
     * 
     * 参数 input - 输入流
     */
    public init (input: InputStream)
    
    /*
     * 读取数据到buf中
     * 
     * 参数 buf - 目标数据
     * 返回值 Int64 - 长度
     */
    public func read (buf: Array<Byte>): Int64
    
    /*
     * 读取数据到buf中
     * 
     * 参数 buf - 目标数据
     * 参数 off - 偏移
     * 参数 len - 长度
     * 返回值 Int64 - 读取的长度
     */
    public func read (buf: Array<Byte>, off: Int32, len: Int64): Int64
    
    /*
     * 匹配sign长度
     * 
     * 参数 sign - 匹配数据
     * 参数 len - 长度
     * 返回值 Bool - 匹配成功返回true
     */
    public static func matches (sign: Array<Byte>, len: Int32): Bool
}
```
##### 2.2.16.2 DeflateCompressorOutputStream
```cangjie
public class DeflateCompressorOutputStream<: CompressorOutputStream {
    
    /*
     * 构造 DeflateCompressorOutputStream
     * 
     * 参数 output - 输出流put
     */
    public init (output: OutputStream)
    
    /*
     * 写入数据到buffer中
     * 
     * 参数 buffer - 目标数组
     */
    public func write (buffer: Array<Byte>)
}
```
#### 2.2.17 compressors.gzip 包
##### 2.2.17.1 GzipParameters
```cangjie
public class GzipParameters {
    
    /*
     * 获取压缩文件的修改时间
     * 
     */
    public func getModificationTime(): Int64
    
    /*
     * 设置压缩文件的修改时间
     * 参数 Int64 -
     */
    public func setModificationTime(mod: Int64)
        
    /*
     * 获取压缩文件的名称
     * 
     */
    public func getFilename(): String
    
    /*
     * 设置压缩文件的名称
     * 参数 String -
     */
    public func setFilename(fname: String)
        
    /*
     * 获取说明
     * 
     */
    public func getComment(): String
    
    /*
     * 设置说明
     * 参数 String -
     */
    public func setComment(com: String)
        
    /*
     * 获取发生压缩的操作系统
     * 
     */
    public func getOperatingSystem(): Int32
    
    /*
     * 设置发生压缩的操作系统
     * 参数 Int32 -
     */
    public func setOperatingSystem(oper: Int32)
        
    /*
     * 获取用于检索压缩数据的缓冲区的大小。
     * 
     */
    public func getBufferSize(): Int64
    
    /*
     * 设置用于从中检索压缩数据的缓冲区的大小
     * 参数 Int64 -
     */
    public func setBufferSize(bufferSize: Int64)
}
```
##### 2.2.17.2 GzipCompressorInputStream
```cangjie
public class GzipCompressorInputStream<: CompressorInputStream {

	/*
     * 构造方法
     * 
     * 参数 InputStream - 输入流
     */
    public init(inputStream: InputStream)
    
    /*
     * 构造方法
     * 
     * 参数 InputStream - 输入流
     * 参数 Bool - 布尔值
     */
    public GzipCompressorInputStream(inputStream: InputStream, decompressConcatenated: Bool)
    
    /*
     * 解压读取，数据解压到buf中
     * 
     * 参数 Array<Byte> - 缓存
     * 异常 IllegalArgumentException - arr为空时抛出异常
     */
    public func read(buf: Array<Byte>): Int64
    
    /*
     * 检查签名是否与.gz文件的预期签名相匹配
     * 
     * 返回值 Bool - 布尔值
     */
    public static func matches(sign: Array<Byte>, len: Int32): Bool
}
```
##### 2.2.17.3 GzipCompressorOutputStream
```cangjie
public class GzipCompressorOutputStream<: CompressorOutputStream {
    
    /*
     * 构造 
     * 
     * 参数 out - 输出流
     */
    public init (out: OutputStream)
    
    /*
     * 将 buffer 中的数据压缩到流中
     * 
     * 参数 Array<Byte> - 写入数据的数据
     */
    public func write(buffer: Array<Byte>)
}
```

### 2.3 示例
下面是bzip2压缩的例子
代码如下: 
```cangjie
from compress4cj import compressors.*
from compress4cj import compressors.deflate.*
from compress4cj import compressors.support.*
from compress4cj import utils.externals.*
from compress import zlib.*
from std import fs.*
from std import io.*

main(){
    let input: File = File("test1.xml", Open(true, true))
    let ouput: File = File("bz2/hello.bz2", CreateOrTruncate(true))
    let cos: CompressorOutputStream = CompressorStreamFactory().createCompressorOutputStream(CompressorStreamFactory.BZIP2_STREAM, ouput)
    IOUtils.copy(input, cos)
    cos.flush()
    cos.close()
    println("success")
}
```
运行结果:
```cangjie
success
```

下面是tar 打包文件的代码
```cangjie
from compress4cj import utils.internals.*
from compress4cj import utils.externals.*
from compress4cj import utils.nio.*
from compress4cj import archivers.support.*
from compress4cj import archivers.tar.*
from compress4cj import archivers.*
from std import fs.*
from std import io.*

func TestTar() {
    let name1 = "./test1.xml"
    let name2 = "./test2.xml"
    let file1 = File(name1, Open(true, false))
    let file2 = File(name2, Open(true, false))

    var output = File("./bla.tar", CreateOrTruncate(true))
    let en1 = TarArchiveEntry(Path(name1), name1)
    let en2 = TarArchiveEntry(Path(name2), name2)

    let tar: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStream(ArchiveStreamFactory.TAR,
        output)

    tar.putArchiveEntry(en1)
    IOUtils.copy(file1, tar)
    tar.closeArchiveEntry()
    tar.putArchiveEntry(en2)
    IOUtils.copy(file2, tar)
    tar.closeArchiveEntry()
    tar.close()
}

func TestUnTar() {
    let input: File = File("./bla.tar", Open(true, true))
    let ais: ArchiveInputStream = ArchiveStreamFactory.DEFAULT.createArchiveInputStream("tar", input)
    let path: String = "./tarTest"
    IOUtils.copy(ais, path)
}

main() {
    TestTar()
    TestUnTar()
    println("success")
}
```

运行结果:
```cangjie
success
```
