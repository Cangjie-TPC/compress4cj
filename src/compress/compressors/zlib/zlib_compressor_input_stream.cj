/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */
package compress4cj.compress.compressors.zlib

import compress.zlib.*
import std.fs.*
import std.math.*
import std.io.*
import std.collection.*
import compress4cj.compress.compressors.*
public class ZLibCompressorInputStream <: CompressorInputStream {

    private static let MAGIC_1:  Byte = 0x1f
    private static let MAGIC_2: Byte = 0x9d
    private var inp: InputStream
    private let oneBytes: Array<Byte> = Array<Byte>(1, repeat: 0)
    var inflate: Inflate = Inflate()
    var input = ArrayList<UInt8>(8192*2)
    var outArray = ArrayList<UInt8>(8192)
    var offset = 0
    public init(input: InputStream) {
        this.inp = input
    }

    public func read(buf: Array<Byte>): Int64 {
        let outArraySize = outArray.size
        if (outArraySize > 0) {
            if (outArraySize > buf.size) {
                let min = min(buf.size, outArray.size - offset)
                for(i in 0..min) {
                    buf[i] = outArray[offset]
                    offset++
                }
                return min
            } else {
                for(i in 0..outArraySize) {
                    buf[i] = outArray[i]
                }
                outArray.clear()
                return outArraySize
            }
        }
        let arr: Array<Byte> = Array<Byte>(buf.size, repeat: 0)
        inp.read(arr)
        if (inflate.inflateInit(wrap: ZLIB_TYPE) != Z_OK) {
            return 0
        }
        var outlen: Int64 = buf.size
        var outBuf: Array<UInt8> = Array<UInt8>(outlen, repeat: 0)
        if (inflate.avail_in != 0) {
            input.clear()
            input.add(all: inflate.next_in[inflate.next_in.size - inflate.avail_in..inflate.next_in.size])
            input.add(all: arr)
            inflate.setInBuf(input.toArray())
        } else {
            inflate.setInBuf(arr)
        }
        inflate.setOutBuf(outBuf)

        var ret: UInt32
        var retBuf: ArrayList<UInt8> = ArrayList<UInt8>()
        while (true) {
            ret = inflate.inflate(Z_NO_FLUSH)
            match {
                case ret == Z_OK =>
                    if (inflate.isHaveOutData()) {
                        retBuf.add(all: outBuf[0..inflate.getOutDataLength()])
                        inflate.resetOutBuf()
                    }
                case ret == Z_STREAM_END =>
                    if (inflate.isHaveOutData()) {
                        retBuf.add(all: outBuf[0..inflate.getOutDataLength()])
                        inflate.resetOutBuf()
                    }
                    break
                case _ => return 0
            }
        }
        let resultarr: Array<Byte> = unsafe{retBuf.getRawArray()[0..retBuf.size]}
        let min = min(buf.size, resultarr.size)
        resultarr.copyTo(buf, 0, 0, min)
        if (resultarr.size - min > 0) {
            outArray.add(all: resultarr[min..resultarr.size])
        }
        return resultarr.size
    }

    public static func matches(sign: Array<Byte>, len: Int32): Bool {
        return len > 3 && sign[0] == MAGIC_1 && sign[1] == MAGIC_2
    }
}
