/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */
package compress4cj.compress

/**
 * Input stream that tracks the number of bytes read.
 * @NotThreadSafe
 */
class CountingInputStream <: InputStream {
    private var bytesRead: Int64 = 0
    private let input: InputStream
    private let arr = [0u8]

    public prop length: Int64 {
        get() {
            match(this.input as Seekable) {
                case Some(v) => v.remainLength
                case None => -1
            }
        }
    }

    public init(input: InputStream) {
        this.input = input
    }

    public override func read(b: Array<UInt8>): Int64 {
        let len = input.read(b)
        count(len)
        return len
    }

    /*
     * read one Byte
     * @return the next byte of data, or -1 if the end of the stream is reached.
     */
    public func readOneByte(): Int64 {
        if (read(arr) == 1) {
            return Int64(arr[0])
        }
        return -1
    }

    /**
     * Increments the counter of already read bytes.
     * Doesn't increment if the EOF has been hit (read == -1)
     *
     * @param read the number of bytes read
     */
    protected func count(read: Int64): Unit {
        if (read != -1) {
            bytesRead += read
        }
    }

    /**
     * Returns the current number of bytes read from this stream.
     * @return the number of read bytes
     */
    public func getBytesRead(): Int64 {
        return bytesRead
    }
}
