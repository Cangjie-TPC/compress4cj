/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */
package compress4cj.compress
import std.io.*

public class FixedLengthBlockOutputStream <: OutputStream & Resource {
    private var out: OutputStream
    private var buf: Array<Byte>
    private var offset: Int64 = 0

    public init(out: OutputStream, blockSize: Int64) {
        this.out = out
        if (blockSize <= 0) {
            throw IllegalArgumentException("blockSize is negative or 0.")
        }
        this.buf = Array<Byte>(blockSize, repeat: 0)
    }

    public func write(buffer: Array<Byte>): Unit {
        var index = buf.size - offset
        if (buffer.size == index) {
            this.out.write(this.buf[0..offset])
            this.out.write(buffer[0..index])
            offset = 0
            return
        } else if (buffer.size > index) {
            this.out.write(this.buf[0..offset])
            this.out.write(buffer[0..index])
            offset = 0
            if (buffer.size - index > this.buf.size) {
                let temp = index + (buffer.size - index) / this.buf.size * this.buf.size
                this.out.write(buffer[index..temp])
                index = temp
            }
        } else {
            index = 0
        }
        while (index < buffer.size) {
            buf[offset] = buffer[index]
            offset++
            index++
        }
    }

    public func flush(): Unit {
        this.flushBlock()
    }

    public func flushBlock(): Unit {
        if (offset == 0) {
            return 
        }
        if (offset < this.buf.size) {
            for (i in offset+1..this.buf.size) {
                this.buf[i] = 0
            }
        }
        out.write(buf)
        out.flush()
        offset = 0
    }
    
    override public func isClosed(): Bool {
        match(this.out as Resource) {
            case Some(v) => v.isClosed()
            case _ => false
        }
    }
    override public func close(): Unit {
        (this.out as Resource)?.close()
    }

}