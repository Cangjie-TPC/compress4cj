/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */

package compress4cj.compress.archivers.rar.rarfile

/*
 * The class is MainHeader class
 * @since 0.37.2
 */
public class MainHeader <: BaseBlock {
    private let log: Logger = getLogger()

    public static let mainHeaderSizeWithEnc: Int16 = 7
    public static let mainHeaderSize: Int16 = 6
    private var highPosAv: Int16 = 0
    private var posAv: Int32 = 0
    private var encryptVersion: Byte = 0

    public init(bb: BaseBlock, mainHeader: Array<Byte>) {
        super(bb)
        var position: Int64 = 0
        highPosAv = Raw.readShortLittleEndian(mainHeader, position)
        position += 2
        posAv = Raw.readIntLittleEndian(mainHeader, position)
        position += 4
        if (hasEncryptVersion()) {
            encryptVersion |= mainHeader[position] & 0xff
        }
    }

    public func hasArchCmt(): Bool {
        return (this.flags & BaseBlock.MHD_COMMENT) != 0
    }

    public func getEncryptVersion(): Byte {
        return encryptVersion
    }

    public func getHighPosAv(): Int16 {
        return highPosAv
    }

    public func getPosAv(): Int32 {
        return posAv
    }

    public func isEncrypted(): Bool {
        return (this.flags & BaseBlock.MHD_PASSWORD) != 0
    }

    public func isMultiVolume(): Bool {
        return (this.flags & BaseBlock.MHD_VOLUME) != 0
    }

    public func isFirstVolume(): Bool {
        return (this.flags & BaseBlock.MHD_FIRSTVOLUME) != 0
    }

    public func print() {
        super.print()
        let str: StringBuilder = StringBuilder()
        str.append("\nposav: ${getPosAv()}")
        str.append("\nhighposav: ${getHighPosAv()}")
        str.append("\nhasencversion: ${hasEncryptVersion()}")
        if (hasEncryptVersion()) {
            str.append(" ${getEncryptVersion()}")
        } else {
            str.append("")
        }
        str.append("\nhasarchcmt: ${hasArchCmt()}")
        str.append("\nisEncrypted: ${isEncrypted()}")
        str.append("\nisMultivolume: ${isMultiVolume()}")
        str.append("\nisFirstvolume: ${isFirstVolume()}")
        str.append("\nisSolid: ${isSolid()}")
        str.append("\nisLocked: ${isLocked()}")
        str.append("\nisProtected: ${isProtected()}")
        str.append("\nisAV: ${isAV()}")
        log.info(str.toString())
    }

    /**
     * returns whether this archive is solid. in this case you can only extract all file at once
     * @return Bool
     */
    public func isSolid(): Bool {
        return (this.flags & MHD_SOLID) != 0
    }

    public func isLocked(): Bool {
        return (this.flags & MHD_LOCK) != 0
    }

    public func isProtected(): Bool {
        return (this.flags & MHD_PROTECT) != 0
    }

    public func isAV(): Bool {
        return (this.flags & MHD_AV) != 0
    }

    /**
     * the numbering format a multivolume archive
     * @return Bool
     */
    public func isNewNumbering(): Bool {
        return (this.flags & MHD_NEWNUMBERING) != 0
    }
}
