/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */
package compress4cj.compress.archivers.rar.unpack.vm

public enum VMCommands {
    | VM_MOV
    | VM_CMP
    | VM_ADD
    | VM_SUB
    | VM_JZ
    | VM_JNZ
    | VM_INC
    | VM_DEC
    | VM_JMP
    | VM_XOR
    | VM_AND
    | VM_OR
    | VM_TEST
    | VM_JS
    | VM_JNS
    | VM_JB
    | VM_JBE
    | VM_JA
    | VM_JAE
    | VM_PUSH
    | VM_POP
    | VM_CALL
    | VM_RET
    | VM_NOT
    | VM_SHL
    | VM_SHR
    | VM_SAR
    | VM_NEG
    | VM_PUSHA
    | VM_POPA
    | VM_PUSHF
    | VM_POPF
    | VM_MOVZX
    | VM_MOVSX
    | VM_XCHG
    | VM_MUL
    | VM_DIV
    | VM_ADC
    | VM_SBB
    | VM_PRINT
    // #ifdef VM_OPTIMIZE
    | VM_MOVB
    | VM_MOVD
    | VM_CMPB
    | VM_CMPD
    | VM_ADDB
    | VM_ADDD
    | VM_SUBB
    | VM_SUBD
    | VM_INCB
    | VM_INCD
    | VM_DECB
    | VM_DECD
    | VM_NEGB
    | VM_NEGD
    // #endif*/
    | VM_STANDARD

    public func getVMCommand(): Int32 {
        match (this) {
            case VM_MOV => 0
            case VM_CMP => 1
            case VM_ADD => 2
            case VM_SUB => 3
            case VM_JZ => 4
            case VM_JNZ => 5
            case VM_INC => 6
            case VM_DEC => 7
            case VM_JMP => 8
            case VM_XOR => 9
            case VM_AND => 10
            case VM_OR => 11
            case VM_TEST => 12
            case VM_JS => 13
            case VM_JNS => 14
            case VM_JB => 15
            case VM_JBE => 16
            case VM_JA => 17
            case VM_JAE => 18
            case VM_PUSH => 19
            case VM_POP => 20
            case VM_CALL => 21
            case VM_RET => 22
            case VM_NOT => 23
            case VM_SHL => 24
            case VM_SHR => 25
            case VM_SAR => 26
            case VM_NEG => 27
            case VM_PUSHA => 28
            case VM_POPA => 29
            case VM_PUSHF => 30
            case VM_POPF => 31
            case VM_MOVZX => 32
            case VM_MOVSX => 33
            case VM_XCHG => 34
            case VM_MUL => 35
            case VM_DIV => 36
            case VM_ADC => 37
            case VM_SBB => 38
            case VM_PRINT => 39
            case VM_MOVB => 40
            case VM_MOVD => 41
            case VM_CMPB => 42
            case VM_CMPD => 43
            case VM_ADDB => 44
            case VM_ADDD => 45
            case VM_SUBB => 46
            case VM_SUBD => 47
            case VM_INCB => 48
            case VM_INCD => 49
            case VM_DECB => 50
            case VM_DECD => 51
            case VM_NEGB => 52
            case VM_NEGD => 53
            case VM_STANDARD => 54
        }
    }

    public func equals(vmCommand: Int32): Bool {
        return getVMCommand() == vmCommand
    }

    public static func findVMCommand(vmCommand: Int32): ?VMCommands {
        return match (vmCommand) {
            case 0 => VM_MOV
            case 1 => VM_CMP
            case 2 => VM_ADD
            case 3 => VM_SUB
            case 4 => VM_JZ
            case 5 => VM_JNZ
            case 6 => VM_INC
            case 7 => VM_DEC
            case 8 => VM_JMP
            case 9 => VM_XOR
            case 10 => VM_AND
            case 11 => VM_OR
            case 12 => VM_TEST
            case 13 => VM_JS
            case 14 => VM_JNS
            case 15 => VM_JB
            case 16 => VM_JBE
            case 17 => VM_JA
            case 18 => VM_JAE
            case 19 => VM_PUSH
            case 20 => VM_POP
            case 21 => VM_CALL
            case 22 => VM_RET
            case 23 => VM_NOT
            case 24 => VM_SHL
            case 25 => VM_SHR
            case 26 => VM_SAR
            case 27 => VM_NEG
            case 28 => VM_PUSHA
            case 29 => VM_POPA
            case 30 => VM_PUSHF
            case 31 => VM_POPF
            case 32 => VM_MOVZX
            case 33 => VM_MOVSX
            case 34 => VM_XCHG
            case 35 => VM_MUL
            case 36 => VM_DIV
            case 37 => VM_ADC
            case 38 => VM_SBB
            case 39 => VM_PRINT
            case 40 => VM_MOVB
            case 41 => VM_MOVD
            case 42 => VM_CMPB
            case 43 => VM_CMPD
            case 44 => VM_ADDB
            case 45 => VM_ADDD
            case 46 => VM_SUBB
            case 47 => VM_SUBD
            case 48 => VM_INCB
            case 49 => VM_INCD
            case 50 => VM_DECB
            case 51 => VM_DECD
            case 52 => VM_NEGB
            case 53 => VM_NEGD
            case 54 => VM_STANDARD
            case _ => None
        }
    }
}
