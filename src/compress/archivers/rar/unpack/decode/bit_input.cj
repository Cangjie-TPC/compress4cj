/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */
package compress4cj.compress.archivers.rar.unpack.decode

let arr = Array<Byte>(0x8000, repeat: 0)
public open class BitInput {
    public static let MAX_SIZE: Int32 = 0x8000
    protected var inAddr: Int32 = 0
    protected var inBit: Int32 = 0
    protected var inBuf: Array<Byte>

    public open func InitBitInput() {
        inAddr = 0
        inBit = 0
    }

    public open func addbits(Bits: Int32) {
        var bits = Bits + inBit
        inAddr += URS(bits, 3)
        inBit = bits & 7
    }

    @OverflowWrapping
    public open func getbits(): Int32 {
        let addr: Int64 = Int64(inAddr)
        return Int32(((UInt32(inBuf[addr]) << 16) | (UInt32(inBuf[addr + 1]) << 8) | UInt32(inBuf[addr + 2])) >> (8 - inBit)) & 0xffff
    }
    public init() {
        inBuf = arr
    }

    public open func faddbits(Bits: Int32) {
        var bits = Bits + inBit
        inAddr += URS(bits, 3)
        inBit = bits & 7
    }
    
    public open func fgetbits(): Int32 {
        let addr: Int64 = Int64(inAddr)
        return Int32(((UInt32(inBuf[addr]) << 16) | (UInt32(inBuf[addr + 1]) << 8) | UInt32(inBuf[addr + 2])) >> (8 - inBit)) & 0xffff
    }

    public open func Overflow(IncPtr: Int32): Bool {
        return (inAddr + IncPtr >= MAX_SIZE)
    }
    public open func getInBuf(): Array<Byte> {
        return inBuf
    }
}
