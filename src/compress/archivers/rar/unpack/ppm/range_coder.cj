/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */
package compress4cj.compress.archivers.rar.unpack.ppm

open public class RangeCoder {
    public static let TOP: Int32 = 1 << 24

    public static let BOT: Int32 = 1 << 15

    protected static let uintMask: Int64 = 0xFFFFffff

    protected var low: Int64 = 0
    protected var code: Int64 = 0
    protected var range: Int64 = 0

    protected let subRange = SubRange()

    private var unpackRead: ?Unpack = None
    public func getSubRange(): SubRange {
        return subRange
    }

    public func initDecoder(unpackRead: Unpack): Unit {
        this.unpackRead = unpackRead
        low = 0
        code = 0
        range = -1
        for (_ in 0..4) {
            code = ((code << 8) | Int64(getChar())) & uintMask
        }
    }

    public func getCurrentCount(): Int32 {
        range = (range / subRange.getScale()) & uintMask
        return Int32((code - low) / (range))
    }

    public func getCurrentShiftCount(SHIFT: Int32): Int64 {
        range = URS(range, SHIFT)
        return ((code - low) / (range)) & uintMask
    }

    public func decode() {
        low = (low + (range * subRange.getLowCount())) & uintMask
        range = (range * (subRange.getHighCount() - subRange.getLowCount())) & uintMask
    }

    private func getChar(): Int32 {
        return unpackRead.getOrThrow().getChar()
    }

    public func ariDecNormalize() {
        var c2 = range < Int64(BOT)
        while ((low ^ (low + range)) < Int64(TOP) || range < Int64(BOT)) {
            if (c2) {
                range = (-low & (Int64(BOT) - 1)) & uintMask
                c2 = false
            }
            code = ((code << 8) | Int64(getChar())) & uintMask
            range = (range << 8) & uintMask
            low = (low << 8) & uintMask
            c2 = range < Int64(BOT)
        }
    }

    public func toString(): String {
        let b = StringBuilder()
        b.append("RangeCoder[")
        b.append("\n  low=")
        b.append(low)
        b.append("\n  code=")
        b.append(code)
        b.append("\n  range=")
        b.append(range)
        b.append("\n  subrange=")
        b.append(subRange.toString())
        b.append("]")
        return b.toString()
    }
}

public class SubRange {
    private var lowCount: Int64 = 0
    private var highCount: Int64 = 0
    private var scale: Int64 = 0

    public func getHighCount(): Int64 {
        return highCount
    }

    public func setHighCount(highCount: Int64) {
        this.highCount = highCount & RangeCoder.uintMask
    }

    public func getLowCount(): Int64 {
        return lowCount & RangeCoder.uintMask
    }

    public func setLowCount(lowCount: Int64) {
        this.lowCount = lowCount & RangeCoder.uintMask
    }

    public func getScale(): Int64 {
        return scale
    }

    public func setScale(scale: Int64) {
        this.scale = scale & RangeCoder.uintMask
    }

    public func incScale(dScale: Int32) {
        setScale(getScale() + Int64(dScale))
    }

    public func toString(): String {
        let b = StringBuilder()
        b.append("SubRange[")
        b.append("\n  lowCount=")
        b.append(lowCount)
        b.append("\n  highCount=")
        b.append(highCount)
        b.append("\n  scale=")
        b.append(scale)
        b.append("]")
        return b.toString()
    }
}
