/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */
package compress4cj.compress.archivers.tar

@When[os != "Windows"]
func OSGetos(){
    return getos()
}

@When[os == "Windows"]
func OSGetos(){
    return "windows"
}

@When[os != "Windows"]
func OSGetuid(){
    return Int64(getuid())
}

@When[os == "Windows"]
func OSGetuid(){
    return 0
}

@When[os != "Windows"]
func OSGetgid(){
    return Int64(getgid())
}

@When[os == "Windows"]
func OSGetgid(){
    return 0
}

@When[os != "Windows"]
func OSGetHostname(){
    return gethostname()
}

@When[os == "Windows"]
func OSGetHostname(){
    return "unknown"
}

@When[os != "Windows"]
func OSGetlogin(){
    return getlogin()
}

@When[os == "Windows"]
func OSGetlogin(){
    return "unknown"
}

let CURRENT_OS = OSGetos()
let CURRENT_UID = OSGetuid()
let CURRENT_GID = OSGetgid()
let CURRENT_HOST_NAME = OSGetHostname()
let CURRENT_LOGIN = OSGetlogin()

public class TarArchiveEntry <: TarConstants & ArchiveEntry & EntryStreamOffsets { 

    public prop OFFSET_UNKNOWN: Int64{
        get() {-1}
    }
    private static let EMPTY_TAR_ARCHIVE_ENTRY_ARRAY: Array<TarArchiveEntry> = Array<TarArchiveEntry>()
    public static let UNKNOWN: Int64 = -1
    var name: String = ""
    private var preserveAbsolutePath: Bool = false
    private var mode: Int64 = 0
    private var userId: Int64 = 0
    private var groupId: Int64 = 0
    private var size: Int64 = 0
    private var modTime: Int64 = 0
    private var aTime: ?DateTime = None
    private var cTime: ?DateTime = None
    private var checkSumOK: Bool = false
    private var linkFlag: UInt8 = 0
    private var linkName: String = ""
    private var magic: String = TarConstants.MAGIC_POSIX
    private var version: String = TarConstants.VERSION_POSIX
    private var userName: String = ""
    private var groupName: String = ""
    private var devMajor: Int64 = 0
    private var devMinor: Int64 = 0
    private var sparseHeaders: ?ArrayList<TarArchiveStructSparse> = None
    private var isExtended: Bool = false
    private var realSize: Int64 = 0
    private var paxGNUSparse: Bool = false
    private var paxGNU1XSparse: Bool = false
    private var starSparse: Bool = false
    private var hasFile = false
    private var file: (Path, FileInfo) = unsafe { zeroValue<(Path, FileInfo)>() }
    private var extraPaxHeaders: Map<String, String> = HashMap<String, String>()
    public static let MAX_NAMELEN: Int64 = 31
    public static let DEFAULT_DIR_MODE: Int64 = 0o40755
    public static let DEFAULT_FILE_MODE: Int64 = 0o100644
    public static let MILLIS_PER_SECOND: Int64 = 1000
    private var dataOffset: Int64 = -1


    public init(){
    }

    public init(name: String){
        this(name, false)
    }

    public init(preserveAbsolutePath: Bool) {
        var user = CURRENT_LOGIN
        if (user.size > 31) {
            user = String(user.toRuneArray().slice(0,31))
        }
        this.userName = user
        this.preserveAbsolutePath = preserveAbsolutePath
    }

    public init(name: String, linkFlag: UInt8) {
        this(name, linkFlag, false)
    }

    public init(name: String, linkFlag: UInt8, preserveAbsolutePath: Bool) { 
        this(name,preserveAbsolutePath ) 
        this.linkFlag = linkFlag
        if (linkFlag == LF_GNUTYPE_LONGNAME) {
            magic = MAGIC_GNU;
            version = VERSION_GNU_SPACE;
        }
    }

    public init(name: String, preserveAbsolutePath: Bool) {
        this(preserveAbsolutePath)
        let n = TarArchiveEntry.normalizeFileName(name, preserveAbsolutePath)
        let isDir = n.endsWith("/")
        this.name = n
        if (isDir) {
            this.mode = DEFAULT_DIR_MODE 
            this.linkFlag = LF_DIR
        } else {
            this.mode = DEFAULT_FILE_MODE
            this.linkFlag = LF_NORMAL
        }
        this.userName = ""
        
    }

    public init(file: Path, fileName: String) {
        let normalizedName: String = TarArchiveEntry.normalizeFileName(fileName, false)
        this.file = (file, FileInfo(file))
        this.hasFile = true
        try {
            this.readFileMode(file, normalizedName)
        } catch (e: Exception) {
            if (!this.file[1].isDirectory()) {
                this.size = this.file[1].size
            }
        }

        this.userName = ""  
        try {
            this.readOsSpecificProperties()
        } catch (e: Exception) {
            this.modTime = this.file[1].lastModificationTime.toUnixTimeStamp().toMilliseconds() / TarArchiveEntry.MILLIS_PER_SECOND
        }
        this.preserveAbsolutePath = false
    }

    public func tarArchiveEntryFile(file: Path, fileName: String): Unit {
        let normalizedName: String = TarArchiveEntry.normalizeFileName(fileName, false)
        this.file = (file, FileInfo(file))
        this.hasFile = true
        try {
            this.readFileMode(file, normalizedName)
        } catch (e: Exception) {
            if (!this.file[1].isDirectory()) {
                this.size = this.file[1].size
            }
        }

        this.userName = "" 
        try {
            this.readOsSpecificProperties()
        } catch (e: Exception) {
            this.modTime = this.file[1].lastModificationTime.toUnixTimeStamp().toMilliseconds() / TarArchiveEntry.MILLIS_PER_SECOND
        }
        this.preserveAbsolutePath = false
    }

    private func tarArchiveEntryPath(preserveAbsolutePath: Bool): Unit {
        this.preserveAbsolutePath = preserveAbsolutePath
    }

    public func tarArchiveEntryBuf(headerBuf: Array<Byte>, encoding: ZipEncoding, lenient: Bool): Unit {
        this.tarArchiveEntryPath(false)
        this.parseTarEncoding(headerBuf, encoding, oldStyle: false, lenient: lenient)
    }

    public func tarArchiveEntryLinkFlag(name: String, linkFlag: Byte): Unit {
        this.tarArchiveEntryPreserveAbsolutePath(name, linkFlag, false)
    }

    public func tarArchiveEntryPreserveAbsolutePath(name: String, linkFlag: UInt8, preserveAbsolutePath: Bool): Unit {
        this.tarArchiveEntryPreserveAbsolutePath(name, preserveAbsolutePath)
        this.linkFlag = linkFlag
        if (linkFlag == TarConstants.LF_GNUTYPE_LONGNAME) {
            this.magic = TarConstants.MAGIC_GNU
            this.version = TarConstants.VERSION_GNU_SPACE
        }
    }

    public func tarArchiveEntryPreserveAbsolutePath(name: String, preserveAbsolutePath: Bool): Unit {
        this.tarArchiveEntryPath(preserveAbsolutePath)
        this.name = TarArchiveEntry.normalizeFileName(name, preserveAbsolutePath)
        let isDir: Bool = this.name.endsWith("/")
        this.mode = if (isDir) {
            TarArchiveEntry.DEFAULT_DIR_MODE
        } else {
            TarArchiveEntry.DEFAULT_FILE_MODE
        }
        this.linkFlag = if (isDir) {
            TarConstants.LF_DIR
        } else {
            TarConstants.LF_NORMAL
        }
        this.modTime = DateTime.now().toUnixTimeStamp().toMilliseconds() / TarArchiveEntry.MILLIS_PER_SECOND
        this.userName = CURRENT_HOST_NAME
    }

    public func tarArchiveEntryLinkOptions(file: Path, fileName: String): Unit {
        let normalizedName: String = TarArchiveEntry.normalizeFileName(fileName, false)
        this.file = (file, FileInfo(file))
        this.hasFile = true
        this.readFileMode(file, normalizedName)
        this.userName = CURRENT_HOST_NAME
        this.readOsSpecificProperties()
        this.preserveAbsolutePath = false
    }

    private func readOsSpecificProperties(): Unit {
        if (this.hasFile) {
            this.setModTime(this.file[1].lastModificationTime.toUnixTimeStamp().toMilliseconds())
            this.userName = CURRENT_HOST_NAME
            this.groupName = ""
            this.userId = CURRENT_UID
            this.groupId = CURRENT_GID
        }
    }

    private func readFileMode(file: Path, normalizedName: String): Unit {
        if (FileInfo(file).isDirectory()) {
            this.mode = TarArchiveEntry.DEFAULT_DIR_MODE
            this.linkFlag = TarConstants.LF_DIR
            let nameLength: Int64 = normalizedName.size
            if (nameLength == 0 || normalizedName[nameLength - 1] != r'/') {
                this.name = normalizedName + "/"
            } else {
                this.name = normalizedName
            }
        } else {
            this.mode = TarArchiveEntry.DEFAULT_FILE_MODE
            this.linkFlag = TarConstants.LF_NORMAL
            this.name = normalizedName
            if (this.hasFile) {
                this.size = this.file[1].size
            }
        }
    }

    public func equals(it: TarArchiveEntry): Bool {
        return this.getName() == it.getName()
    }

    public func equalsObject(it: Object): Bool {
        if (!(it is TarArchiveEntry)) {
            return false
        }
        return equals((it as TarArchiveEntry).getOrThrow())
    }

    public func isDescendent(desc: TarArchiveEntry): Bool {
        return desc.getName().startsWith(this.getName())
    }

    public func getName(): String {
        return this.name
    }

    public func setName(name: String): Unit {
        this.name = TarArchiveEntry.normalizeFileName(name, this.preserveAbsolutePath)
    }

    public func setMode(mode: Int64): Unit {
        this.mode = mode
    }

    public func getLinkName(): String {
        return this.linkName
    }

    public func setLinkName(link: String): Unit {
        this.linkName = link
    }

    public func setUserId(userId: Int64): Unit {
        this.setUserIdLong(userId)
    }

    public func getLongUserId(): Int64 {
        return this.userId
    }

    public func setUserIdLong(userId: Int64): Unit {
        this.userId = userId
    }

    public func setGroupId(groupId: Int64): Unit {
        this.setGroupIdLong(groupId)
    }

    public func getLongGroupId(): Int64 {
        return this.groupId
    }

    public func setGroupIdLong(groupId: Int64): Unit {
        this.groupId = groupId
    }

    public func getUserName(): String {
        return this.userName
    }

    public func setUserName(userName: String): Unit {
        this.userName = userName
    }

    public func getGroupName(): String {
        return this.groupName
    }

    public func setGroupName(groupName: String): Unit {
        this.groupName = groupName
    }

    public func setIds(userId: Int64, groupId: Int64): Unit {
        this.setUserId(userId)
        this.setGroupId(groupId)
    }

    public func setNames(userName: String, groupName: String): Unit {
        this.setUserName(userName)
        this.setGroupName(groupName)
    }

    public func setModTime(time: Int64): Unit {
        if (time != 0) {
            this.modTime = time / TarArchiveEntry.MILLIS_PER_SECOND
        }
    }

    public func setModTimeDate(time: DateTime): Unit {
        this.modTime = time.toUnixTimeStamp().toMilliseconds() / TarArchiveEntry.MILLIS_PER_SECOND
    }

    public func getModTime(): DateTime {
        return DateTime.ofEpoch(second: this.modTime, nanosecond: 0)
    }

    public func getLastModifiedDate(): DateTime {
        return this.getModTime()
    }

    public func isCheckSumOK(): Bool {
        return this.checkSumOK
    }

    public func getFile(): ?Path {
        return if (hasFile) {
            this.file[0]
        } else {
            None
        }
    }

    public func getPath(): ?Path {
        return if (hasFile) {
            this.file[0]
        } else {
            None
        }
    }

    public func getMode(): Int64 {
        return this.mode
    }

    public func getSize(): Int64 {
        return this.size
    }

    public func setSparseHeaders(sparseHeaders: ArrayList<TarArchiveStructSparse>): Unit {
        this.sparseHeaders = sparseHeaders
    }

    public func getSparseHeaders(): ArrayList<TarArchiveStructSparse> {
        return this.sparseHeaders.getOrThrow()
    }

    public func isPaxGNU1XSparse(): Bool {
        return this.paxGNU1XSparse
    }

    public func setSize(size: Int64): Unit {
        if (size < 0) {
            throw IllegalArgumentException("Size is out of range: ${size}")
        }
        this.size = size
    }

    public func getDevMajor(): Int64 {
        return this.devMajor
    }

    public func setDevMajor(devNo: Int64): Unit {
        if (devNo < 0) {
            throw  IllegalArgumentException("Major device Int64 is out of range: ${devNo}")
        }
        this.devMajor = devNo
    }

    public func getDevMinor(): Int64 {
        return this.devMinor
    }

    public func setDevMinor(devNo: Int64): Unit {
        if (devNo < 0) {
            throw  IllegalArgumentException("Minor device Int64 is out of range: ${devNo}" )
        }
        this.devMinor = devNo
    }

    public func isExtendedFollows(): Bool {
        return this.isExtended
    }

    public func getRealSize(): Int64 {
        if (!this.isSparse()) {
            return this.getSize()
        }
        return this.realSize
    }

    public func isGNUSparse(): Bool {
        return this.isOldGNUSparse() || this.isPaxGNUSparse()
    }

    public func isOldGNUSparse(): Bool {
        return this.linkFlag == TarConstants.LF_GNUTYPE_SPARSE
    }

    public func isPaxGNUSparse(): Bool {
        return this.paxGNUSparse
    }

    public func isStarSparse(): Bool {
        return this.starSparse
    }

    public func isGNULongLinkEntry(): Bool {
        return this.linkFlag == TarConstants.LF_GNUTYPE_LONGLINK
    }

    public func isGNULongNameEntry(): Bool {
        return this.linkFlag == TarConstants.LF_GNUTYPE_LONGNAME
    }

    public func isPaxHeader(): Bool {
        return this.linkFlag == TarConstants.LF_PAX_EXTENDED_HEADER_LC || this.linkFlag == TarConstants.LF_PAX_EXTENDED_HEADER_UC
    }

    public func isGlobalPaxHeader(): Bool {
        return this.linkFlag == TarConstants.LF_PAX_GLOBAL_EXTENDED_HEADER
    }

    public func isDirectory(): Bool {
        if (hasFile) {
            return this.file[1].isDirectory()
        } else {
            if (this.linkFlag == TarConstants.LF_DIR) {
                return true
            }
            return false
        }
    }

    public func isFile(): Bool {
        if (this.hasFile) {
            this.file[1].isRegular()
        } else {
            if (this.linkFlag == TarConstants.LF_OLDNORM || this.linkFlag == TarConstants.LF_NORMAL) {
                return true
            }
            return !this.getName().endsWith("/")
        }
    }

    public func isSymbolicLink(): Bool {
        return this.linkFlag == TarConstants.LF_SYMLINK
    }

    public func isLink(): Bool {
        return this.linkFlag == TarConstants.LF_LINK
    }

    public func isCharacterDevice(): Bool {
        return this.linkFlag == TarConstants.LF_CHR
    }

    public func isBlockDevice(): Bool {
        return this.linkFlag == TarConstants.LF_BLK
    }

    public func isFIFO(): Bool {
        return this.linkFlag == TarConstants.LF_FIFO
    }

    public func getLinkFlag(): UInt8 {
        return this.linkFlag
    }

    public func isSparse(): Bool {
        return this.isGNUSparse() || this.isStarSparse()
    }

    public func getDataOffset(): Int64 {
        return this.dataOffset
    }

    public func setDataOffset(dataOffset: Int64): Unit {
        if (dataOffset < 0) {
            throw  IllegalArgumentException("The offset can not be smaller than 0")
        }
        this.dataOffset = dataOffset
    }

    public func isStreamContiguous(): Bool {
        return true
    }

    public func getExtraPaxHeaders(): Map<String, String> {
        return this.extraPaxHeaders
    }

    public func clearExtraPaxHeaders(): Unit {
        this.extraPaxHeaders.clear()
    }

    public func addPaxHeader(name: String, value: String): Unit {
        try {
            this.processPaxHeader(name, value)
        } catch (ex: Exception) {
            throw  IllegalArgumentException("Invalid input")
        }
    }

    public func getExtraPaxHeader(name: String): ?String {
        return this.extraPaxHeaders.get(name)
    }

    protected func updateEntryFromPaxHeaders(headers: Map<String, String>): Unit {
        for ((key, value) in headers){
            this.processPaxHeader3(key, value, headers)
        }
    }

    private func processPaxHeader(key: String, val: String): Unit {
        this.processPaxHeader3(key, val, this.extraPaxHeaders)
    }

    private func processPaxHeader3(key: String, val: String, headers: Map<String, String>): Unit {
        match (key) {
            case "path"     => this.setName(val)
            case "linkpath" => this.setLinkName(val)
            case "gid"      => this.setGroupIdLong(Int64.parse(val))
            case "gname"    => this.setGroupName(val)
            case "uid"      => this.setUserIdLong(Int64.parse(val))
            case "uname"    => this.setUserName(val)
            case "size" => 
                let size: Int64 = Int64.parse(val)
                if (size < 0) {
                    throw  Exception("Corrupted TAR archive. Entry size is negative")
                }
                this.setSize(size)
            case "mtime"    => this.setModTime(Int64.parse(val) * 1000)
            case "SCHILY.devminor" => 
                let devMinor: Int64 = Int64.parse(val)
                if (devMinor < 0) {
                    throw  Exception("Corrupted TAR archive. Dev-Minor is negative")
                }
                this.setDevMinor(devMinor)
            case "SCHILY.devmajor" => 
                let devMajor: Int64 = Int64.parse(val)
                if (devMajor < 0) {
                    throw  Exception("Corrupted TAR archive. Dev-Major is negative")
                }
                this.setDevMajor(devMajor)
            case "GNU.sparse.size" => this.fillGNUSparse0xData(headers)
            case "GNU.sparse.realsize" =>  this.fillGNUSparse1xData(headers)
            case "SCHILY.filetype" => 
                if ("sparse" == val) {
                    this.fillStarSparseData(headers)
                }
            case _  => this.extraPaxHeaders.add(key, val)
        }
    }

    public func writeEntryHeader(outbuf: Array<Byte>): Unit {
        try {
            this.writeEntryHeader(outbuf, TarUtils.DEFAULT_ENCODING, false)
        } catch (ex: Exception) {
            try {
                this.writeEntryHeader(outbuf, TarUtils.FALLBACK_ENCODING, false)
            } catch (ex2: Exception) {
                throw  Exception(ex2.message)
            }
        }
    }

    public func writeEntryHeader(outbuf: Array<Byte>, encoding: ZipEncoding, starMode: Bool): Unit {
        var offset: Int64 = 0
        offset = TarUtils.formatNameBytes(name, outbuf, offset, NAMELEN, encoding)
        offset = writeEntryHeaderField(mode, outbuf, offset, MODELEN, starMode)
        offset = writeEntryHeaderField(userId, outbuf, offset, UIDLEN, starMode)
        offset = writeEntryHeaderField(groupId, outbuf, offset, GIDLEN, starMode)
        offset = writeEntryHeaderField(size, outbuf, offset, SIZELEN, starMode)
        offset = writeEntryHeaderField(this.modTime, outbuf, offset, MODTIMELEN, starMode)

        let csOffset = offset

        offset = fill(UInt8(UInt32(r' ')), offset, outbuf, CHKSUMLEN)

        outbuf[offset] = linkFlag
        offset++
        offset = TarUtils.formatNameBytes(linkName, outbuf, offset, NAMELEN, encoding)
        offset = TarUtils.formatNameBytes(magic, outbuf, offset, MAGICLEN)
        offset = TarUtils.formatNameBytes(version, outbuf, offset, VERSIONLEN)
        offset = TarUtils.formatNameBytes(userName, outbuf, offset, UNAMELEN, encoding)
        offset = TarUtils.formatNameBytes(groupName, outbuf, offset, GNAMELEN, encoding)
        offset = writeEntryHeaderField(devMajor, outbuf, offset, DEVLEN, starMode)
        offset = writeEntryHeaderField(devMinor, outbuf, offset, DEVLEN, starMode)

        if (starMode) {
            // skip prefix
            offset = fill(0, offset, outbuf, PREFIXLEN_XSTAR)
            offset = writeEntryHeaderOptionalTimeField(aTime, offset, outbuf, ATIMELEN_XSTAR)
            offset = writeEntryHeaderOptionalTimeField(cTime, offset, outbuf, CTIMELEN_XSTAR)
            // 8-byte fill
            offset = fill(0, offset, outbuf, 8)
            // Do not write MAGIC_XSTAR because it causes issues with some TAR tools
            // This makes it effectively XUSTAR, which guarantees compatibility with USTAR
            offset = fill(0, offset, outbuf, XSTAR_MAGIC_LEN)
        }
        offset = fill(0, offset, outbuf, outbuf.size - offset) // NOSONAR - assignment as documentation
        let chk: Int64 = TarUtils.computeCheckSum(outbuf)
        TarUtils.formatCheckSumOctalBytes(chk, outbuf, csOffset, TarConstants.CHKSUMLEN)
    }

    @OverflowWrapping
    func fill(value: Int64, offset: Int64, outbuf: Array<Byte>, length: Int64): Int64 {
        fill(UInt8(value), offset, outbuf, length)
    }

    func fill(value: Byte, offset: Int64, outbuf: Array<Byte>, length: Int64): Int64 {
        for (i in 0..length) {
            outbuf[offset + i] = value
        }
        return offset + length
    }

    private func writeEntryHeaderField(value: Int64, outbuf: Array<Byte>, offset: Int64, length: Int64, starMode: Bool): Int64 {
        if (!starMode && (value < 0 || value >= 1 << 3 * (length - 1))) {
            return TarUtils.formatLongOctalBytes(0, outbuf, offset, length)
        }
        return TarUtils.formatLongOctalOrBinaryBytes(value, outbuf, offset, length)
    }

    public func parseTarHeader(header: Array<Byte>): Unit {
        try {
            this.parseTarEncoding(header, TarUtils.DEFAULT_ENCODING)
        } catch (ex:Exception) {
            try {
                this.parseTarEncoding(header, TarUtils.DEFAULT_ENCODING, oldStyle: true, lenient: false)
            } catch (ex2:Exception) {
                throw  Exception("not really possible")
            }
        }
    }

    private func parseTarEncoding(header: Array<Byte>, encoding: ZipEncoding, oldStyle!: Bool = false, lenient!: Bool = false): Unit {
        try {
            this.parseTarHeaderUnwrapped(header, encoding, oldStyle, lenient)
        } catch (ex: Exception) {
            throw  Exception("Corrupted TAR archive.")
        }
    }

    private func parseTarHeaderUnwrapped(header: Array<Byte>, encoding: ZipEncoding, oldStyle: Bool, lenient: Bool): Unit {
        var offset: Int64 = 0
        this.name = if (oldStyle) {
            TarUtils.parseName(header, offset, TarConstants.NAMELEN)
        } else {
            TarUtils.parseNameEncoding(header, offset, TarConstants.NAMELEN, encoding)
        }
        offset += TarConstants.NAMELEN
        this.mode = this.parseOctalOrBinary(header, offset, TarConstants.MODELEN, lenient)
        offset += TarConstants.MODELEN
        this.userId = this.parseOctalOrBinary(header, offset, TarConstants.UIDLEN, lenient)
        offset += TarConstants.UIDLEN
        this.groupId = this.parseOctalOrBinary(header, offset, TarConstants.GIDLEN, lenient)
        offset += TarConstants.GIDLEN
        this.size = TarUtils.parseOctalOrBinary(header, offset, TarConstants.SIZELEN)
        if (this.size < 0) {
            throw  Exception("broken archive, entry with negative size")
        }
        offset += TarConstants.SIZELEN
        this.modTime = this.parseOctalOrBinary(header, offset, TarConstants.MODTIMELEN, lenient)
        offset += TarConstants.MODTIMELEN
        this.checkSumOK = TarUtils.verifyCheckSum(header)
        offset += TarConstants.CHKSUMLEN
        this.linkFlag = header[offset]
        offset++
        this.linkName = if (oldStyle) {
            TarUtils.parseName(header, offset, TarConstants.NAMELEN)
        } else {
            TarUtils.parseNameEncoding(header, offset, TarConstants.NAMELEN, encoding)
        }
        offset += TarConstants.NAMELEN
        this.magic = TarUtils.parseName(header, offset, TarConstants.MAGICLEN)
        offset += TarConstants.MAGICLEN
        this.version = TarUtils.parseName(header, offset, TarConstants.VERSIONLEN)
        offset += TarConstants.VERSIONLEN
        this.userName = if (oldStyle) {
            TarUtils.parseName(header, offset, TarConstants.UNAMELEN)
        } else {
            TarUtils.parseNameEncoding(header, offset, TarConstants.UNAMELEN, encoding)
        }
        offset += TarConstants.UNAMELEN
        this.groupName = if (oldStyle) {
            TarUtils.parseName(header, offset, TarConstants.GNAMELEN)
        } else {
            TarUtils.parseNameEncoding(header, offset, TarConstants.GNAMELEN, encoding)
        }
        offset += TarConstants.GNAMELEN
        if (this.linkFlag == TarConstants.LF_CHR || this.linkFlag == TarConstants.LF_BLK) {
            this.devMajor = this.parseOctalOrBinary(header, offset, TarConstants.DEVLEN, lenient)
            offset += TarConstants.DEVLEN
            this.devMinor = this.parseOctalOrBinary(header, offset, TarConstants.DEVLEN, lenient)
            offset += TarConstants.DEVLEN
        } else {
            offset += 2 * TarConstants.DEVLEN
        }

        let types: Int64 = this.evaluateType(header)
        match  {
            case types == TarConstants.FORMAT_OLDGNU => 
                offset += TarConstants.ATIMELEN_GNU
                offset += TarConstants.CTIMELEN_GNU
                offset += TarConstants.OFFSETLEN_GNU
                offset += TarConstants.LONGNAMESLEN_GNU
                offset += TarConstants.PAD2LEN_GNU
                this.sparseHeaders = TarUtils.readSparseStructs(header, offset, TarConstants.SPARSE_HEADERS_IN_OLDGNU_HEADER)
                offset += TarConstants.SPARSELEN_GNU
                this.isExtended = TarUtils.parseBoolean(header, offset)
                offset += TarConstants.ISEXTENDEDLEN_GNU
                this.realSize = TarUtils.parseOctal(header, offset, TarConstants.REALSIZELEN_GNU)
                offset += TarConstants.REALSIZELEN_GNU
            case types == TarConstants.FORMAT_XSTAR =>
                let xstarPrefix: String = if (oldStyle) {
                    TarUtils.parseName(header, offset, TarConstants.PREFIXLEN_XSTAR)
                } else {
                    TarUtils.parseNameEncoding(header, offset, TarConstants.PREFIXLEN_XSTAR, encoding)
                }
                if (xstarPrefix.size > 0) {
                    this.name = xstarPrefix + "/" + this.name
                }
            case _ =>
                let prefix: String = if (oldStyle) {
                    TarUtils.parseName(header, offset, TarConstants.PREFIXLEN)
                } else {
                    TarUtils.parseNameEncoding(header, offset, TarConstants.PREFIXLEN, encoding)
                }
                if (this.isDirectory() && !this.name.endsWith("/")) {
                    this.name = this.name + "/"
                }
                if (prefix.size > 0) {
                    this.name = prefix + "/" + this.name
                }
        }
    }

    private func parseOctalOrBinary(header: Array<Byte>, offset: Int64, length: Int64, lenient: Bool): Int64 {
        if (lenient) {
            try {
                return TarUtils.parseOctalOrBinary(header, offset, length)
            } catch (ex: Exception) {
                return TarArchiveEntry.UNKNOWN
            }
        }
        return TarUtils.parseOctalOrBinary(header, offset, length)
    }

    private func evaluateType(header: Array<Byte>): Int64 {
        if (ArchiveUtils.matchAsciiBuffer(TarConstants.MAGIC_GNU, header, offset: TarConstants.MAGIC_OFFSET, length: TarConstants.MAGICLEN)) {
            return TarConstants.FORMAT_OLDGNU
        }
        if (ArchiveUtils.matchAsciiBuffer(TarConstants.MAGIC_POSIX, header, offset: TarConstants.MAGIC_OFFSET, length: TarConstants.MAGICLEN)) {
            if (ArchiveUtils.matchAsciiBuffer(TarConstants.MAGIC_XSTAR, header, offset: TarConstants.XSTAR_MAGIC_OFFSET, length: TarConstants.XSTAR_MAGIC_LEN)) {
                return TarConstants.FORMAT_XSTAR
            }
            return TarConstants.FORMAT_POSIX
        }
        return 0
    }

    func fillGNUSparse0xData(headers: Map<String, String>): Unit {
        this.paxGNUSparse = true
        this.realSize = Int64.parse(headers["GNU.sparse.size"])
        if (headers.contains("GNU.sparse.name")) {
            this.name = headers["GNU.sparse.name"]
        }
    }

    func fillGNUSparse1xData(headers: Map<String, String>): Unit {
        this.paxGNUSparse = true
        this.paxGNU1XSparse = true
        if (headers.contains("GNU.sparse.name")) {
            this.name = headers["GNU.sparse.name"]
        }
        if (headers.contains("GNU.sparse.realsize")) {
            try {
                this.realSize = Int64.parse(headers["GNU.sparse.realsize"])
            } catch (ex:Exception) {
                throw  Exception("Corrupted TAR archive. GNU.sparse.realsize header for ${this.name} contains non-numeric value")
            }
        }
    }

    func fillStarSparseData(headers: Map<String, String>): Unit {
        this.starSparse = true
        if (headers.contains("SCHILY.realsize")) {
            try {
                this.realSize = Int64.parse(headers["SCHILY.realsize"])
            } catch (ex: Exception) {
                throw  Exception("Corrupted TAR archive. SCHILY.realsize header for " + this.name + " contains non-numeric value")
            }
        }
    }

    private static func normalizeFileName(fileNameLet: String, preserveAbsolutePath: Bool): String {
        var fileName = fileNameLet
        if (!preserveAbsolutePath) {
            let osname: String = CURRENT_OS

            if (osname != "") {
                if (osname.startsWith("windows")) {
                    if (fileName.size > 2) {
                        let ch1 = fileName[0]
                        let ch2 = fileName[1]
                        if (ch2 == r':' && (ch1 >= r'a' && ch1 <= r'z' || ch1 >= r'A' && ch1 <= r'Z')) {
                            fileName = String(fileName.toRuneArray()[2..])  // D: C: c: 
                        }
                    }
                } else if (osname.contains("netware")) {
                    let fileNameChars = fileName.toRuneArray()
                    for (i in 0..fileNameChars.size where fileNameChars[i] == r':') {
                        fileName = String(fileName.toRuneArray()[i+1..])
                        break
                    }
                }
            }
        }
        while (!preserveAbsolutePath && fileName.startsWith("/")) {
            fileName = String(fileName.toRuneArray()[1..])
        }
        return fileName
    }

    public func getOrderedSparseHeaders(): Array<TarArchiveStructSparse> {
        match (this.sparseHeaders) {
            case None => return Array<TarArchiveStructSparse>()
            case Some(v) => 
                if (v.size == 0) {
                    return Array<TarArchiveStructSparse>()
                }
        }
        let getRawArray: Array<TarArchiveStructSparse> = unsafe {this.sparseHeaders.getOrThrow().getRawArray()}
        let orderedAndFiltered = ArrayList<TarArchiveStructSparse>()
        for (item in getRawArray where (item.getOffset() > 0 || item.getNumbytes() > 0)) {
            orderedAndFiltered.add(item)
        }
        orderedAndFiltered.sortBy(stable: true, comparator: {R: TarArchiveStructSparse, L: TarArchiveStructSparse =>
            if (R.getOffset() < L.getOffset()) {
                return Ordering.LT
            } else if(R.getOffset() > L.getOffset()) {
                return Ordering.GT
            } else {
                return Ordering.EQ
            }
        })

        let numberOfHeaders: Int64 = orderedAndFiltered.size
        var i = 0
        while (i < numberOfHeaders) {
            let str: TarArchiveStructSparse = orderedAndFiltered[i]
            if (i + 1 < numberOfHeaders && str.getOffset() + str.getNumbytes() > orderedAndFiltered[i + 1].getOffset()) {
                throw Exception("Corrupted TAR archive. Sparse blocks for ${this.getName()} overlap each other.")
            }
            if (str.getOffset() + str.getNumbytes() < 0) {
                throw Exception("Unreadable TAR archive. Offset and numbytes for sparse block in ${this.getName()} too large.")
            }
            i++
        }
        if (orderedAndFiltered.size != 0) {
            let last: TarArchiveStructSparse = orderedAndFiltered[numberOfHeaders - 1]
            if (last.getOffset() + last.getNumbytes() > getRealSize()) {
                throw Exception("Corrupted TAR archive. Sparse block extends beyond real size of the entry")
            }
        }
        return unsafe {orderedAndFiltered.getRawArray()}
    }

    private func writeEntryHeaderOptionalTimeField(time: ?DateTime, offset: Int64, outbuf: Array<Byte>, fieldLength: Int64): Int64 {
        var off = offset
        match(time) {
            case Some(v) => off = writeEntryHeaderField(v.toUnixTimeStamp().toMilliseconds(), outbuf, offset, fieldLength, true)
            case None => off = fill(0, offset, outbuf, fieldLength)
        }
        return offset
    }
}


