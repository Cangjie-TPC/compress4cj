/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2024. All rights reserved.
 */
package compress4cj.compress.archivers

public interface ArchiveStreamProvider {

    func createArchiveInputStream(name: String, input: InputStream, encoding: ?String): ArchiveInputStream

    func createArchiveOutputStream(name: String, output: OutputStream, encoding: ?String): ArchiveOutputStream

    func getInputStreamArchiveNames(): Set<String>

    func getOutputStreamArchiveNames(): Set<String>

}